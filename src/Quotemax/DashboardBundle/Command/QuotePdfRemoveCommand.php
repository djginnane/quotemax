<?php

namespace Quotemax\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;


class QuotePdfRemoveCommand extends ContainerAwareCommand
{
	/*
	 *  //To find files in symfony by finder class
	 *  	- http://fabien.potencier.org/article/43/find-your-files
	 *  	- http://symfony.com/doc/current/components/finder.html
	 * 		- http://symfony.com/doc/current/components/filesystem.html  << for basic utilities for the file sytem
	 * 
	 */
	protected function configure()
	{
		//ex. >php app/console quote:pdf:remove --confirm yes
		$this
		->setName('quote:pdf:remove')
		->setDescription('Remove all generated PDF for quote/invoice in web folder.')
		->addOption('confirm', null, InputOption::VALUE_REQUIRED, 'confirm \'yes\' or \'no\', to proceed the task or cancel')
		;
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
	
		if ($input->getOption('confirm') ==  'yes') {
			$result = false;
			$output->writeln('Proceeded command action.');
			$paramPdfPath = $this->getContainer()->getParameter('quotemax.path_pdf');
			$path = $this->getContainer()->get('kernel')->getRootDir() . '/../web'.'/'.$paramPdfPath.'/';
			$finder = new Finder();
			//$iterator = $finder->files()->in(__DIR__); //find current folder of this file
			$iterator = $finder->files()->depth(1)->date('before yesterday')->in($path);
			$output->writeln('Find files in dir > '.$path);
			$filesystem = new Filesystem();
			foreach ($iterator as $file)
			{
				//print $file->getRealpath()."\n";
				$output->writeln('  - '.$file->getRelativePath()." removed\n");
				$filesystem->remove($path.$file->getRelativePath());
			}
			
			$output->writeln('Succesfully removed all temp generated PDFs.');
			
			/*
			if($result){
				$output->writeln('Succesfully expired \'pending\' quotes later than valid days');
			}else{
				$output->writeln('Not found pending quotes to be expired.');
			}*/
		}else{
			$output->writeln('Aborted command action.');
		}
	
		$output->writeln('Ended command action!!');
	}

}
