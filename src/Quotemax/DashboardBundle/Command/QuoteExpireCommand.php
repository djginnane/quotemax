<?php

namespace Quotemax\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class QuoteExpireCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		//ex. >php app/console quote:expire --confirm yes
		$this
		->setName('quote:expire')
		->setDescription('Find pending quotes older than valid days, then set expired status')
		->addOption('confirm', null, InputOption::VALUE_REQUIRED, 'confirm \'yes\' or \'no\', to proceed the task or cancel')
		;
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
	
		if ($input->getOption('confirm') ==  'yes') {
			$output->writeln('Proceeded command action.');
			$quoteManager = $this->getContainer()->get('quotemax_dashboard.quote_manager');
			$result = $quoteManager->findPendingQuotesAndExpire();
			if($result){
				$output->writeln('Succesfully expired \'pending\' quotes later than valid days');
			}else{
				$output->writeln('Not found pending quotes to be expired.');
			}
		}else{
			$output->writeln('Aborted command action.');
		}
	
		$output->writeln('Ended command action.');
	}

}
