<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\TransportRateRepository")
 * @ORM\Table(name="transport_rate")
 */
class TransportRate
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * 
	 */
	protected $id;
	
	
    /**
     * @ORM\Column(type="decimal",  scale=2, nullable=false)
     *
     */
    protected $fromWeight;
    
    
    /**
     * @ORM\Column(type="decimal",  scale=2, nullable=false)
     *
     */
    protected $toWeight;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=false)
     *
     */
    protected $rate;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     *
     */
    protected $conditionCompare;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $formula;
    

    /**
     * Set fromWeight
     *
     * @param string $fromWeight
     * @return TransportRate
     */
    public function setFromWeight($fromWeight)
    {
        $this->fromWeight = $fromWeight;

        return $this;
    }

    /**
     * Get fromWeight
     *
     * @return string 
     */
    public function getFromWeight()
    {
        return $this->fromWeight;
    }

    /**
     * Set toWeight
     *
     * @param string $toWeight
     * @return TransportRate
     */
    public function setToWeight($toWeight)
    {
        $this->toWeight = $toWeight;

        return $this;
    }

    /**
     * Get toWeight
     *
     * @return string 
     */
    public function getToWeight()
    {
        return $this->toWeight;
    }

    /**
     * Set rate
     *
     * @param string $rate
     * @return TransportRate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set conditionCompare
     *
     * @param string $conditionCompare
     * @return TransportRate
     */
    public function setConditionCompare($conditionCompare)
    {
        $this->conditionCompare = $conditionCompare;

        return $this;
    }

    /**
     * Get conditionCompare
     *
     * @return string 
     */
    public function getConditionCompare()
    {
        return $this->conditionCompare;
    }

    /**
     * Set formula
     *
     * @param string $formula
     * @return TransportRate
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string 
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
