<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\UserDetailRepository")
 * @ORM\Table(name="user_detail")
 */
class UserDetail
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Quotemax\UserBundle\Entity\User", inversedBy="detail")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * 
     */
    protected $user;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $name;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users", cascade={"remove"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;
    
  

    /**
     * Set name
     *
     * @param string $name
     * @return UserDetail
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param \Quotemax\UserBundle\Entity\User $user
     * @return UserDetail
     */
    public function setUser(\Quotemax\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Quotemax\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set company
     *
     * @param \Quotemax\DashboardBundle\Entity\Company $company
     * @return UserDetail
     */
    public function setCompany(\Quotemax\DashboardBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Quotemax\DashboardBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
