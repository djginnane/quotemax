-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Host: 127.12.133.130:3306
-- Generation Time: May 29, 2015 at 04:28 AM
-- Server version: 5.5.41
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quotemax`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weightOrder` int(11) DEFAULT NULL,
  `methodOfChoice` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_64C19C1989D9B62` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `weightOrder`, `methodOfChoice`, `isActive`, `slug`) VALUES
(1, 'Thickness', 1, 'O', 1, 'thickness'),
(2, 'Led', NULL, 'O', 1, 'led'),
(3, 'Consumables (paint/potting)', NULL, 'M', 1, 'consumables-paint-potting'),
(4, 'Illuminated Area', NULL, 'O', 1, 'illuminated-area'),
(5, 'Options (Front)', NULL, 'O', 1, 'options-front'),
(6, 'Labour Cost', NULL, 'M', 1, 'labour-cost'),
(8, 'Extra Rules', NULL, 'O', 1, 'extra-rules'),
(9, 'Options (Return) (DELETED BY ADMIN)', NULL, 'O', 0, 'options-return'),
(10, 'Face spec.', NULL, 'O', 1, 'face-details'),
(11, 'Special design', NULL, 'O', 1, 'special-design'),
(12, 'Raw material / Finishes\r\n', NULL, 'O', 1, 'raw-material'),
(13, 'Mounting', NULL, 'O', 1, 'mounting'),
(14, 'Lighting\r\n', NULL, 'O', 1, 'option'),
(15, 'Letter Thickness', NULL, 'O', 1, 'letter-thickness'),
(16, 'Front Cover Material', NULL, 'O', 1, 'front-cover-material'),
(17, 'Return', NULL, 'O', 1, 'return'),
(18, 'Options', NULL, 'O', 1, 'options'),
(19, 'Led - Acrynox', NULL, 'M', 1, 'led-acrynox'),
(20, 'Consumables (paint, potting) - Acrynox', NULL, 'M', 0, 'consumables-paint-potting-acrynox'),
(21, 'Raw Material', NULL, 'O', 1, 'raw-material-1'),
(22, 'Thickness', NULL, 'O', 1, 'thickness-1'),
(23, 'Mounting', NULL, 'O', 1, 'mounting-1'),
(24, 'Laser Cutting Service', NULL, 'O', 1, 'laser-cutting-service'),
(25, 'Raw Material', NULL, 'O', 1, 'raw-material-2'),
(26, 'Logo/Text Colors', NULL, 'O', 1, 'logo-text-colors'),
(27, 'Mounting', NULL, 'O', 1, 'mounting-2'),
(28, 'Acid Etching', NULL, 'M', 1, 'acid-etching'),
(29, 'Labour cost - Acrylic', NULL, 'M', 1, 'labour-cost-acrylic'),
(30, 'Labour Cost - Inox', NULL, 'M', 1, 'labour-cost-inox'),
(31, 'Labour Cost - Acrynox', NULL, 'M', 1, 'labour-cost-acrynox'),
(32, 'Labour Cost - Flat Cut', NULL, 'M', 1, 'labour-cost-flatcut'),
(33, 'Labour Cost - Plaque', NULL, 'M', 1, 'labour-cost-plaque');

-- --------------------------------------------------------

--
-- Table structure for table `category_value`
--

CREATE TABLE IF NOT EXISTS `category_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weightOrder` int(11) DEFAULT NULL,
  `formattedRules` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relatedRule_id` int(11) DEFAULT NULL,
  `isAdditionalCostAfterMargin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F544177D12469DE2` (`category_id`),
  KEY `IDX_F544177DEE0CA5E8` (`relatedRule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=156 ;

--
-- Dumping data for table `category_value`
--

INSERT INTO `category_value` (`id`, `category_id`, `name`, `value`, `weightOrder`, `formattedRules`, `relatedRule_id`, `isAdditionalCostAfterMargin`) VALUES
(1, 1, 'Acrylic 20mm', 'Acrylic 20mm', NULL, '0.65*%a%', NULL, NULL),
(2, 1, 'Acrylic 30mm', 'Acrylic 30mm', NULL, '0.8*%a%', NULL, NULL),
(3, 2, 'Led (Return)', 'Led (Return)', NULL, '20.0*%p%*%coefficient-led-return%', NULL, NULL),
(4, 3, 'Consumables (paint/potting)', 'Consumables (paint/potting)', NULL, '15.0*%h%', NULL, NULL),
(5, 4, 'Front illuminated', 'Front illuminated', NULL, '0.0', 18, NULL),
(6, 5, 'Face Stainless Steel', 'Face Stainless Steel', NULL, '0.35*%a%', 19, NULL),
(7, 5, 'Face sticker 3M ScotchCal', 'Face sticker 3M ScotchCal', NULL, '0.1*%a%', NULL, NULL),
(8, 9, 'Return stainless Steel', 'Return stainless Steel', NULL, '0.35*%d%*%p%', NULL, NULL),
(9, 9, 'Return sticker 3M ScotchCal', 'Return sticker 3M ScotchCal', NULL, '0.1*%d%*%p%', NULL, NULL),
(10, 6, 'Labour cost', 'Labour cost', NULL, '8.0*%p%', NULL, NULL),
(11, 4, 'Side illuminated (50%)', 'Side illuminated (50%)', NULL, '0.0', 3, NULL),
(12, 4, 'Side illuminated (100%)', 'Side illuminated (100%)', NULL, '0.0', 3, NULL),
(13, 4, 'Reverse illuminated', 'Reverse illuminated', NULL, '0.3*%a%', 3, NULL),
(14, 4, 'Outlined illuminated  ', 'Outlined illuminated  ', NULL, '0.3*%a%', 18, NULL),
(15, 4, 'Front illuminated + 50% side illuminated', 'Front illuminated + 50% side illuminated', NULL, '0.0', 18, NULL),
(16, 4, 'Front & reverse illuminated combined', 'Front & reverse illuminated combined', NULL, '0.3*%a%', 18, NULL),
(17, NULL, '[DELETED] Reverse illuminated with illuminated sides', 'Reverse illuminated with illuminated sides', NULL, '0.3*%a%', 3, NULL),
(18, 2, 'Led (Face)', 'Led (Face)', NULL, '15.0*%s%*%coefficient-led-face%', NULL, NULL),
(19, 8, 'Cutting service charge (stainless only)', 'Cutting service charge (stainless only)', NULL, '2.0*%p%', NULL, NULL),
(20, 10, 'Standard', 'Metal face', NULL, '0.0', NULL, NULL),
(21, 10, 'Diffusing Acrylic with trim edge\r\n', 'Trim edge with Acrylic face', NULL, '0.8*%a%', NULL, NULL),
(22, 11, 'Prismatic', 'Prismatic', NULL, '0.0', NULL, 1),
(23, 11, 'Serif', 'Serif', NULL, '0.0', NULL, 1),
(24, 12, 'Polished stainless steel (304: standard)', 'Polished Stainless Steel (304)', 10, '0.5*(%a%+(%d%*%p%))', NULL, NULL),
(25, 12, 'Satin stainless steel (304: standard)', 'Satin Stainless Steel (304)', 20, '0.5*(%a%+(%d%*%p%))', NULL, NULL),
(26, 12, 'Polished stainless steel (316L: seaside usage)', 'Polished Stainless Steel (316L)', 30, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(27, 12, 'Satin stainless steel (316L: seaside usage)', 'Satin Stainless Steel (316L)', 40, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(28, 12, 'Polished gold titanium', 'Polished Titanium (any color)', 50, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(29, 12, 'Satin gold titanium ', 'Satin Titanium (any color)', 60, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(30, 12, 'Polished brass', 'Polished Brass', 150, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(31, 12, 'Satin brass', 'Satin Brass', 160, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(32, 12, 'Polished copper', 'Polished Copper', 170, '0.5*(%a%+(%d%*%p%))', NULL, NULL),
(33, 12, 'Satin copper', 'Satin Copper', 180, '0.5*(%a%+(%d%*%p%))', NULL, NULL),
(34, 12, 'Steel with rusty effect\r\n', 'Painted Stainless Steel (any color)', 210, '0.4*(%a%+(%d%*%p%))', NULL, NULL),
(35, NULL, '[Removed for now]Painted Aluminium (any color)', 'Painted Aluminium (any color)', NULL, '0.4*(%a%+(%d%*%p%))', NULL, NULL),
(36, 12, 'Painted galvanized steel', 'Painted Galvanized Steel', 220, '0.4*(%a%+(%d%*%p%))', NULL, NULL),
(37, 13, 'Inserts + studs + spacers ', 'Inserts', NULL, '50.0', NULL, NULL),
(38, 13, 'Straps with adhesive VHB', 'Straps with adhesive VHB', NULL, '50.0', NULL, NULL),
(39, 13, 'Welded nuts at the back', 'Welded nuts', NULL, '60.0', NULL, NULL),
(40, 13, 'Clear polycarbonate back', 'Clear polycarbonate back', NULL, '0.2*%a%', NULL, NULL),
(41, 13, 'PVC back', 'PVC Back', NULL, '0.1*%a%', NULL, NULL),
(42, 14, 'Front-lighting with LED & transformer\r\n', 'Led lighting', NULL, '10.0*(%p%/2)', NULL, 1),
(43, 15, '20mm', '20mm', NULL, '0.65*%a%', NULL, NULL),
(44, 16, 'Polished Stainless Steel (304)', 'Polished Stainless Steel (304)', NULL, '0.5*(%a%+(%ri%*%p%))', NULL, NULL),
(45, 16, 'Satin Stainless Steel (304)', 'Satin Stainless Steel (304)', NULL, '0.5*(%a%+(%ri%*%p%))', NULL, NULL),
(46, 16, 'Polished Stainless Steel (316L)', 'Polished Stainless Steel (316L)', NULL, '0.6*(%a%+(%ri%*%p%))', NULL, NULL),
(47, 16, 'Satin Stainless Steel (316L)', 'Satin Stainless Steel (316L)', NULL, '0.6*(%a%+(%ri%*%p%))', NULL, NULL),
(48, 16, 'Polished Titanium', 'Polished Titanium', NULL, '0.6*(%a%+(%ri%*%p%))', NULL, NULL),
(49, 16, 'Satin Titanium', 'Satin Titanium', NULL, '0.6*(%a%+(%ri%*%p%))', NULL, NULL),
(50, 16, 'Polished Brass', 'Polished Brass', NULL, '0.6*(%a%+(%ri%*%p%))', NULL, NULL),
(51, 16, 'Satin Brass', 'Satin Brass', NULL, '0.6*(%a%+(%ri%*%p%))', NULL, NULL),
(52, 16, 'Polished Copper', 'Polished Copper', NULL, '0.5*(%a%+(%ri%*%p%))', NULL, NULL),
(53, 16, 'Satin Copper', 'Satin Copper', NULL, '0.5*(%a%+(%ri%*%p%))', NULL, NULL),
(54, 16, 'Painted Stainless Steel', 'Painted Stainless Steel', NULL, '0.4*(%a%+(%ri%*%p%))', NULL, NULL),
(55, 16, 'Painted Aluminium', 'Painted Aluminium', NULL, '0.4*(%a%+(%ri%*%p%))', NULL, NULL),
(56, 16, 'Painted Galvanized Steel', 'Painted Galvanized Steel', NULL, '0.4*(%a%+(%ri%*%p%))', NULL, NULL),
(57, 15, '30mm', '30mm', NULL, '0.80*%a%', NULL, NULL),
(58, 17, '10mm', '10mm', NULL, '0.0', NULL, NULL),
(59, 17, '15mm', '15mm', NULL, '0.0', NULL, NULL),
(60, 18, 'Return sticker 3M ScotchCal', 'Return sticker 3M ScotchCal', NULL, '0.1*((%d%-%ri%)*%p%)', NULL, NULL),
(61, 19, 'Led - Return (Acrynox)', 'Led - Return (Acrynox)', NULL, '20.0*%p%*%coefficient-led-return%', NULL, NULL),
(62, 21, 'Polished Stainless Steel (304) 1mm', 'Polished Stainless Steel (304) 1mm', NULL, '0.5*%a%', NULL, NULL),
(63, 21, 'Polished Stainless Steel (304) 1.5mm', 'Polished Stainless Steel (304) 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(64, 21, 'Polished Stainless Steel (304) 2mm', 'Polished Stainless Steel (304) 2mm', NULL, '0.6*%a%', NULL, NULL),
(65, 21, 'Polished Stainless Steel (304) 3mm', 'Polished Stainless Steel (304) 3mm', NULL, '0.65*%a%', NULL, NULL),
(66, 21, 'Satin Stainless Steel (304) 1mm', 'Satin Stainless Steel (304) 1mm', NULL, '0.5*%a%', NULL, NULL),
(67, 21, 'Satin Stainless Steel (304) 1.5mm', 'Satin Stainless Steel (304) 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(68, 21, 'Satin Stainless Steel (304) 2mm', 'Satin Stainless Steel (304) 2mm', NULL, '0.6*%a%', NULL, NULL),
(69, 21, 'Satin Stainless Steel (304) 3mm', 'Satin Stainless Steel (304) 3mm', NULL, '0.65*%a%', NULL, NULL),
(70, 21, 'Polished Stainless Steel (316L) 1mm', 'Polished Stainless Steel (316L) 1mm', NULL, '0.5*%a%', NULL, NULL),
(71, 21, 'Polished Stainless Steel (316L) 1.5mm', 'Polished Stainless Steel (316L) 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(72, 21, 'Polished Stainless Steel (316L) 2mm', 'Polished Stainless Steel (316L) 2mm', NULL, '0.6*%a%', NULL, NULL),
(73, 21, 'Polished Stainless Steel (316L) 3mm', 'Polished Stainless Steel (316L) 3mm', NULL, '0.65*%a%', NULL, NULL),
(74, 21, 'Satin Stainless Steel (316L) 1mm', 'Satin Stainless Steel (316L) 1mm', NULL, '0.5*%a%', NULL, NULL),
(75, 21, 'Satin Stainless Steel (316L) 1.5mm', 'Satin Stainless Steel (316L) 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(76, 21, 'Satin Stainless Steel (316L) 2mm', 'Satin Stainless Steel (316L) 2mm', NULL, '0.6*%a%', NULL, NULL),
(77, 21, 'Satin Stainless Steel (316L) 3mm', 'Satin Stainless Steel (316L) 3mm', NULL, '0.65*%a%', NULL, NULL),
(78, 21, 'Titanium 1mm', 'Titanium 1mm', NULL, '0.5*%a%', NULL, NULL),
(79, 21, 'Titanium 1.5mm', 'Titanium 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(80, 21, 'Titanium 2mm', 'Titanium 2mm', NULL, '0.6*%a%', NULL, NULL),
(81, 21, 'Titanium 3mm', 'Titanium 3mm', NULL, '0.65*%a%', NULL, NULL),
(82, 21, 'Copper 1mm', 'Copper 1mm', NULL, '0.5*%a%', NULL, NULL),
(83, 21, 'Copper 1.5mm', 'Copper 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(84, 21, 'Copper 2mm', 'Copper 2mm', NULL, '0.6*%a%', NULL, NULL),
(85, 21, 'Copper 3mm', 'Copper 3mm', NULL, '0.65*%a%', NULL, NULL),
(86, 21, 'Brass 1mm', 'Brass 1mm', NULL, '0.5*%a%', NULL, NULL),
(87, 21, 'Brass 1.5mm', 'Brass 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(88, 21, 'Brass 2mm', 'Brass 2mm', NULL, '0.6*%a%', NULL, NULL),
(89, 21, 'Brass 3mm', 'Brass 3mm', NULL, '0.65*%a%', NULL, NULL),
(90, 23, 'Welded nuts', 'Welded nuts', NULL, '200.0', NULL, NULL),
(91, 23, 'Welded studs', 'Welded studs', NULL, '400.0', NULL, NULL),
(92, 23, 'Adhesive VHB', 'Adhesive VHB', NULL, '200.0', NULL, NULL),
(93, 24, 'Cutting 1 mm', 'Cutting 1 mm', NULL, '2.0*%p%', NULL, NULL),
(94, 24, 'Cutting 1.5 mm', 'Cutting 1.5 mm', NULL, '3.0*%p%', NULL, NULL),
(95, 24, 'Cutting 2 mm', 'Cutting 2 mm', NULL, '4.0*%p%', NULL, NULL),
(96, 24, 'Cutting 3 mm', 'Cutting 3 mm', NULL, '5.0*%p%', NULL, NULL),
(97, 25, 'Polished Stainless Steel (304) 1mm', 'Polished Stainless Steel (304) 1mm', NULL, '0.5*%a%', NULL, NULL),
(98, 25, 'Polished Stainless Steel (304) 1.5mm', 'Polished Stainless Steel (304) 1.5mm', NULL, '0.5*%a%', NULL, NULL),
(99, 25, 'Polished Stainless Steel (304) 2mm', 'Polished Stainless Steel (304) 2mm', NULL, '0.5*%a%', NULL, NULL),
(100, 25, 'Polished Stainless Steel (304) 3mm', 'Polished Stainless Steel (304) 3mm', NULL, '0.5*%a%', NULL, NULL),
(101, 25, 'Satin Stainless Steel (304) 1mm', 'Satin Stainless Steel (304) 1mm', NULL, '0.5*%a%', NULL, NULL),
(102, 25, 'Satin Stainless Steel (304) 1.5mm', 'Satin Stainless Steel (304) 1.5mm', NULL, '0.5*%a%', NULL, NULL),
(103, 25, 'Satin Stainless Steel (304) 2mm', 'Satin Stainless Steel (304) 2mm', NULL, '0.5*%a%', NULL, NULL),
(104, 25, 'Satin Stainless Steel (304) 3mm', 'Satin Stainless Steel (304) 3mm', NULL, '0.5*%a%', NULL, NULL),
(105, 25, 'Polished Stainless Steel (316L) 1mm', 'Polished Stainless Steel (316L) 1mm', NULL, '0.5*%a%', NULL, NULL),
(106, 25, 'Polished Stainless Steel (316L) 1.5mm', 'Polished Stainless Steel (316L) 1.5mm', NULL, '0.5*%a%', NULL, NULL),
(107, 25, 'Polished Stainless Steel (316L) 2mm', 'Polished Stainless Steel (316L) 2mm', NULL, '0.5*%a%', NULL, NULL),
(108, 25, 'Polished Stainless Steel (316L) 3mm', 'Polished Stainless Steel (316L) 3mm', NULL, '0.5*%a%', NULL, NULL),
(109, 25, 'Satin Stainless Steel (316L) 1mm', 'Satin Stainless Steel (316L) 1mm', NULL, '0.5*%a%', NULL, NULL),
(110, 25, 'Satin Stainless Steel (316L) 1.5mm', 'Satin Stainless Steel (316L) 1.5mm', NULL, '0.5*%a%', NULL, NULL),
(111, 25, 'Satin Stainless Steel (316L) 2mm', 'Satin Stainless Steel (316L) 2mm', NULL, '0.5*%a%', NULL, NULL),
(112, 25, 'Satin Stainless Steel (316L) 3mm', 'Satin Stainless Steel (316L) 3mm', NULL, '0.5*%a%', NULL, NULL),
(113, 25, 'Titanium 1mm', 'Titanium 1mm', NULL, '0.5*%a%', NULL, NULL),
(114, 25, 'Titanium 1.5mm', 'Titanium 1.5mm', NULL, '0.5*%a%', NULL, NULL),
(115, 25, 'Titanium 2mm', 'Titanium 2mm', NULL, '0.5*%a%', NULL, NULL),
(116, 25, 'Titanium 3mm', 'Titanium 3mm', NULL, '0.5*%a%', NULL, NULL),
(117, 25, 'Copper 1mm', 'Copper 1mm', NULL, '0.5*%a%', NULL, NULL),
(118, 25, 'Copper 1.5mm', 'Copper 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(119, 25, 'Copper 2mm', 'Copper 2mm', NULL, '0.6*%a%', NULL, NULL),
(120, 25, 'Copper 3mm', 'Copper 3mm', NULL, '0.65*%a%', NULL, NULL),
(121, 25, 'Brass 1mm', 'Brass 1mm', NULL, '0.5*%a%', NULL, NULL),
(122, 25, 'Brass 1.5mm', 'Brass 1.5mm', NULL, '0.55*%a%', NULL, NULL),
(123, 25, 'Brass 2mm', 'Brass 2mm', NULL, '0.6*%a%', NULL, NULL),
(124, 25, 'Brass 3mm', 'Brass 3mm', NULL, '0.65*%a%', NULL, NULL),
(125, 26, '1 color', '1 color', NULL, '80.0', NULL, NULL),
(126, 26, '2 colors', '2 colors', NULL, '160.0', NULL, NULL),
(127, 26, '3 colors', '3 colors', NULL, '240.0', NULL, NULL),
(128, 26, '4 colors', '4 colors', NULL, '320.0', NULL, NULL),
(129, 27, 'Adhesive VHB', 'Adhesive VHB', NULL, '200.0', NULL, NULL),
(130, 27, 'Welded nuts', 'Welded nuts', NULL, '200.0', NULL, NULL),
(131, 27, 'Welded studs', 'Welded studs', NULL, '400.0', NULL, NULL),
(132, 27, 'Holes', 'Holes', NULL, '0.0', NULL, NULL),
(133, 28, 'Acid Etching', 'Acid Etching', NULL, '0.5*%a%', NULL, NULL),
(134, 22, '1mm', '1mm', NULL, '0.0', 93, NULL),
(135, 22, '1.5mm', '1.5mm', NULL, '0.0', 94, NULL),
(136, 22, '2mm', '2mm', NULL, '0.0', 95, NULL),
(137, 22, '3mm', '3mm', NULL, '0.0', 96, NULL),
(138, 29, 'Labour cost - Acrylic', 'Labour cost - Acrylic', NULL, '8.0*%p%', NULL, NULL),
(139, 30, 'Labour cost - Inox', 'Labour cost - Inox', NULL, '8.0*%p%', NULL, NULL),
(140, 31, 'Labour cost - Acrynox', 'Labour cost - Acrynox', NULL, '8.0*%p%', NULL, NULL),
(141, 32, 'Labour cost - Flat Cut', 'Labour cost - Flat Cut', NULL, '8.0*%p%', NULL, NULL),
(142, 33, 'Labour cost - Plaque', 'Labour cost - Plaque', NULL, '8.0*%p%', NULL, NULL),
(143, 14, 'Back-lighting with LED & transformer', 'Back-lighting with LED & transformer', NULL, '10.0*(%p%/2)', NULL, NULL),
(144, 12, 'Polished rose gold titanium', 'Polished rose gold titanium', 70, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(145, 12, 'Satin rose gold titanium', 'Satin rose gold titanium', 80, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(146, 12, 'Polished copper titanium \r\n', 'Polished copper titanium \r\n', 90, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(147, 12, 'Satin copper titanium \r\n', 'Satin copper titanium \r\n', 100, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(148, 12, 'Polished bronze titanium\r\n', 'Polished bronze titanium\r\n\r\n', 110, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(149, 12, 'Satin bronze titanium\r\n\r\n', 'Satin bronze titanium', 120, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(150, 12, 'Polished black titanium\r\n', 'Polished black titanium\r\n', 130, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(151, 12, 'Satin black titanium\r\n', 'Satin black titanium\r\n', 140, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(152, 12, 'Antique brass', 'Antique brass', 190, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(153, 12, 'Patinated brass\r\n', 'Patinated brass\r\n', 200, '0.6*(%a%+(%d%*%p%))', NULL, NULL),
(154, 13, 'White diffusing acrylic back\r\n', 'White diffusing acrylic back\r\n', NULL, '0.15*%a%', NULL, NULL),
(155, 13, 'Stainless steel back\r\n', 'Stainless steel back\r\n', NULL, '0.3*%a%', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isSameAsBilling` tinyint(1) DEFAULT NULL,
  `contactName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phoneNumber` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faxNumber` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNumber` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instantMessaging` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `adminFeeEur` decimal(10,2) DEFAULT NULL,
  `adminFeeUsd` decimal(10,2) DEFAULT NULL,
  `homeMessageEn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `homeMessageFr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billingAddress_id` int(11) DEFAULT NULL,
  `shippingAddress_id` int(11) DEFAULT NULL,
  `transportRemoteAreaSurcharge` tinyint(1) DEFAULT NULL,
  `transportFuelSurcharge` tinyint(1) DEFAULT NULL,
  `transportSafetySurcharge` tinyint(1) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4FBF094FBDE815F8` (`clientCode`),
  UNIQUE KEY `UNIQ_4FBF094F43656FE6` (`billingAddress_id`),
  UNIQUE KEY `UNIQ_4FBF094FB1835C8F` (`shippingAddress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `clientCode`, `name`, `type`, `isSameAsBilling`, `contactName`, `phoneNumber`, `faxNumber`, `mobileNumber`, `email`, `website`, `instantMessaging`, `unit`, `currency`, `discount`, `adminFeeEur`, `adminFeeUsd`, `homeMessageEn`, `homeMessageFr`, `billingAddress_id`, `shippingAddress_id`, `transportRemoteAreaSurcharge`, `transportFuelSurcharge`, `transportSafetySurcharge`, `internal`) VALUES
(1, 'SINX', 'Signinox Co., Ltd.', 'M', 1, 'Signinox Admin', '+(66) 2948 4190', '+(66) 2948 4192', '-', 'ylejan@signinox.com', 'www.signinox.com', 'www.signinox.com', 'imperial', 'usd', '10.00', '10.00', '15.00', '', '', 1, 2, 1, 1, 1, 1),
(2, 'SIFQ', 'Signifique Co., Ltd.', 'M', 1, 'Signifique Admin', '+(66) 2948 4190', '+(66) 2948 4192', '-', 'ylejan@signinox.com', NULL, 'www.signinox.com', 'imperial', 'usd', '10.00', '10.00', '15.00', '', '', 3, 4, 1, 1, 1, 1),
(3, 'CCCC', 'Company # cccc', 'R', 1, 'cccc', 'cccc', 'cccc', 'cccc', 'mr_tukkae1@hotmail.com', 'cccc', 'cccc', 'imperial', 'usd', '5.00', '20.00', '20.00', 'cccc', 'cccc', 5, 6, 1, 1, 1, 0),
(4, 'DDDD', 'Company # dddd', 'W', 1, 'dddd dddd', 'dddd', 'dddd', 'dddd', 'mr_tukkae@hotmail.com', 'dddd', 'dddd', 'imperial', 'usd', '33.00', '33.00', '33.00', '33', '33', 7, 8, 1, 1, 1, 0),
(5, 'EEEE', 'Company # eeee', 'W', 1, 'eeee eeee', 'eeee', 'eeee', 'eeee', 'mr_tukkae@hotmail.com', 'eeee', 'eeee', 'imperial', 'usd', '50.00', '50.00', '50.00', 'eeeee', 'eeee', 9, 10, 1, 1, 1, 0),
(6, 'FFFF', 'Test Enseigne Co, Ltd', 'A', 1, 'Gérard', '0988 452 092', 'n/a', '0988 452 092', 'gderoland@signifique.com', 'www.testenseigne.com', 'gdr974', 'metric', 'eur', '5.00', '0.00', '0.00', 'hello / สวัสดี', 'bonjour / สวัสดี', 11, 12, 0, 1, 0, 0),
(7, 'COMP', 'company[name]', 'M', 1, 'qqqq qqqq', 'company[phoneNumber]', 'company[faxNumber]', 'company[mobileNumber]', 'mr_tukkae@hotmail.com', 'company[website]', 'company[instantMessaging]', 'metric', 'eur', '11.00', '11.00', '11.00', 'company[homeMessageEn]', 'company[homeMessageFr]', 13, 14, 1, 1, 1, 0),
(8, '1234', 'Brians Test Company', 'M', 1, 'brian', '0818099073', NULL, NULL, 'brian@ascentec.net', NULL, NULL, 'metric', 'eur', '5.00', '10.00', '10.00', NULL, NULL, 15, 16, 1, 1, 1, 0),
(9, 'YOYO', 'yoyoyo Yeye', 'A', 1, 'yoyoy ye', '0818084788', '0818084787', '0818084789', 'yoyo@example.com', 'company[website]', 'company[instantMessaging]', 'metric', 'eur', '0.00', '20.00', '20.00', 'Yoyo yeye', 'yoyo yeye', 17, 18, 1, 1, 1, 0),
(10, 'ACT', 'Ascentec Technologies .Ltd', 'A', 0, 'Nattapon T.', '+66-8180-88888', '+66-0223-44444', '+66-8180-88888', 'nattapon@ascentec.net', 'www.ascentec.net', 'Hello Ascentec', 'metric', 'eur', '0.00', '0.00', '0.00', NULL, NULL, 19, 20, 1, 1, 1, 0),
(11, 'APPP', 'IStudio Thailand Distributor', 'R', 1, 'Copperwired Admin', '02-160-5931', '02-160-5931', '02-160-5931', 'mr_tukkae@hotmail.com', 'http://www.mustlovemac.co', 'http://www.mustlovemac.co', 'metric', 'eur', '5.00', '10.00', '5.00', NULL, NULL, 21, 22, 1, 1, 1, 0),
(12, 'YYY', 'yyy', 'M', 1, 'Copperwired Admin', '02-160-5931', '02-160-5931', '02-160-5931', 'mr_tukkaesss', 'www.ascentec.net', 'http://www.mustlovemac.co', 'metric', 'eur', '5.00', '5.00', '5.00', 'asdf', 'adf', 23, 24, 1, 1, 1, 0),
(13, '333', 'aaaa', 'M', 1, '4444', '4444', '4444', '4444', '4444@hotmail.com1', '4444', '4444', 'metric', 'eur', '4.00', '4.00', '4.00', NULL, NULL, 25, 26, 1, 1, 1, 0),
(14, '555', '5555', 'M', 1, '555', '555', '55', '555', '5555@hotmail.com', '555', '555', 'metric', 'eur', '0.00', '0.00', '0.00', '555', '555', 27, 28, 1, 1, 1, 0),
(16, '5558', 'adfadfsdf', 'M', 1, 'asdfa', 'dfadf', 'asdfas', 'dfasdfa', 'dsfa@hotmail.com', 'adf', 'ad', 'metric', 'eur', NULL, NULL, NULL, '33', '33', 31, 32, 1, 1, 1, 0),
(17, 'ATM', 'Artemis S.A.', 'M', 1, 'Yann', '02 12 34 56 78', '02 12 34 56 79', '02 12 34 56 78', 'sales@signinox.com', 'www.signinox.com', NULL, 'metric', 'eur', '0.00', '0.00', '0.00', NULL, NULL, 33, 34, 1, 1, 1, 0),
(18, '54e1ad1252bf0', 'Power User Company', 'M', 1, '123-4567-854', '123-4567-854', '123-4567-854', '123-4567-854', 'poweruser@hotmail.com', NULL, NULL, 'metric', 'eur', '0.00', '0.00', '0.00', NULL, NULL, 35, 36, 1, 1, 1, 0),
(19, '5519236355ecc', 'MARTIN SIGN', 'M', 1, 'Christophe Martin', '66 92 639 72 72', NULL, NULL, 'christophemartinthailand@gmail.com', NULL, NULL, 'metric', 'eur', '5.00', NULL, NULL, NULL, NULL, 39, 40, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_detail`
--

CREATE TABLE IF NOT EXISTS `company_detail` (
  `company_id` int(11) NOT NULL,
  `bankAccountInfo` longtext COLLATE utf8_unicode_ci,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quoteCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company_detail`
--

INSERT INTO `company_detail` (`company_id`, `bankAccountInfo`, `logo`, `quoteCode`) VALUES
(1, 'Account Name: Signinox Co., Ltd.					\r\nAccount Number: 066-7-07447-0					\r\nBank Name: Bangkok Bank Public Company Limited					\r\nBank Address: 1448/13 Soi. Ladprao 87 (Chantrasuk),					\r\nPraditmanutham Rd., Khlong Chan, Bangkapi, Bangkok 10240, Thailand					\r\nBank Branch: Crystal Design Center					\r\nBank SWIFT Code: BKKBTHBK', 'logo_signinox.png', 'SX'),
(2, 'Bank Name: Bangkok Bank Public Company Limited					\r\nBank address: 1448/13 Soi. Ladprao 87 (Chantrasuk),					\r\nPraditmanutham Rd., Khlong Chan, Bangkapi, Bangkok 10240, THAILAND					\r\nBank Branch: Crystal Design Center					\r\nAccount Name: Signifique Co., Ltd.					\r\nAccount Number: 066-7-07450-4					\r\nBank SWIFT Code: BKKBTHBK', 'logo_signifique.png', 'SN');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fromCurrency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `toCurrency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,5) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6956883F5E237E06` (`name`),
  UNIQUE KEY `UNIQ_6956883F989D9B62` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `fromCurrency`, `toCurrency`, `rate`, `slug`) VALUES
(1, 'THB / EUR', 'thb', 'eur', '0.02690', 'thbeur'),
(2, 'THB / USD', 'thb', 'usd', '0.03070', 'thbusd'),
(3, 'EUR / THB', 'eur', 'thb', '37.20320', 'eurthb'),
(5, 'USD / THB', 'usd', 'thb', '32.54820', 'usdthb');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `path`) VALUES
(1, 'eee', '19beb7e58bf09728dfcd79b31dfc86d4a568a08e.jpeg'),
(2, 'eee', '5a3deea002b69e95f1491259d8c12cc2ae47a580.jpeg'),
(3, 'eeee', '2ee1795882dc432df3d9c120a3d010db3d2bc5a5.jpeg'),
(4, 'pppp', '2e34c23280ad0e639c1d77dc6f31c66b868d18e6.jpeg'),
(5, NULL, '099b460c9f5dbdbf8ea7a5eebf2d690e79f13d49.jpeg'),
(6, 'example for logo', 'c30fe47b634a944bf0b8625e2e0612f0a277a5b9.jpeg'),
(7, 'Dota2 Logo', '48b3ff8f5ee1b3d63e5a8807a2ab31a62537b49d.jpeg'),
(8, 'screenshot example', 'a083a49dc91f6790e7904f1025c0118cd956df7a.png'),
(9, 'eee', '114c50d2cb790a0f2dbf23d0966fe2953f36ad9c.jpeg'),
(10, '111', '7b6c2fb960a17d90d7c37cc220e795b6d87f156f.jpeg'),
(11, 'eee', '73b9f5bd3d9381b9fa8968a39bb3c0c4f533fe74.jpeg'),
(12, 'ww', '3fd3ad0777ce05b3462f616648a5cb5dfc803cee.jpeg'),
(13, 'example', '567cf8285be3e067a244be0b64aa32b5eb65567e.png'),
(14, NULL, 'png'),
(15, NULL, 'png'),
(16, NULL, 'png'),
(17, NULL, 'png'),
(18, NULL, 'jpeg'),
(19, NULL, 'png'),
(20, NULL, 'jpeg'),
(21, NULL, 'jpeg'),
(22, NULL, 'jpeg'),
(23, NULL, 'jpeg'),
(24, NULL, 'pptx'),
(25, NULL, 'pdf'),
(26, NULL, 'pdf'),
(27, NULL, 'pdf'),
(28, NULL, 'pdf'),
(29, NULL, 'pdf'),
(30, NULL, 'pdf'),
(31, NULL, 'pptx'),
(32, NULL, 'pptx'),
(33, NULL, 'pptx'),
(34, NULL, 'pdf'),
(35, NULL, 'pdf'),
(36, NULL, 'pdf'),
(37, NULL, 'pdf'),
(38, NULL, 'pdf'),
(39, NULL, 'pptx'),
(40, NULL, 'pptx'),
(41, NULL, 'pptx'),
(42, NULL, 'pptx'),
(43, NULL, 'pptx'),
(44, NULL, 'pptx');

-- --------------------------------------------------------

--
-- Table structure for table `ext_translations`
--

CREATE TABLE IF NOT EXISTS `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=184 ;

--
-- Dumping data for table `ext_translations`
--

INSERT INTO `ext_translations` (`id`, `locale`, `object_class`, `field`, `foreign_key`, `content`) VALUES
(1, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '1', 'Epaisseur'),
(2, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '2', 'Led'),
(3, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '3', 'Consommables (peinture/potting)'),
(4, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '4', 'Surface éclairée'),
(5, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '5', 'Options (Face)'),
(6, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '6', 'Coût main doeuvre'),
(7, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '8', 'Règles supplémentaires'),
(8, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '9', 'Options (Chant)'),
(9, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '10', 'Type de face'),
(10, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '11', 'Design spécial'),
(11, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '12', 'Matière / Finition'),
(12, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '13', 'Fixation'),
(13, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '14', 'Eclairage'),
(14, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '15', 'Epaisseur Lettre'),
(15, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '16', 'Matériaux Couverture Face'),
(16, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '17', 'Chant'),
(17, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '18', 'Options'),
(18, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '19', 'Led - Acrynox'),
(19, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '20', 'Consommables (peinture, potting, etc…)'),
(20, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '21', 'Matières Premières'),
(21, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '22', 'Epaisseur'),
(22, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '23', 'Fixations'),
(23, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '24', 'Service Découpe Laser'),
(24, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '25', 'Matières Premières'),
(25, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '26', 'Couleurs Logo/Texte'),
(26, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '27', 'Fixations'),
(27, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Category', 'name', '28', 'Gravure a lacide'),
(30, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '1', 'Acrylique 20mm'),
(31, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '2', 'Acrylique 30mm'),
(32, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '3', 'Led (Chant)'),
(33, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '4', 'Consommables (peinture/potting)'),
(34, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '5', 'Face Avant Diffusante'),
(35, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '6', 'Face Inox'),
(36, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '7', 'Film Diffusant 3M'),
(37, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '8', 'Chant Inox'),
(38, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '9', 'Film Diffusant 3M Chant'),
(39, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '10', 'Main dOeuvre'),
(40, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '11', 'Chant Lumineux (50%)'),
(41, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '12', 'Chant Lumineux (100%)'),
(42, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '13', 'Rétroéclairé'),
(43, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '14', 'Eclairage Liseret'),
(44, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '15', 'Face Avant Diffusante + 50% Chant Lumineux'),
(45, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '16', 'Face Avant Diffusante & Retroéclairage'),
(46, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '17', 'Retroéclairage avec Chant Lumineux'),
(47, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '18', 'Led (Face)'),
(48, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '19', 'Service Découpe Laser (Inox)'),
(49, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '20', 'Standard'),
(50, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '21', 'Acrylique diffusant avec listel\r\n'),
(51, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '22', 'Prismatique'),
(52, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '23', 'Serif'),
(53, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '24', 'Inox poli miroir (304 : standard)\r\n'),
(54, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '25', 'Inox brossé (304 : standard)\r\n'),
(55, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '26', 'Inox poli miroir (316L : usage bord de mer)\r\n'),
(56, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '27', 'Inox brossé (316L : usage bord de mer)\r\n'),
(57, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '28', 'Inox titanium or poli miroir'),
(58, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '29', 'Inox titanium or brossé'),
(59, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '30', 'Laiton Poli Miroir'),
(60, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '31', 'Laiton Brossé'),
(61, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '32', 'Cuivre Poli Miroir'),
(62, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '33', 'Cuivre Brossé'),
(63, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '34', 'Acier effet rouille\r\n'),
(64, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '35', 'Aluminium Peint (Toute couleur disponible)'),
(65, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '36', 'Acier galvanisé peint\r\n'),
(66, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '37', 'Inserts avec Rivets'),
(67, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '38', 'Plaque de Collage avec Adhésif VHB'),
(68, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '39', 'Ecrous Soudés'),
(69, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '40', 'Semelle Polycarbonate Transparente'),
(70, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '41', 'Semelle PVC (Blanc)'),
(71, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '42', 'Eclairage face par modules LED + transformateur \r\n'),
(72, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '43', '20mm'),
(73, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '44', 'Inox Poli Miroir (304)'),
(74, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '45', 'Inox Brossé(304)'),
(75, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '46', 'Inox Poli Miroir (316L)'),
(76, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '47', 'Inox Brossé (316L)'),
(77, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '48', 'Inox Titanium Poli Miroir'),
(78, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '49', 'Inox Brossé Titanium'),
(79, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '50', 'Laiton Poli Miroir'),
(80, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '51', 'Laiton Brossé'),
(81, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '52', 'Cuivre Poli Miroir'),
(82, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '53', 'Cuivre Brossé'),
(83, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '54', 'Inox Peint'),
(84, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '55', 'Aluminium Peint'),
(85, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '56', 'Acier Galvanisé Peint'),
(86, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '57', '30mm'),
(87, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '58', '10mm'),
(88, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '59', '15mm'),
(89, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '60', 'Film Diffusant 3M Chant'),
(90, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '61', 'Led - Chant (Acrynox)'),
(91, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '62', 'Inox Poli Miroir (304) 1mm'),
(92, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '63', 'Inox Poli Miroir (304) 1.5mm'),
(93, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '64', 'Inox Poli Miroir (304) 2mm'),
(94, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '65', 'Inox Poli Miroir (304) 3mm'),
(95, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '66', 'Inox Brossé (304) 1mm'),
(96, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '67', 'Inox Brossé  (304) 1.5mm'),
(97, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '68', 'Inox Brossé  (304) 2mm'),
(98, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '69', 'Inox Brossé (304) 3mm'),
(99, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '70', 'Inox Poli Miroir (316L) 1mm'),
(100, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '71', 'Inox Poli Miroir (316L) 1.5mm'),
(101, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '72', 'Inox Poli Miroir (316L) 2mm'),
(102, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '73', 'Inox Poli Miroir (316L) 3mm'),
(103, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '74', 'Inox Brossé (316L) 1mm'),
(104, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '75', 'Inox Brossé (316L) 1.5mm'),
(105, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '76', 'Inox Brossé (316L) 2mm'),
(106, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '77', 'Inox Brossé (316L) 3mm'),
(107, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '78', 'Inox Titanium 1mm'),
(108, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '79', 'Inox Titanium 1.5mm'),
(109, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '80', 'Inox Titanium 2mm'),
(110, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '81', 'Inox Titanium 3mm'),
(111, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '82', 'Cuivre 1mm'),
(112, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '83', 'Cuivre 1.5mm'),
(113, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '84', 'Cuivre 2mm'),
(114, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '85', 'Cuivre 3mm'),
(115, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '86', 'Laiton 1mm'),
(116, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '87', 'Laiton 1.5mm'),
(117, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '88', 'Laiton 2mm'),
(118, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '89', 'Laiton 3mm'),
(119, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '90', 'Ecrous Soudés'),
(120, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '91', 'Tiges Filetées Soudées'),
(121, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '92', 'Adhésif VHB'),
(122, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '93', 'Découpe Laser 1mm'),
(123, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '94', 'Découpe Laser 1.5mm'),
(124, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '95', 'Découpe Laser 2mm'),
(125, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '96', 'Découpe Laser 3mm'),
(126, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '97', 'Inox Poli Miroir (304) 1mm'),
(127, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '98', 'Inox Poli Miroir (304) 1.5mm'),
(128, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '99', 'Inox Poli Miroir (304) 2mm'),
(129, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '100', 'Inox Poli Miroir (304) 3mm'),
(130, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '101', 'Inox Brossé (304) 1mm'),
(131, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '102', 'Inox Brossé (304) 1.5mm'),
(132, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '103', 'Inox Brossé (304) 2mm'),
(133, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '104', 'Inox Brossé (304) 3mm'),
(134, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '105', 'Inox Poli Miroir (316L) 1mm'),
(135, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '106', 'Inox Poli Miroir (316L) 1.5mm'),
(136, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '107', 'Inox Poli Miroir (316L) 2mm'),
(137, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '108', 'Inox Poli Miroir (316L) 3mm'),
(138, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '109', 'Inox Brossé (316L) 1mm'),
(139, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '110', 'Inox Brossé (316L) 1.5mm'),
(140, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '111', 'Inox Brossé (316L) 2mm'),
(141, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '112', 'Inox Brossé (316L) 3mm'),
(142, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '113', 'Inox Titanium 1mm'),
(143, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '114', 'Inox Titanium 1.5mm'),
(144, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '115', 'Inox Titanium 2mm'),
(145, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '116', 'Inox Titanium 3mm'),
(146, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '117', 'Cuivre 1mm'),
(147, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '118', 'Cuivre 1.5mm'),
(148, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '119', 'Cuivre 2mm'),
(149, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '120', 'Cuivre 3mm'),
(150, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '121', 'Laiton 1mm'),
(151, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '122', 'Laiton 1.5mm'),
(152, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '123', 'Laiton 2mm'),
(153, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '124', 'Laiton 3mm'),
(154, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '125', '1 couleur'),
(155, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '126', '2 couleurs'),
(156, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '127', '3 couleurs'),
(157, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '128', '4 couleurs'),
(158, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '129', 'Adhésif VHB'),
(159, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '130', 'Ecrous Soudés'),
(160, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '131', 'Tiges Filetées Soudées'),
(161, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '132', 'Trous'),
(162, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '133', 'Gravure'),
(163, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '134', '1mm'),
(164, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '135', '1.5mm'),
(165, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '136', '2mm'),
(166, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '137', '3mm'),
(167, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '154', 'Semelle acrylique blanc diffusan'),
(168, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '155', 'Semelle inox\r\n'),
(169, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Product', 'name', '2', 'Lettres boitier\r\n'),
(170, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '144', 'Titanium or rose poli miroir\r\n'),
(171, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '145', 'Titanium or rose brossé\r\n'),
(172, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '146', 'Titanium cuivre poli miroir\r\n'),
(173, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '147', 'Titanium cuivre brossé\r\n'),
(174, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '148', 'Titanium bronze poli miroir\r\n'),
(175, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '149', 'Titanium bronze brossé\r\n'),
(176, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '150', 'Titanium noir poli miroir\r\n'),
(177, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '151', 'Titanium noir brossé\r\n'),
(178, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '152', 'Laiton antique\r\n'),
(179, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '153', 'Laiton patiné'),
(180, 'fr', 'Quotemax\\DashboardBundle\\Entity\\CategoryValue', 'name', '143', 'Retro-éclairage par modules LED + transformateur\r\n'),
(181, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Product', 'name', '5', 'Plaques gravées'),
(182, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Product', 'name', '4', 'Lettres découpées'),
(183, 'fr', 'Quotemax\\DashboardBundle\\Entity\\Product', 'name', '1', 'Acrylux (PMMA 30 + LED)');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productType` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` decimal(10,2) NOT NULL,
  `width` decimal(10,2) NOT NULL,
  `depth` decimal(10,2) NOT NULL,
  `perimeter` decimal(10,2) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `measureUnit` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `surface` decimal(10,2) DEFAULT NULL,
  `additionalCost` decimal(10,2) NOT NULL,
  `margin` decimal(10,2) NOT NULL,
  `priceSelling` decimal(10,2) NOT NULL,
  `priceSellingTotalAll` decimal(10,2) NOT NULL,
  `numOfLed` int(11) DEFAULT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1F1B251EDB805178` (`quote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=250 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `quote_id`, `name`, `productType`, `height`, `width`, `depth`, `perimeter`, `quantity`, `currency`, `measureUnit`, `price`, `surface`, `additionalCost`, `margin`, `priceSelling`, `priceSellingTotalAll`, `numOfLed`, `type`) VALUES
(77, 47, 'Letter # 1 - ''A''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '25580.00', '10.00', '0.00', '5116.00', '30696.00', '306960.00', NULL, NULL),
(78, 47, 'Letter # 2 - ''B''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '25580.00', '10.00', '0.00', '5116.00', '30696.00', '306960.00', NULL, NULL),
(79, 47, 'Letter # 3 - ''C''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '25580.00', '10.00', '0.00', '5116.00', '30696.00', '306960.00', NULL, NULL),
(80, 47, 'GraphicElement # 1', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '25580.00', '10.00', '0.00', '5116.00', '30696.00', '306960.00', NULL, NULL),
(81, 48, 'Letter # 1 - ''F''', NULL, '10.00', '10.00', '3.00', '1000.00', 20, 'eur', 'metric', '9910.00', '10.00', '5000.00', '1982.00', '16892.00', '337840.00', NULL, NULL),
(82, 48, 'Letter # 2 - ''G''', NULL, '10.00', '10.00', '3.00', '1000.00', 20, 'eur', 'metric', '9910.00', '10.00', '5000.00', '1982.00', '16892.00', '337840.00', NULL, NULL),
(83, 48, 'Letter # 3 - ''H''', NULL, '10.00', '10.00', '3.00', '1000.00', 20, 'eur', 'metric', '9910.00', '10.00', '5000.00', '1982.00', '16892.00', '337840.00', NULL, NULL),
(84, 48, 'GraphicElement # 1', NULL, '10.00', '10.00', '3.00', '1000.00', 20, 'eur', 'metric', '9910.00', '10.00', '5000.00', '1982.00', '16892.00', '337840.00', NULL, NULL),
(85, 49, 'Letter # 1 - ''H''', NULL, '10.00', '10.00', '3.00', '1000.00', 30, 'eur', 'metric', '24180.00', '10.00', '0.00', '4836.00', '29016.00', '870480.00', NULL, NULL),
(86, 49, 'Letter # 2 - ''J''', NULL, '10.00', '10.00', '3.00', '1000.00', 30, 'eur', 'metric', '24180.00', '10.00', '0.00', '4836.00', '29016.00', '870480.00', NULL, NULL),
(87, 49, 'Letter # 3 - ''K''', NULL, '10.00', '10.00', '3.00', '1000.00', 30, 'eur', 'metric', '24180.00', '10.00', '0.00', '4836.00', '29016.00', '870480.00', NULL, NULL),
(88, 49, 'GraphicElement # 1', NULL, '10.00', '10.00', '3.00', '1000.00', 30, 'eur', 'metric', '24180.00', '10.00', '0.00', '4836.00', '29016.00', '870480.00', NULL, NULL),
(89, 50, 'Letter # 1 - ''X''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '13465.00', '10.00', '0.00', '2693.00', '16158.00', '161580.00', NULL, NULL),
(90, 50, 'Letter # 2 - ''Y''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '13465.00', '10.00', '0.00', '2693.00', '16158.00', '161580.00', NULL, NULL),
(91, 50, 'Letter # 3 - ''Z''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '13465.00', '10.00', '0.00', '2693.00', '16158.00', '161580.00', NULL, NULL),
(92, 50, 'GraphicElement # 1', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'eur', 'metric', '13465.00', '10.00', '0.00', '2693.00', '16158.00', '161580.00', NULL, NULL),
(108, 58, 'Letter # 1 - ''2''', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'usd', 'metric', '13450.00', NULL, '0.00', '2690.00', '16140.00', '161400.00', NULL, NULL),
(109, 58, 'GraphicElement # 1', NULL, '10.00', '10.00', '3.00', '1000.00', 10, 'usd', 'metric', '13450.00', NULL, '0.00', '2690.00', '16140.00', '161400.00', NULL, NULL),
(110, 59, 'Letter # 1 - ''Y''', NULL, '20.00', '20.00', '6.00', '2000.00', 20, 'usd', 'metric', '23810.00', NULL, '10000.00', '4762.00', '38572.00', '771440.00', NULL, NULL),
(111, 59, 'Letter # 2 - ''O''', NULL, '20.00', '20.00', '6.00', '2000.00', 20, 'usd', 'metric', '23810.00', NULL, '10000.00', '4762.00', '38572.00', '771440.00', NULL, NULL),
(112, 59, 'GraphicElement # 1', NULL, '20.00', '20.00', '6.00', '2000.00', 20, 'usd', 'metric', '23810.00', NULL, '10000.00', '4762.00', '38572.00', '771440.00', NULL, NULL),
(113, 60, 'Letter # 1 - ''N''', NULL, '20.00', '25.00', '3.00', '100.00', 10, 'eur', 'metric', '2300.00', '10.00', '0.00', '460.00', '2760.00', '27600.00', NULL, NULL),
(114, 60, 'Letter # 2 - ''I''', NULL, '20.00', '25.00', '3.00', '100.00', 10, 'eur', 'metric', '2300.00', '10.00', '0.00', '460.00', '2760.00', '27600.00', NULL, NULL),
(115, 60, 'Letter # 3 - ''K''', NULL, '20.00', '25.00', '3.00', '100.00', 10, 'eur', 'metric', '2300.00', '10.00', '0.00', '460.00', '2760.00', '27600.00', NULL, NULL),
(116, 60, 'Letter # 4 - ''E''', NULL, '20.00', '25.00', '3.00', '100.00', 10, 'eur', 'metric', '2300.00', '10.00', '0.00', '460.00', '2760.00', '27600.00', NULL, NULL),
(117, 60, 'GraphicElement # 1', NULL, '20.00', '25.00', '3.00', '100.00', 10, 'eur', 'metric', '2300.00', '10.00', '0.00', '460.00', '2760.00', '27600.00', NULL, NULL),
(118, 61, 'Letter # 1 - ''P''', NULL, '20.00', '25.00', '3.00', '100.00', 1, 'eur', 'metric', '1250.00', NULL, '500.00', '250.00', '2000.00', '2000.00', NULL, NULL),
(119, 61, 'Letter # 2 - ''U''', NULL, '20.00', '25.00', '3.00', '100.00', 1, 'eur', 'metric', '1250.00', NULL, '500.00', '250.00', '2000.00', '2000.00', NULL, NULL),
(120, 61, 'Letter # 3 - ''M''', NULL, '20.00', '25.00', '3.00', '100.00', 1, 'eur', 'metric', '1250.00', NULL, '500.00', '250.00', '2000.00', '2000.00', NULL, NULL),
(121, 61, 'Letter # 4 - ''A''', NULL, '20.00', '25.00', '3.00', '100.00', 1, 'eur', 'metric', '1250.00', NULL, '500.00', '250.00', '2000.00', '2000.00', NULL, NULL),
(128, 63, 'Letter # 1 - ''R''', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(129, 63, 'Letter # 2 - ''e''', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(130, 63, 'Letter # 3 - ''e''', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(131, 63, 'Letter # 4 - ''b''', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(132, 63, 'Letter # 5 - ''o''', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(133, 63, 'Letter # 6 - ''k''', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(134, 63, 'GraphicElement # 1', NULL, '20.00', '25.00', '1.00', '100.00', 1, 'eur', 'metric', '1450.00', NULL, '0.00', '290.00', '1740.00', '1740.00', NULL, NULL),
(135, 64, 'GraphicElement # 1', NULL, '30.00', '50.00', '0.20', '150.00', 1, 'eur', 'metric', '3360.00', NULL, '0.00', '672.00', '4032.00', '4032.00', NULL, NULL),
(139, 67, 'Letter # 1 - ''C''', NULL, '20.00', '25.00', '3.00', '100.00', 2, 'eur', 'metric', '2300.00', '10.00', '0.00', '460.00', '2760.00', '5520.00', 5, NULL),
(140, 67, 'Letter # 2 - ''O''', NULL, '20.00', '25.00', '3.00', '120.00', 2, 'eur', 'metric', '2310.00', '8.00', '0.00', '462.00', '2772.00', '5544.00', 4, NULL),
(141, 68, 'Letter # 1 - ''C''', NULL, '20.00', '25.00', '3.00', '100.00', 2, 'eur', 'metric', '1250.00', NULL, '500.00', '250.00', '2000.00', '4000.00', 30, NULL),
(142, 68, 'Letter # 2 - ''O''', NULL, '20.00', '25.00', '3.00', '120.00', 2, 'eur', 'metric', '1440.00', NULL, '600.00', '288.00', '2328.00', '4656.00', 36, NULL),
(143, 69, 'Letter # 1 - ''C''', NULL, '25.40', '25.00', '3.00', '100.00', 2, 'eur', 'imperial', '3611.25', '10.00', '0.00', '722.25', '4333.50', '8667.00', 30, NULL),
(144, 69, 'Letter # 2 - ''O''', NULL, '20.32', '25.00', '3.00', '120.00', 2, 'eur', 'imperial', '3889.00', '8.00', '0.00', '777.80', '4666.80', '9333.60', 36, NULL),
(145, 70, 'Letter # 1 - ''C''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1050.00', NULL, '500.00', '210.00', '1760.00', '1760.00', 30, NULL),
(146, 70, 'Letter # 2 - ''O''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1050.00', NULL, '500.00', '210.00', '1760.00', '1760.00', 30, NULL),
(147, 71, 'Letter # 1 - ''5''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1870.00', '10.00', '0.00', '374.00', '2244.00', '2244.00', 5, NULL),
(148, 71, 'Letter # 2 - ''5''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1870.00', '10.00', '0.00', '374.00', '2244.00', '2244.00', 5, NULL),
(153, 74, 'Letter # 1 - ''C''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1790.00', '10.00', '0.00', '358.00', '2148.00', '2148.00', 5, NULL),
(154, 74, 'Letter # 2 - ''O''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1790.00', '10.00', '0.00', '358.00', '2148.00', '2148.00', 5, NULL),
(155, 75, 'Letter # 1 - ''C''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1050.00', NULL, '500.00', '210.00', '1760.00', '1760.00', 30, NULL),
(156, 75, 'Letter # 2 - ''O''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '1050.00', NULL, '500.00', '210.00', '1760.00', '1760.00', 30, NULL),
(157, 76, 'Letter # 1 - ''C''', NULL, '100.00', '100.00', '30.00', '100.00', 1, 'eur', 'metric', '7300.00', NULL, '0.00', '1460.00', '8760.00', '8760.00', 0, 'lt'),
(158, 76, 'Letter # 2 - ''O''', NULL, '100.00', '100.00', '30.00', '100.00', 1, 'eur', 'metric', '7300.00', NULL, '0.00', '1460.00', '8760.00', '8760.00', 0, 'lt'),
(159, 76, 'Letter # 3 - ''T''', NULL, '100.00', '100.00', '30.00', '100.00', 1, 'eur', 'metric', '7300.00', NULL, '0.00', '1460.00', '8760.00', '8760.00', 0, 'lt'),
(160, 77, 'Letter # 1 - ''C''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '8515.00', '100.00', '0.00', '1703.00', '10218.00', '10218.00', 50, 'lt'),
(161, 77, 'Letter # 2 - ''O''', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '8515.00', '100.00', '0.00', '1703.00', '10218.00', '10218.00', 50, 'lt'),
(162, 77, 'GraphicElement # yo', NULL, '10.00', '10.00', '3.00', '100.00', 1, 'eur', 'metric', '8515.00', '100.00', '0.00', '1703.00', '10218.00', '10218.00', 50, 'lg'),
(163, 78, 'Letter # 1 - ''D''', NULL, '23.00', '12.00', '3.00', '43.00', 1, 'eur', 'metric', '776.80', NULL, '0.00', '155.36', '932.16', '932.16', 0, 'lt'),
(164, 78, 'Letter # 2 - ''K''', NULL, '23.00', '12.00', '3.00', '43.00', 1, 'eur', 'metric', '776.80', NULL, '0.00', '155.36', '932.16', '932.16', 0, 'lt'),
(165, 78, 'Letter # 3 - ''Y''', NULL, '26.00', '5.00', '7.00', '43.00', 1, 'eur', 'metric', '670.40', NULL, '0.00', '134.08', '804.48', '804.48', 0, 'lt'),
(166, 79, 'Letter # 1 - ''D''', NULL, '50.00', '12.00', '10.00', '89.00', 1, 'eur', 'metric', '2502.00', NULL, '0.00', '500.40', '3002.40', '3002.40', 0, 'lt'),
(167, 79, 'Letter # 2 - ''r''', NULL, '50.00', '13.00', '3.00', '43.00', 1, 'eur', 'metric', '1598.50', NULL, '0.00', '319.70', '1918.20', '1918.20', 0, 'lt'),
(168, 79, 'Letter # 3 - ''u''', NULL, '100.00', '25.00', '15.00', '43.00', 1, 'eur', 'metric', '4631.50', NULL, '0.00', '926.30', '5557.80', '5557.80', 0, 'lt'),
(169, 79, 'Letter # 4 - ''m''', NULL, '76.00', '22.00', '13.00', '45.00', 1, 'eur', 'metric', '3385.50', NULL, '0.00', '677.10', '4062.60', '4062.60', 0, 'lt'),
(170, 80, 'Letter # 1 - ''D''', NULL, '20.00', '10.00', '2.00', '35.00', 1, 'eur', 'metric', '415.00', NULL, '0.00', '83.00', '498.00', '498.00', 0, 'lt'),
(171, 80, 'Letter # 2 - ''E''', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '325.00', NULL, '0.00', '65.00', '390.00', '390.00', 0, 'lt'),
(172, 80, 'Letter # 3 - ''F''', NULL, '20.00', '10.00', '2.00', '60.00', 1, 'eur', 'metric', '640.00', NULL, '0.00', '128.00', '768.00', '768.00', 0, 'lt'),
(173, 81, 'Letter # 1 - ''C''', NULL, '20.00', '20.00', '2.00', '45.00', 1, 'eur', 'metric', '655.00', NULL, '0.00', '131.00', '786.00', '786.00', 0, 'lt'),
(174, 81, 'Letter # 2 - ''H''', NULL, '20.00', '20.00', '2.00', '80.00', 1, 'eur', 'metric', '970.00', NULL, '0.00', '194.00', '1164.00', '1164.00', 0, 'lt'),
(175, 81, 'Letter # 3 - ''R''', NULL, '20.00', '25.00', '2.00', '80.00', 1, 'eur', 'metric', '1020.00', NULL, '0.00', '204.00', '1224.00', '1224.00', 0, 'lt'),
(176, 81, 'Letter # 4 - ''I''', NULL, '20.00', '4.00', '2.00', '48.00', 1, 'eur', 'metric', '522.00', NULL, '0.00', '104.40', '626.40', '626.40', 0, 'lt'),
(177, 81, 'Letter # 5 - ''S''', NULL, '20.00', '20.00', '2.00', '60.00', 1, 'eur', 'metric', '790.00', NULL, '0.00', '158.00', '948.00', '948.00', 0, 'lt'),
(178, 82, 'Letter # 1 - ''N''', NULL, '20.00', '20.04', '2.00', '99.17', 1, 'eur', 'metric', '1202.84', NULL, '0.00', '240.57', '1443.41', '1443.41', 0, 'lt'),
(179, 82, 'Letter # 2 - ''I''', NULL, '20.00', '12.12', '2.00', '52.17', 1, 'eur', 'metric', '675.40', NULL, '0.00', '135.08', '810.48', '810.48', 0, 'lt'),
(180, 82, 'Letter # 3 - ''K''', NULL, '20.00', '19.79', '2.00', '94.33', 1, 'eur', 'metric', '1155.32', NULL, '0.00', '231.06', '1386.38', '1386.38', 0, 'lt'),
(181, 82, 'Letter # 4 - ''E''', NULL, '20.00', '17.01', '2.00', '81.88', 1, 'eur', 'metric', '1007.42', NULL, '0.00', '201.48', '1208.90', '1208.90', 0, 'lt'),
(182, 82, 'GraphicElement # 1', NULL, '20.00', '55.57', '2.00', '138.31', 1, 'eur', 'metric', '1989.29', NULL, '0.00', '397.86', '2387.15', '2387.15', 0, 'lg'),
(183, 83, 'Letter # 1 - ''P''', NULL, '30.00', '27.00', '3.00', '70.00', 1, 'eur', 'metric', '1768.00', NULL, '350.00', '353.60', '2471.60', '2471.60', 21, 'lt'),
(184, 83, 'Letter # 2 - ''A''', NULL, '30.00', '30.00', '3.00', '90.00', 1, 'eur', 'metric', '2075.00', NULL, '450.00', '415.00', '2940.00', '2940.00', 27, 'lt'),
(185, 83, 'Letter # 3 - ''U''', NULL, '30.00', '30.00', '3.00', '92.00', 1, 'eur', 'metric', '2094.00', NULL, '460.00', '418.80', '2972.80', '2972.80', 28, 'lt'),
(186, 83, 'Letter # 4 - ''L''', NULL, '30.00', '28.00', '3.00', '80.00', 1, 'eur', 'metric', '1902.00', NULL, '400.00', '380.40', '2682.40', '2682.40', 24, 'lt'),
(187, 84, 'GraphicElement # 1', NULL, '50.00', '60.00', '5.00', '250.00', 1, 'eur', 'metric', '7250.00', NULL, '1250.00', '1450.00', '9950.00', '9950.00', 75, 'lg'),
(188, 85, 'Letter # 1 - ''N''', NULL, '20.00', '20.04', '2.00', '99.17', 1, 'eur', 'metric', '1142.93', NULL, '0.00', '228.59', '1371.52', '1371.52', 0, 'lt'),
(189, 85, 'Letter # 2 - ''I''', NULL, '20.00', '12.12', '2.00', '52.17', 1, 'eur', 'metric', '640.73', NULL, '0.00', '128.15', '768.88', '768.88', 0, 'lt'),
(190, 85, 'Letter # 3 - ''K''', NULL, '20.00', '19.79', '2.00', '94.33', 1, 'eur', 'metric', '1096.87', NULL, '0.00', '219.37', '1316.24', '1316.24', 0, 'lt'),
(191, 85, 'Letter # 4 - ''E''', NULL, '20.00', '17.01', '2.00', '81.88', 1, 'eur', 'metric', '957.02', NULL, '0.00', '191.40', '1148.42', '1148.42', 0, 'lt'),
(192, 85, 'GraphicElement # 1', NULL, '20.00', '55.57', '2.00', '138.31', 1, 'eur', 'metric', '1850.49', NULL, '0.00', '370.10', '2220.59', '2220.59', 0, 'lg'),
(193, 86, 'Letter # 1 - ''P''', NULL, '20.00', '15.00', '3.00', '60.00', 1, 'eur', 'metric', '770.00', NULL, '0.00', '154.00', '924.00', '924.00', 0, 'lt'),
(194, 86, 'Letter # 2 - ''W''', NULL, '20.00', '25.00', '3.00', '70.00', 1, 'eur', 'metric', '965.00', NULL, '0.00', '193.00', '1158.00', '1158.00', 0, 'lt'),
(195, 87, 'Letter # 1 - ''T''', NULL, '20.00', '10.00', '3.00', '50.00', 1, 'eur', 'metric', '625.00', NULL, '0.00', '125.00', '750.00', '750.00', 0, 'lt'),
(196, 87, 'Letter # 2 - ''o''', NULL, '20.00', '10.00', '2.00', '50.00', 1, 'eur', 'metric', '600.00', NULL, '0.00', '120.00', '720.00', '720.00', 0, 'lt'),
(197, 87, 'Letter # 3 - ''t''', NULL, '20.00', '10.00', '2.00', '50.00', 1, 'eur', 'metric', '600.00', NULL, '0.00', '120.00', '720.00', '720.00', 0, 'lt'),
(198, 87, 'Letter # 4 - ''o''', NULL, '10.00', '10.00', '2.00', '30.00', 1, 'eur', 'metric', '370.00', NULL, '0.00', '74.00', '444.00', '444.00', 0, 'lt'),
(199, 88, 'Letter # 1 - ''a''', NULL, '20.00', '25.00', '2.00', '45.00', 1, 'eur', 'metric', '30985.00', '400.00', '0.00', '6197.00', '37182.00', '37182.00', 200, 'lt'),
(200, 88, 'Letter # 2 - ''b''', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '15630.00', '200.00', '0.00', '3126.00', '18756.00', '18756.00', 100, 'lt'),
(201, 88, 'Letter # 3 - ''c''', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '15630.00', '200.00', '0.00', '3126.00', '18756.00', '18756.00', 100, 'lt'),
(202, 88, 'GraphicElement # 1', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '14130.00', '180.00', '0.00', '2826.00', '16956.00', '16956.00', 90, 'lg'),
(203, 89, 'Letter # 1 - ''N''', NULL, '20.00', '25.00', '2.00', '45.00', 1, 'eur', 'metric', '930.00', NULL, '0.00', '186.00', '1116.00', '1116.00', 0, 'lt'),
(204, 89, 'Letter # 2 - ''I''', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '500.00', NULL, '0.00', '100.00', '600.00', '600.00', 0, 'lt'),
(205, 89, 'Letter # 3 - ''K''', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '500.00', NULL, '0.00', '100.00', '600.00', '600.00', 0, 'lt'),
(206, 89, 'Letter # 4 - ''E''', NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '500.00', NULL, '0.00', '100.00', '600.00', '600.00', 0, 'lt'),
(207, 90, 'Letter # 1 - ''N''', NULL, '40.00', '20.00', '5.00', '70.00', 1, 'eur', 'metric', '1300.00', NULL, '0.00', '260.00', '1560.00', '1560.00', 0, 'lt'),
(208, 90, 'Letter # 2 - ''I''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(209, 90, 'Letter # 3 - ''K''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(210, 90, 'Letter # 4 - ''E''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(211, 90, 'GraphicElement # 1', NULL, '30.00', '50.00', '8.00', '80.00', 1, 'eur', 'metric', '1974.00', NULL, '0.00', '394.80', '2368.80', '2368.80', 0, 'lg'),
(212, 91, 'Letter # 1 - ''N''', NULL, '40.00', '20.00', '5.00', '70.00', 1, 'eur', 'metric', '1300.00', NULL, '0.00', '260.00', '1560.00', '1560.00', 0, 'lt'),
(213, 91, 'Letter # 2 - ''I''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(214, 91, 'Letter # 3 - ''K''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(215, 91, 'Letter # 4 - ''E''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(216, 91, 'GraphicElement # 1', NULL, '30.00', '50.00', '8.00', '80.00', 1, 'eur', 'metric', '1974.00', NULL, '0.00', '394.80', '2368.80', '2368.80', 0, 'lg'),
(217, 92, 'Letter # 1 - ''N''', NULL, '40.00', '20.00', '5.00', '70.00', 1, 'eur', 'metric', '1300.00', NULL, '0.00', '260.00', '1560.00', '1560.00', 0, 'lt'),
(218, 92, 'Letter # 2 - ''I''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(219, 92, 'Letter # 3 - ''K''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(220, 92, 'Letter # 4 - ''E''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(221, 92, 'GraphicElement # 1', NULL, '30.00', '50.00', '8.00', '80.00', 1, 'eur', 'metric', '1974.00', NULL, '0.00', '394.80', '2368.80', '2368.80', 0, 'lg'),
(222, 93, 'Letter # 1 - ''N''', NULL, '40.00', '20.00', '5.00', '70.00', 1, 'eur', 'metric', '1300.00', NULL, '0.00', '260.00', '1560.00', '1560.00', 0, 'lt'),
(223, 93, 'Letter # 2 - ''I''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(224, 93, 'Letter # 3 - ''K''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(225, 93, 'Letter # 4 - ''E''', NULL, '25.00', '15.00', '5.00', '35.00', 1, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '792.00', 0, 'lt'),
(226, 93, 'GraphicElement # 1', NULL, '30.00', '50.00', '8.00', '80.00', 1, 'eur', 'metric', '1974.00', NULL, '0.00', '394.80', '2368.80', '2368.80', 0, 'lg'),
(227, 94, 'Letter # 1 - ''N''', NULL, '40.00', '20.00', '5.00', '70.00', 2, 'eur', 'metric', '1300.00', NULL, '0.00', '260.00', '1560.00', '3120.00', 0, 'lt'),
(228, 94, 'Letter # 2 - ''I''', NULL, '25.00', '15.00', '5.00', '35.00', 2, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '1584.00', 0, 'lt'),
(229, 94, 'Letter # 3 - ''K''', NULL, '25.00', '15.00', '5.00', '35.00', 2, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '1584.00', 0, 'lt'),
(230, 94, 'Letter # 4 - ''E''', NULL, '25.00', '15.00', '5.00', '35.00', 2, 'eur', 'metric', '660.00', NULL, '0.00', '132.00', '792.00', '1584.00', 0, 'lt'),
(231, 94, 'GraphicElement # 1', NULL, '30.00', '50.00', '8.00', '80.00', 2, 'eur', 'metric', '1974.00', NULL, '0.00', '394.80', '2368.80', '4737.60', 0, 'lg'),
(232, 95, NULL, NULL, '20.00', '25.00', '2.00', '45.00', 1, 'eur', 'metric', '705.00', NULL, '0.00', '141.00', '846.00', '846.00', 0, 'lt'),
(233, 95, NULL, NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '375.00', NULL, '0.00', '75.00', '450.00', '450.00', 0, 'lt'),
(234, 95, NULL, NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '375.00', NULL, '0.00', '75.00', '450.00', '450.00', 0, 'lg'),
(235, 95, NULL, NULL, '20.00', '10.00', '2.00', '25.00', 1, 'eur', 'metric', '375.00', NULL, '0.00', '75.00', '450.00', '450.00', 0, 'lg'),
(236, 96, NULL, NULL, '20.00', '25.00', '2.00', '45.00', 1, 'eur', 'metric', '705.00', NULL, '0.00', '141.00', '846.00', '846.00', 0, 'lt'),
(237, 97, NULL, NULL, '20.00', '25.00', '2.00', '45.00', 1, 'eur', 'metric', '705.00', NULL, '0.00', '141.00', '846.00', '846.00', 0, 'lt'),
(238, 98, NULL, NULL, '20.00', '25.00', '2.00', '100.00', 1, 'eur', 'metric', '1320.00', NULL, '0.00', '264.00', '1584.00', '1584.00', 0, 'lt'),
(239, 98, NULL, NULL, '20.00', '10.00', '2.00', '90.00', 1, 'eur', 'metric', '988.00', NULL, '0.00', '197.60', '1185.60', '1185.60', 0, 'lt'),
(240, 98, NULL, NULL, '20.00', '10.00', '2.00', '90.00', 1, 'eur', 'metric', '988.00', NULL, '0.00', '197.60', '1185.60', '1185.60', 0, 'lt'),
(241, 98, NULL, NULL, '20.00', '10.00', '2.00', '90.00', 1, 'eur', 'metric', '988.00', NULL, '0.00', '197.60', '1185.60', '1185.60', 0, 'lt'),
(242, 98, NULL, NULL, '15.00', '5.00', '2.00', '50.00', 1, 'eur', 'metric', '520.00', NULL, '0.00', '104.00', '624.00', '624.00', 0, 'lt'),
(243, 98, NULL, NULL, '25.00', '15.00', '4.00', '90.00', 1, 'eur', 'metric', '1236.00', NULL, '0.00', '247.20', '1483.20', '1483.20', 0, 'lg'),
(244, 99, NULL, NULL, '30.00', '25.00', '3.00', '130.00', 1, 'eur', 'metric', '1774.00', NULL, '650.00', '354.80', '2778.80', '2778.80', 39, 'lt'),
(245, 99, NULL, NULL, '30.00', '25.00', '3.00', '130.00', 1, 'eur', 'metric', '1774.00', NULL, '650.00', '354.80', '2778.80', '2778.80', 39, 'lt'),
(246, 99, NULL, NULL, '30.00', '25.00', '3.00', '130.00', 1, 'eur', 'metric', '1774.00', NULL, '650.00', '354.80', '2778.80', '2778.80', 39, 'lt'),
(247, 99, NULL, NULL, '30.00', '25.00', '3.00', '130.00', 1, 'eur', 'metric', '1774.00', NULL, '650.00', '354.80', '2778.80', '2778.80', 39, 'lt'),
(248, 99, NULL, NULL, '30.00', '25.00', '3.00', '130.00', 1, 'eur', 'metric', '1774.00', NULL, '650.00', '354.80', '2778.80', '2778.80', 39, 'lt'),
(249, 99, NULL, NULL, '40.00', '25.00', '4.00', '150.00', 1, 'eur', 'metric', '2210.00', NULL, '750.00', '442.00', '3402.00', '3402.00', 45, 'lg');

-- --------------------------------------------------------

--
-- Table structure for table `item_option`
--

CREATE TABLE IF NOT EXISTS `item_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categoryValue_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DC7F757F4584665A` (`product_id`),
  KEY `IDX_DC7F757FDB805178` (`quote_id`),
  KEY `IDX_DC7F757F126F525E` (`item_id`),
  KEY `IDX_DC7F757F12469DE2` (`category_id`),
  KEY `IDX_DC7F757FB3C678F7` (`categoryValue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=479 ;

--
-- Dumping data for table `item_option`
--

INSERT INTO `item_option` (`id`, `product_id`, `quote_id`, `item_id`, `category_id`, `remark`, `categoryValue_id`) VALUES
(1, 1, NULL, NULL, 1, NULL, NULL),
(2, 1, NULL, NULL, 3, NULL, NULL),
(3, 1, NULL, NULL, 4, NULL, NULL),
(4, 1, NULL, NULL, 5, NULL, NULL),
(5, 1, NULL, NULL, 29, 'labour cost', NULL),
(6, 2, NULL, NULL, 10, NULL, NULL),
(7, 2, NULL, NULL, 11, NULL, NULL),
(8, 2, NULL, NULL, 12, NULL, NULL),
(9, 2, NULL, NULL, 13, NULL, NULL),
(10, 2, NULL, NULL, 14, NULL, NULL),
(11, 2, NULL, NULL, 30, 'labour cost', NULL),
(12, 3, NULL, NULL, 15, NULL, NULL),
(13, 3, NULL, NULL, 16, NULL, NULL),
(14, 3, NULL, NULL, 17, NULL, NULL),
(15, 3, NULL, NULL, 18, NULL, NULL),
(16, 3, NULL, NULL, 19, NULL, NULL),
(17, 3, NULL, NULL, 3, NULL, NULL),
(18, 3, NULL, NULL, 31, 'labour cost', NULL),
(19, 4, NULL, NULL, 21, NULL, NULL),
(20, 4, NULL, NULL, 22, NULL, NULL),
(21, 4, NULL, NULL, 23, NULL, NULL),
(22, 4, NULL, NULL, 32, 'labour cost', NULL),
(23, 5, NULL, NULL, 25, NULL, NULL),
(24, 5, NULL, NULL, 22, NULL, NULL),
(25, 5, NULL, NULL, 26, NULL, NULL),
(26, 5, NULL, NULL, 27, NULL, NULL),
(27, 5, NULL, NULL, 28, NULL, NULL),
(28, 5, NULL, NULL, 33, 'labour cost', NULL),
(29, NULL, NULL, NULL, NULL, 'TBD', NULL),
(30, NULL, NULL, NULL, NULL, 'TBD', NULL),
(31, NULL, NULL, NULL, NULL, 'TBD', NULL),
(32, NULL, NULL, NULL, NULL, 'TBD', NULL),
(33, NULL, NULL, NULL, NULL, 'TBD', NULL),
(34, NULL, NULL, NULL, NULL, 'TBD', NULL),
(35, NULL, NULL, NULL, NULL, 'TBD', NULL),
(36, NULL, NULL, NULL, NULL, 'TBD', NULL),
(37, NULL, NULL, NULL, NULL, 'TBD', NULL),
(38, NULL, NULL, NULL, NULL, 'TBD', NULL),
(39, NULL, NULL, NULL, NULL, 'TBD', NULL),
(40, NULL, NULL, NULL, NULL, 'TBD', NULL),
(41, NULL, NULL, NULL, NULL, 'TBD', NULL),
(42, NULL, NULL, NULL, NULL, 'TBD', NULL),
(43, NULL, NULL, NULL, NULL, 'TBD', NULL),
(44, NULL, NULL, NULL, NULL, 'TBD', NULL),
(45, NULL, NULL, NULL, NULL, 'TBD', NULL),
(46, NULL, NULL, NULL, NULL, 'TBD', NULL),
(47, NULL, NULL, NULL, NULL, 'TBD', NULL),
(48, NULL, NULL, NULL, NULL, 'TBD', NULL),
(49, NULL, NULL, NULL, NULL, 'TBD', NULL),
(50, NULL, NULL, NULL, NULL, 'TBD', NULL),
(166, NULL, 47, NULL, 1, NULL, 2),
(167, NULL, 47, NULL, 3, NULL, 4),
(168, NULL, 47, NULL, 4, NULL, 13),
(169, NULL, 47, NULL, 5, NULL, 6),
(170, NULL, 47, NULL, 6, NULL, 10),
(171, NULL, 47, NULL, 9, NULL, 9),
(172, NULL, 48, NULL, 10, NULL, 20),
(173, NULL, 48, NULL, 11, NULL, 23),
(174, NULL, 48, NULL, 12, NULL, 26),
(175, NULL, 48, NULL, 13, NULL, 37),
(176, NULL, 48, NULL, 14, NULL, 42),
(177, NULL, 48, NULL, 6, NULL, 10),
(178, NULL, 49, NULL, 15, NULL, 57),
(179, NULL, 49, NULL, 16, NULL, 52),
(180, NULL, 49, NULL, 17, NULL, 59),
(181, NULL, 49, NULL, 18, NULL, 60),
(182, NULL, 49, NULL, 19, NULL, 61),
(183, NULL, 49, NULL, 3, NULL, 4),
(184, NULL, 49, NULL, 6, NULL, 10),
(185, NULL, 50, NULL, 21, NULL, 81),
(186, NULL, 50, NULL, 22, NULL, 137),
(187, NULL, 50, NULL, 23, NULL, 91),
(188, NULL, 50, NULL, 6, NULL, 10),
(233, NULL, 58, NULL, 22, NULL, 137),
(234, NULL, 58, NULL, 23, NULL, 91),
(235, NULL, 58, NULL, 6, NULL, 10),
(236, NULL, 59, NULL, 10, NULL, 21),
(237, NULL, 59, NULL, 11, NULL, 23),
(238, NULL, 59, NULL, 12, NULL, 27),
(239, NULL, 59, NULL, 13, NULL, 38),
(240, NULL, 59, NULL, 14, NULL, 42),
(241, NULL, 59, NULL, 6, NULL, 10),
(242, NULL, 60, NULL, 1, NULL, 2),
(243, NULL, 60, NULL, 3, NULL, 4),
(244, NULL, 60, NULL, 4, NULL, 5),
(245, NULL, 60, NULL, 5, NULL, 7),
(246, NULL, 60, NULL, 6, NULL, 10),
(247, NULL, 60, NULL, 9, NULL, NULL),
(248, NULL, 61, NULL, 10, NULL, 20),
(249, NULL, 61, NULL, 11, NULL, NULL),
(250, NULL, 61, NULL, 12, NULL, 24),
(251, NULL, 61, NULL, 13, NULL, 37),
(252, NULL, 61, NULL, 14, NULL, 42),
(253, NULL, 61, NULL, 6, NULL, 10),
(261, NULL, 63, NULL, 21, NULL, 62),
(262, NULL, 63, NULL, 22, NULL, 134),
(263, NULL, 63, NULL, 23, NULL, 92),
(264, NULL, 63, NULL, 6, NULL, 10),
(265, NULL, 64, NULL, 25, NULL, 97),
(266, NULL, 64, NULL, 22, NULL, 134),
(267, NULL, 64, NULL, 26, NULL, 126),
(268, NULL, 64, NULL, 27, NULL, 129),
(269, NULL, 64, NULL, 28, NULL, 133),
(270, NULL, 64, NULL, 6, NULL, 10),
(284, NULL, 67, NULL, 1, NULL, 2),
(285, NULL, 67, NULL, 3, NULL, 4),
(286, NULL, 67, NULL, 4, NULL, 5),
(287, NULL, 67, NULL, 5, NULL, 7),
(288, NULL, 67, NULL, 6, NULL, 10),
(289, NULL, 67, NULL, 9, NULL, NULL),
(290, NULL, 68, NULL, 10, NULL, 20),
(291, NULL, 68, NULL, 11, NULL, 22),
(292, NULL, 68, NULL, 12, NULL, 24),
(293, NULL, 68, NULL, 13, NULL, 38),
(294, NULL, 68, NULL, 14, NULL, 42),
(295, NULL, 68, NULL, 6, NULL, 10),
(296, NULL, 69, NULL, 1, NULL, 2),
(297, NULL, 69, NULL, 3, NULL, 4),
(298, NULL, 69, NULL, 4, NULL, 11),
(299, NULL, 69, NULL, 5, NULL, 6),
(300, NULL, 69, NULL, 6, NULL, 10),
(301, NULL, 69, NULL, 9, NULL, NULL),
(302, NULL, 70, NULL, 10, NULL, 20),
(303, NULL, 70, NULL, 11, NULL, 22),
(304, NULL, 70, NULL, 12, NULL, 26),
(305, NULL, 70, NULL, 13, NULL, 41),
(306, NULL, 70, NULL, 14, NULL, 42),
(307, NULL, 70, NULL, 6, NULL, 10),
(308, NULL, 71, NULL, 1, NULL, 1),
(309, NULL, 71, NULL, 3, NULL, 4),
(310, NULL, 71, NULL, 4, NULL, 5),
(311, NULL, 71, NULL, 5, NULL, NULL),
(312, NULL, 71, NULL, 6, NULL, 10),
(313, NULL, 71, NULL, 9, NULL, 8),
(326, NULL, 74, NULL, 1, NULL, 2),
(327, NULL, 74, NULL, 3, NULL, 4),
(328, NULL, 74, NULL, 4, NULL, 5),
(329, NULL, 74, NULL, 5, NULL, 7),
(330, NULL, 74, NULL, 29, NULL, 138),
(331, NULL, 75, NULL, 10, NULL, 20),
(332, NULL, 75, NULL, 11, NULL, 22),
(333, NULL, 75, NULL, 12, NULL, 24),
(334, NULL, 75, NULL, 13, NULL, 37),
(335, NULL, 75, NULL, 14, NULL, 42),
(336, NULL, 75, NULL, 30, NULL, 139),
(337, NULL, 76, NULL, 10, NULL, NULL),
(338, NULL, 76, NULL, 11, NULL, NULL),
(339, NULL, 76, NULL, 12, NULL, 24),
(340, NULL, 76, NULL, 13, NULL, NULL),
(341, NULL, 76, NULL, 14, NULL, NULL),
(342, NULL, 76, NULL, 30, NULL, 139),
(343, NULL, 77, NULL, 1, NULL, 1),
(344, NULL, 77, NULL, 3, NULL, 4),
(345, NULL, 77, NULL, 4, NULL, 5),
(346, NULL, 77, NULL, 5, NULL, NULL),
(347, NULL, 77, NULL, 29, NULL, 138),
(348, NULL, 78, NULL, 10, NULL, 21),
(349, NULL, 78, NULL, 11, NULL, NULL),
(350, NULL, 78, NULL, 12, NULL, 34),
(351, NULL, 78, NULL, 13, NULL, 37),
(352, NULL, 78, NULL, 14, NULL, NULL),
(353, NULL, 78, NULL, 30, NULL, 139),
(354, NULL, 79, NULL, 10, NULL, 21),
(355, NULL, 79, NULL, 11, NULL, 23),
(356, NULL, 79, NULL, 12, NULL, 33),
(357, NULL, 79, NULL, 13, NULL, 40),
(358, NULL, 79, NULL, 14, NULL, 143),
(359, NULL, 79, NULL, 30, NULL, 139),
(360, NULL, 80, NULL, 10, NULL, 20),
(361, NULL, 80, NULL, 11, NULL, NULL),
(362, NULL, 80, NULL, 12, NULL, 24),
(363, NULL, 80, NULL, 13, NULL, NULL),
(364, NULL, 80, NULL, 14, NULL, NULL),
(365, NULL, 80, NULL, 30, NULL, 139),
(366, NULL, 81, NULL, 10, NULL, 20),
(367, NULL, 81, NULL, 11, NULL, NULL),
(368, NULL, 81, NULL, 12, NULL, 24),
(369, NULL, 81, NULL, 13, NULL, 37),
(370, NULL, 81, NULL, 14, NULL, NULL),
(371, NULL, 81, NULL, 30, NULL, 139),
(372, NULL, 82, NULL, 10, NULL, 20),
(373, NULL, 82, NULL, 11, NULL, NULL),
(374, NULL, 82, NULL, 12, NULL, 31),
(375, NULL, 82, NULL, 13, NULL, 37),
(376, NULL, 82, NULL, 14, NULL, NULL),
(377, NULL, 82, NULL, 30, NULL, 139),
(378, NULL, 83, NULL, 10, NULL, 21),
(379, NULL, 83, NULL, 11, NULL, NULL),
(380, NULL, 83, NULL, 12, NULL, 24),
(381, NULL, 83, NULL, 13, NULL, 37),
(382, NULL, 83, NULL, 14, NULL, 42),
(383, NULL, 83, NULL, 30, NULL, 139),
(384, NULL, 84, NULL, 10, NULL, 21),
(385, NULL, 84, NULL, 11, NULL, NULL),
(386, NULL, 84, NULL, 12, NULL, 28),
(387, NULL, 84, NULL, 13, NULL, 41),
(388, NULL, 84, NULL, 14, NULL, 42),
(389, NULL, 84, NULL, 30, NULL, 139),
(390, NULL, 85, NULL, 10, NULL, 20),
(391, NULL, 85, NULL, 11, NULL, NULL),
(392, NULL, 85, NULL, 12, NULL, 25),
(393, NULL, 85, NULL, 13, NULL, 37),
(394, NULL, 85, NULL, 14, NULL, NULL),
(395, NULL, 85, NULL, 30, NULL, 139),
(396, NULL, 86, NULL, 10, NULL, 20),
(397, NULL, 86, NULL, 11, NULL, NULL),
(398, NULL, 86, NULL, 12, NULL, 24),
(399, NULL, 86, NULL, 13, NULL, 37),
(400, NULL, 86, NULL, 14, NULL, NULL),
(401, NULL, 86, NULL, 30, NULL, 139),
(402, NULL, 87, NULL, 10, NULL, 20),
(403, NULL, 87, NULL, 11, NULL, NULL),
(404, NULL, 87, NULL, 12, NULL, 25),
(405, NULL, 87, NULL, 13, NULL, 37),
(406, NULL, 87, NULL, 14, NULL, NULL),
(407, NULL, 87, NULL, 30, NULL, 139),
(408, NULL, 88, NULL, 1, NULL, 1),
(409, NULL, 88, NULL, 3, NULL, 4),
(410, NULL, 88, NULL, 4, NULL, 5),
(411, NULL, 88, NULL, 5, NULL, NULL),
(412, NULL, 88, NULL, 29, NULL, 138),
(413, NULL, 89, NULL, 10, NULL, NULL),
(414, NULL, 89, NULL, 11, NULL, NULL),
(415, NULL, 89, NULL, 12, NULL, 24),
(416, NULL, 89, NULL, 13, NULL, 37),
(417, NULL, 89, NULL, 14, NULL, 143),
(418, NULL, 89, NULL, 30, NULL, 139),
(419, NULL, 90, NULL, 10, NULL, 20),
(420, NULL, 90, NULL, 11, NULL, NULL),
(421, NULL, 90, NULL, 12, NULL, 28),
(422, NULL, 90, NULL, 13, NULL, 37),
(423, NULL, 90, NULL, 14, NULL, NULL),
(424, NULL, 90, NULL, 30, NULL, 139),
(425, NULL, 91, NULL, 10, NULL, 20),
(426, NULL, 91, NULL, 11, NULL, NULL),
(427, NULL, 91, NULL, 12, NULL, 28),
(428, NULL, 91, NULL, 13, NULL, 37),
(429, NULL, 91, NULL, 14, NULL, NULL),
(430, NULL, 91, NULL, 30, NULL, 139),
(431, NULL, 92, NULL, 10, NULL, 20),
(432, NULL, 92, NULL, 11, NULL, NULL),
(433, NULL, 92, NULL, 12, NULL, 28),
(434, NULL, 92, NULL, 13, NULL, 37),
(435, NULL, 92, NULL, 14, NULL, NULL),
(436, NULL, 92, NULL, 30, NULL, 139),
(437, NULL, 93, NULL, 10, NULL, 20),
(438, NULL, 93, NULL, 11, NULL, NULL),
(439, NULL, 93, NULL, 12, NULL, 28),
(440, NULL, 93, NULL, 13, NULL, 37),
(441, NULL, 93, NULL, 14, NULL, NULL),
(442, NULL, 93, NULL, 30, NULL, 139),
(443, NULL, 94, NULL, 10, NULL, 20),
(444, NULL, 94, NULL, 11, NULL, NULL),
(445, NULL, 94, NULL, 12, NULL, 28),
(446, NULL, 94, NULL, 13, NULL, 37),
(447, NULL, 94, NULL, 14, NULL, NULL),
(448, NULL, 94, NULL, 30, NULL, 139),
(449, NULL, 95, NULL, 10, NULL, 20),
(450, NULL, 95, NULL, 11, NULL, NULL),
(451, NULL, 95, NULL, 12, NULL, 24),
(452, NULL, 95, NULL, 13, NULL, 37),
(453, NULL, 95, NULL, 14, NULL, NULL),
(454, NULL, 95, NULL, 30, NULL, 139),
(455, NULL, 96, NULL, 10, NULL, 20),
(456, NULL, 96, NULL, 11, NULL, NULL),
(457, NULL, 96, NULL, 12, NULL, 24),
(458, NULL, 96, NULL, 13, NULL, 37),
(459, NULL, 96, NULL, 14, NULL, NULL),
(460, NULL, 96, NULL, 30, NULL, 139),
(461, NULL, 97, NULL, 10, NULL, 20),
(462, NULL, 97, NULL, 11, NULL, NULL),
(463, NULL, 97, NULL, 12, NULL, 24),
(464, NULL, 97, NULL, 13, NULL, 37),
(465, NULL, 97, NULL, 14, NULL, NULL),
(466, NULL, 97, NULL, 30, NULL, 139),
(467, NULL, 98, NULL, 10, NULL, 20),
(468, NULL, 98, NULL, 11, NULL, NULL),
(469, NULL, 98, NULL, 12, NULL, 153),
(470, NULL, 98, NULL, 13, NULL, 40),
(471, NULL, 98, NULL, 14, NULL, NULL),
(472, NULL, 98, NULL, 30, NULL, 139),
(473, NULL, 99, NULL, 10, NULL, 20),
(474, NULL, 99, NULL, 11, NULL, NULL),
(475, NULL, 99, NULL, 12, NULL, 149),
(476, NULL, 99, NULL, 13, NULL, 37),
(477, NULL, 99, NULL, 14, NULL, 42),
(478, NULL, 99, NULL, 30, NULL, 139);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `address`, `zipcode`, `city`, `country`) VALUES
(1, NULL, '22 Soi Phokaew 1, Yaek 5, Khlong Chan, Bangkapi, Bangkok 10240, Thailand', '10240', 'Bangkok', 'TH'),
(2, NULL, '22 Soi Phokaew 1, Yaek 5, Khlong Chan, Bangkapi, Bangkok 10240, Thailand', '10240', 'Bangkok', 'TH'),
(3, NULL, '22 Soi Phokaew 1, Yaek 5, Khlong Chan, Bangkapi, Bangkok 10240, Thailand', '10240', 'Bangkok', 'TH'),
(4, NULL, '22 Soi Phokaew 1, Yaek 5, Khlong Chan, Bangkapi, Bangkok 10240, Thailand', '10240', 'Bangkok', 'TH'),
(5, NULL, 'cccc', 'cccc', 'cccc', 'BH'),
(6, NULL, 'cccc', 'cccc', 'cccc', 'BH'),
(7, NULL, 'dddd', 'dddd', 'dddd', 'BS'),
(8, NULL, 'dddd', '10500', 'dddd', 'AD'),
(9, NULL, 'eeee', 'eeee', 'eeee', 'GT'),
(10, NULL, 'eeee', 'eeee', 'eeee', 'LS'),
(11, NULL, 'adresse ligne 1\r\nadresse ligne 2\r\nadresse ligne 3', '10110', 'Bangkok', 'TH'),
(12, NULL, 'adresse ligne 1\r\nadresse ligne 2\r\nadresse ligne 3', '10110', 'Bangkok', 'TH'),
(13, NULL, 'company[billingAddress][address]', '90210', 'company[billingAddress][city]', 'AF'),
(14, NULL, 'company[shippingAddress][address]', '90210', 'company[shippingAddress][city]', 'AF'),
(15, NULL, 'broughton', '242344', 'chester', 'AF'),
(16, NULL, 'broughton', '21133', 'chester', 'AF'),
(17, NULL, 'yoyo address', '10500', 'Bagnkok', 'TH'),
(18, NULL, 'yoyo address', '10500', 'Bagnkok', 'TH'),
(19, NULL, 'ถนนอโศก 90cv très agréable à conduire\r\n13 A/F South Tower, World Finance Center,\r\nHarbour City, 17 Canton Road,\r\nTsim Sha Tsui, Kowloon, Hong Kong', '101100', 'Kowloon', 'HK'),
(20, NULL, 'Unit 21/1, 21st Floor, BUI Building, \r\n177/1 Surawong Rd.,\r\nBangrak, Bangkok 10500, Thailand', '10500', 'Bangkok', 'TH'),
(21, NULL, 'Central Embassy ชั้น 3 เลขที่ 1031 ถ.เพลินจิต แขวงลุมพินี เขตปทุมวัน กทม. 10330, กรุงเทพมหานคร, 10330', '10330', 'Bangkok', 'TH'),
(22, NULL, 'Central Embassy ชั้น 3 เลขที่ 1031 ถ.เพลินจิต แขวงลุมพินี เขตปทุมวัน กทม. 10330, กรุงเทพมหานคร, 10330', '10330', 'Bangkok', 'TH'),
(23, NULL, 'yyy', '10500', 'BKK', 'AT'),
(24, NULL, 'yyy', '10500', 'BKK', 'AT'),
(25, NULL, 'aaaa', 'aaaa', 'aaa', 'AZ'),
(26, NULL, 'aaaa', 'aaaa', 'aaa', 'AZ'),
(27, NULL, '555', '555', '55', 'AT'),
(28, NULL, '555', '555', '55', 'AT'),
(31, NULL, 'asdfasdfa', 'asdfad', 'fadfadf', 'AW'),
(32, NULL, 'asdfasdfa', 'asdfad', 'fadfadf', 'AW'),
(33, NULL, '36 rue du Bac', '75007', 'Paris', 'FR'),
(34, NULL, '36 rue du Bac', '75007', 'Paris', 'FR'),
(35, NULL, 'address', '10300', 'City', 'TH'),
(36, NULL, 'address', '10300', 'City', 'TH'),
(37, NULL, 'Internal Company', '10500', '10000', 'AL'),
(38, NULL, 'Internal Company', '10500', '10000', 'AL'),
(39, NULL, '2 rue Toulouse', '35000', 'RENNES', 'FR'),
(40, NULL, '2 rue Toulouse', '35000', 'RENNES', 'FR');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `company_id` int(11) DEFAULT NULL,
  `weightOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD979B1AD6` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `company_id`, `weightOrder`) VALUES
(1, 'Vision III', 'Acrylic\r\n', 2, 20),
(2, 'Fabricated letters\r\n', 'Inox', 1, 10),
(3, 'Acrynox', 'Acrynox', 2, 30),
(4, 'Solid cut letters\r\n', 'Flat Cut', 1, 40),
(5, 'Engraved plaques\r\n', 'Plaque', 1, 50);

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `measureUnit` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `currencyRate` decimal(10,5) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `priceTransport` decimal(10,2) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `modifiedAt` datetime NOT NULL,
  `margin` decimal(10,2) NOT NULL,
  `additionalCost` decimal(10,2) NOT NULL,
  `priceSelling` decimal(10,2) NOT NULL,
  `priceSellingTotalAll` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `adminFee` decimal(10,2) NOT NULL,
  `priceSellingTotalFinal` decimal(10,2) DEFAULT NULL,
  `volume` decimal(10,2) NOT NULL,
  `weightTransport` decimal(10,2) NOT NULL,
  `priceGrandTotal` decimal(10,2) NOT NULL,
  `priceTransportSub` decimal(10,2) DEFAULT NULL,
  `priceTransportRemoteArea` decimal(10,2) DEFAULT NULL,
  `priceTransportFuel` decimal(10,2) DEFAULT NULL,
  `priceTransportSafety` decimal(10,2) DEFAULT NULL,
  `statusPayment` tinyint(1) NOT NULL,
  `numOfLed` int(11) DEFAULT NULL,
  `priceAdaptor` decimal(10,2) DEFAULT NULL,
  `adaptorType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmedAt` datetime NOT NULL,
  `paidAt` datetime NOT NULL,
  `trackingNo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliveredAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6B71CBF477153098` (`code`),
  KEY `IDX_6B71CBF4979B1AD6` (`company_id`),
  KEY `IDX_6B71CBF4A76ED395` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=100 ;

--
-- Dumping data for table `quote`
--

INSERT INTO `quote` (`id`, `company_id`, `user_id`, `code`, `name`, `description`, `quantity`, `currency`, `measureUnit`, `status`, `currencyRate`, `price`, `priceTransport`, `createdAt`, `modifiedAt`, `margin`, `additionalCost`, `priceSelling`, `priceSellingTotalAll`, `discount`, `adminFee`, `priceSellingTotalFinal`, `volume`, `weightTransport`, `priceGrandTotal`, `priceTransportSub`, `priceTransportRemoteArea`, `priceTransportFuel`, `priceTransportSafety`, `statusPayment`, `numOfLed`, `priceAdaptor`, `adaptorType`, `confirmedAt`, `paidAt`, `trackingNo`, `deliveredAt`) VALUES
(3, 7, NULL, '7y138g5w', 'CCCC', 'cccc', 3, 'eur', 'metric', 'EXP', '0.00000', NULL, NULL, '2014-08-16 17:57:18', '0000-00-00 00:00:00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(47, 1, NULL, 'ACRYLICEXAMPLE', 'Acrylic Product Example', 'Acrylic Product Example', 10, 'eur', 'metric', 'PEN', '0.02338', '102320.00', '4429.92', '2014-08-04 08:00:27', '2014-08-04 10:32:11', '20464.00', '0.00', '122784.00', '1227840.00', '61392.00', '427.75', '1166875.75', '1200.00', '7.20', '1171305.67', '3156.00', '200.00', '671.20', '402.72', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(48, 1, NULL, 'INOXEXAMPLE', 'Inox Product Example', 'Inox Product Example', 20, 'eur', 'metric', 'PEN', '0.02338', '39640.00', '5954.52', '2014-08-04 08:03:15', '2014-08-04 10:31:37', '7928.00', '20000.00', '67568.00', '1351360.00', '67568.00', '427.75', '1284219.75', '1200.00', '14.40', '1290174.27', '4311.00', '200.00', '902.20', '541.32', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(49, 1, NULL, 'ACRYNOXEXAMPLE', 'Acrynox Product Example', 'Acrynox Product Example', 30, 'eur', 'metric', 'PEN', '0.02338', '96720.00', '7397.28', '2014-08-04 08:14:35', '2014-08-04 10:31:11', '19344.00', '0.00', '116064.00', '3481920.00', '174096.00', '427.75', '3308251.75', '1200.00', '21.60', '3315649.03', '5404.00', '200.00', '1120.80', '672.48', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(50, 1, NULL, 'FLATCUTEXAMPLE', 'Flat Cut  Product Example', 'Flat Cut  Product Example', 12, 'eur', 'metric', 'PEN', '0.02338', '53860.00', '4952.64', '2014-08-04 09:15:38', '2014-08-05 08:09:22', '10772.00', '0.00', '64632.00', '646320.00', '32316.00', '427.75', '614431.75', '1200.00', '8.64', '619384.39', '3552.00', '200.00', '750.40', '450.24', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(58, 5, NULL, 'EEEE10000004', 'Jojo', 'Jojo Jojo', 10, 'usd', 'metric', 'PEN', '0.03147', '26900.00', '3138.96', '2014-08-13 11:25:53', '2014-08-13 13:47:10', '5380.00', '0.00', '32280.00', '322800.00', '16140.00', '476.55', '307136.55', '600.00', '3.60', '310275.51', '2178.00', '200.00', '475.60', '285.36', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(59, 9, NULL, 'YOYO10000000', 'YO Logo', 'YO logo', 20, 'usd', 'metric', 'PEN', '0.03147', '71430.00', '24746.04', '2014-08-13 14:16:43', '2014-08-13 14:18:56', '14286.00', '30000.00', '115716.00', '2314320.00', '115716.00', '476.55', '2199080.55', '7200.00', '86.40', '2223826.59', '18547.00', '200.00', '3749.40', '2249.64', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(60, 1, NULL, 'AAAA10000000', 'nike', 'nike log', 10, 'eur', 'metric', 'PEN', '0.02338', '11500.00', '14092.32', '2014-08-13 15:43:09', '2014-08-13 15:51:19', '2300.00', '0.00', '13800.00', '138000.00', '6900.00', '427.75', '131527.75', '7500.00', '45.00', '145620.07', '10476.00', '200.00', '2135.20', '1281.12', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(61, 1, NULL, 'AAAA10000001', 'PUMA Logo', 'PUMA Logo', 1, 'eur', 'metric', 'CON', '0.02338', '5000.00', '3138.96', '2014-08-13 16:20:43', '2014-08-13 16:36:14', '1000.00', '2000.00', '8000.00', '8000.00', '400.00', '427.75', '8027.75', '6000.00', '3.60', '11166.71', '2178.00', '200.00', '475.60', '285.36', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(63, 1, NULL, 'AAAA10000003', 'Reebok logo', 'Reebok logo', 1, 'eur', 'metric', 'PEN', '0.02338', '10150.00', '2509.32', '2014-08-13 17:13:33', '2014-08-13 17:14:03', '2030.00', '0.00', '12180.00', '12180.00', '609.00', '427.75', '11998.75', '3500.00', '2.10', '14508.07', '1701.00', '200.00', '380.20', '228.12', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(64, 1, NULL, 'AAAA10000004', 'Luren', 'Luren', 1, 'eur', 'metric', 'CON', '0.02338', '3360.00', '1527.24', '2014-08-13 17:24:26', '2014-08-13 17:33:13', '672.00', '0.00', '4032.00', '4032.00', '201.60', '427.75', '4258.15', '300.00', '0.18', '5785.39', '957.00', '200.00', '231.40', '138.84', 0, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(67, 10, NULL, 'ACT10000000', 'Example for a sign with 4 fabricated letters "COCO" for Acrylic', 'Example for a sign with 4 fabricated letters "COCO" for Acrylic', 2, 'eur', 'metric', 'PAD', '0.02338', '4610.00', '3138.96', '2014-08-27 13:15:50', '2014-09-02 14:57:19', '922.00', '0.00', '5532.00', '11064.00', '553.20', '427.75', '10938.55', '3000.00', '3.60', '14877.51', '2178.00', '200.00', '475.60', '285.36', 1, 18, '800.00', '5.0A:0|2.5A:1', '0000-00-00 00:00:00', '2014-09-02 14:57:19', NULL, '0000-00-00 00:00:00'),
(68, 10, NULL, 'ACT10000001', 'Example for a sign with 4 fabricated letters "COCO" for Inox', 'Example for a sign with 4 fabricated letters "COCO" for Inox', 2, 'eur', 'metric', 'PEN', '0.02338', '2690.00', '3138.96', '2014-08-27 13:22:40', '2014-09-02 14:33:10', '538.00', '1100.00', '4328.00', '8656.00', '432.80', '427.75', '8650.95', '3000.00', '3.60', '13389.91', '2178.00', '200.00', '475.60', '285.36', 0, 132, '1600.00', '5.0A:1|2.5A:0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(69, 10, NULL, 'ACT10000002', 'Example for a sign with 4 fabricated letters "COCO" for Acrylic for Adaptor Calculation', 'Example for a sign with 4 fabricated letters "COCO" for Acrylic for Adaptor Calculation.', 2, 'eur', 'imperial', 'DLV', '0.02338', '7500.25', '3348.84', '2014-08-28 14:30:46', '2015-02-15 17:50:37', '1500.05', '0.00', '9000.30', '18000.60', '900.03', '427.75', '17528.32', '3429.00', '4.11', '22477.16', '2337.00', '200.00', '507.40', '304.44', 1, 132, '1600.00', '5.0A:1|2.5A:0', '0000-00-00 00:00:00', '2015-02-15 16:00:31', 'DSL100001', '2015-02-15 17:50:37'),
(70, 10, NULL, 'ACT10000003', 'Example for a sign with 4 fabricated letters "COCO" for Acrylic', 'Example for a sign with 4 fabricated letters "COCO" for Acrylic', 1, 'eur', 'metric', 'CON', '0.02338', '2100.00', '1527.24', '2014-11-28 14:22:10', '2014-11-28 15:00:15', '420.00', '1000.00', '3520.00', '3520.00', '176.00', '427.75', '3771.75', '600.00', '0.36', '6098.99', '957.00', '200.00', '231.40', '138.84', 0, 60, '800.00', '5.0A:0|2.5A:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(71, 11, NULL, 'APPP10000000', 'test pdf in thai character', 'test pdf in thai character', 1, 'eur', 'metric', 'CON', '0.02338', '3740.00', '1527.24', '2015-01-12 16:12:13', '2015-01-19 11:44:54', '748.00', '0.00', '4488.00', '4488.00', '0.00', '427.75', '4691.35', '600.00', '0.36', '7018.59', '957.00', '200.00', '231.40', '138.84', 0, 10, '800.00', '5.0A:0|2.5A:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(74, 10, NULL, 'SN-2015021001', 'Test Acrylic Product', 'Test Acrylic Product', 1, 'eur', 'metric', 'PEN', '0.02338', '3580.00', '1389.56', '2015-02-08 18:01:42', '2015-02-16 20:30:39', '716.00', '0.00', '4296.00', '4296.00', '214.80', '427.75', '4508.95', '600.00', '0.36', '6698.51', '957.00', '95.70', '210.54', '126.32', 0, 10, '800.00', '5.0A:0|2.5A:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(75, 11, NULL, 'SX-2015021001', 'Test Inox Product', 'Test Inox Product', 1, 'eur', 'metric', 'CON', '0.02338', '2100.00', '1389.56', '2015-02-08 18:03:27', '2015-02-17 11:56:22', '420.00', '1000.00', '3520.00', '3520.00', '176.00', '427.75', '3771.75', '600.00', '0.36', '5961.31', '957.00', '95.70', '210.54', '126.32', 0, 60, '800.00', '5.0A:0|2.5A:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(76, 10, NULL, 'SX-2015021002', 'CO TEST', NULL, 1, 'eur', 'metric', 'CON', '0.02300', '21900.00', '140809.15', '2015-02-19 15:44:38', '2015-03-26 10:15:22', '4380.00', '0.00', '26280.00', '26280.00', '0.00', '0.00', '26280.00', '900000.00', '540.00', '167089.15', '96976.00', '9697.60', '21334.72', '12800.83', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(77, 4, NULL, 'SN-2015031001', 'COCO', NULL, 1, 'eur', 'metric', 'CON', '0.02690', '25545.00', '1659.64', '2015-03-06 16:01:11', '2015-03-26 09:50:27', '5109.00', '0.00', '30654.00', '30654.00', '10115.82', '1227.71', '21765.89', '900.00', '0.54', '25025.52', '1143.00', '114.30', '251.46', '150.88', 0, 150, '1600.00', '5.0A:1|2.5A:0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(78, 11, NULL, 'SX-2015031001', 'Dean Test', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '2224.00', '2199.78', '2015-03-27 12:59:35', '2015-03-27 12:59:36', '444.80', '0.00', '2668.80', '2668.80', '133.44', '372.03', '2907.39', '2566.00', '1.54', '5107.17', '1515.00', '151.50', '333.30', '199.98', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(79, 11, NULL, 'SX-2015031002', 'Deans baker', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '12117.50', '13852.08', '2015-03-28 12:55:44', '2015-03-28 12:55:45', '2423.50', '0.00', '14541.00', '14541.00', '727.05', '372.03', '14185.98', '67186.00', '40.31', '28038.06', '9540.00', '954.00', '2098.80', '1259.28', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(80, 8, NULL, 'SX-2015031003', 'GDRProjet04', NULL, 1, 'eur', 'metric', 'PAD', '0.02690', '1380.00', '1659.64', '2015-03-30 10:20:51', '2015-03-30 10:32:20', '276.00', '0.00', '1656.00', '1656.00', '82.80', '372.03', '1945.23', '1200.00', '0.72', '3604.87', '1143.00', '114.30', '251.46', '150.88', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-03-30 10:32:20', NULL, '0000-00-00 00:00:00'),
(81, 19, NULL, 'SX-2015031004', 'Chris', NULL, 1, 'eur', 'metric', 'DLV', '0.02690', '3957.00', '2245.32', '2015-03-30 10:28:17', '2015-03-31 07:02:56', '791.40', '0.00', '4748.40', '4748.40', '237.42', '0.00', '4510.98', '3560.00', '2.14', '6756.30', '1701.00', '0.00', '340.20', '204.12', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-03-31 07:01:46', '45377', '2015-03-31 07:02:56'),
(82, 19, NULL, 'SX-2015031005', 'Galerie Lafayette', NULL, 1, 'eur', 'metric', 'PAD', '0.02690', '6030.27', '2455.20', '2015-03-31 06:46:53', '2015-03-31 09:55:49', '1206.05', '0.00', '7236.33', '7236.33', '361.82', '0.00', '6874.51', '4981.20', '2.99', '9329.71', '1860.00', '0.00', '372.00', '223.20', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-03-31 09:55:49', NULL, '0000-00-00 00:00:00'),
(83, 19, NULL, 'SX-2015031006', 'PAUL', NULL, 1, 'eur', 'metric', 'DLV', '0.02690', '7839.00', '3817.44', '2015-03-31 06:53:39', '2015-03-31 09:56:08', '1567.80', '1660.00', '11066.80', '11066.80', '553.34', '0.00', '10513.46', '10350.00', '6.21', '15930.90', '2892.00', '0.00', '578.40', '347.04', 1, 100, '1600.00', '5.0A:1|2.5A:0', '0000-00-00 00:00:00', '2015-03-31 09:55:17', '55646', '2015-03-31 09:56:08'),
(84, 19, NULL, 'SX-2015031007', 'CHANEL', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '7250.00', '4688.64', '2015-03-31 07:09:58', '2015-03-31 07:09:58', '1450.00', '1250.00', '9950.00', '9950.00', '497.50', '0.00', '9452.50', '15000.00', '9.00', '14941.14', '3552.00', '0.00', '710.40', '426.24', 0, 75, '800.00', '5.0A:0|2.5A:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(85, 19, NULL, 'SX-2015031008', 'NIKE', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '5688.04', '2455.20', '2015-03-31 09:46:50', '2015-03-31 09:46:50', '1137.61', '0.00', '6825.65', '6825.65', '341.28', '0.00', '6484.37', '4981.20', '2.99', '8939.57', '1860.00', '0.00', '372.00', '223.20', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(86, 4, NULL, 'SX-2015041001', 'Mr Jack', NULL, 1, 'eur', 'metric', 'DLV', '0.02690', '1735.00', '1929.71', '2015-04-28 04:31:30', '2015-04-28 04:36:36', '347.00', '0.00', '2082.00', '2082.00', '687.06', '1227.71', '2622.65', '2400.00', '1.44', '4552.35', '1329.00', '132.90', '292.38', '175.43', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-04-28 04:35:56', '64667', '2015-04-28 04:36:36'),
(87, 19, NULL, 'SX-2015051001', 'Toto', NULL, 1, 'eur', 'metric', 'DLV', '0.02690', '2195.00', '1508.76', '2015-05-05 09:26:43', '2015-05-05 09:35:44', '439.00', '0.00', '2634.00', '2634.00', '131.70', '0.00', '2502.30', '1600.00', '0.96', '4011.06', '1143.00', '0.00', '228.60', '137.16', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-05-05 09:30:11', 'dhl 001', '2015-05-05 09:35:44'),
(88, 6, NULL, 'SN-2015051001', 'P1', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '76375.00', '1929.71', '2015-05-19 10:24:25', '2015-05-19 10:24:25', '15275.00', '0.00', '91650.00', '91650.00', '9165.00', '372.03', '82857.03', '2200.00', '1.32', '90386.74', '1329.00', '132.90', '292.38', '175.43', 0, 490, '5600.00', '5.0A:3|2.5A:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(89, 4, NULL, 'SX-2015051002', 'Projet Nike 01', NULL, 1, 'eur', 'metric', 'PAD', '0.02690', '2430.00', '1929.71', '2015-05-20 11:15:42', '2015-05-20 11:17:45', '486.00', '0.00', '2916.00', '2916.00', '962.28', '1227.71', '3181.43', '2200.00', '1.32', '5111.13', '1329.00', '132.90', '292.38', '175.43', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-05-20 11:17:45', NULL, '0000-00-00 00:00:00'),
(90, 19, NULL, 'SX-2015051003', 'Galerie Lafayette', NULL, 1, 'eur', 'metric', 'CON', '0.02690', '5254.00', '5472.72', '2015-05-26 08:06:54', '2015-05-27 04:20:00', '1050.80', '0.00', '6304.80', '6304.80', '315.24', '0.00', '5989.56', '21625.00', '12.98', '11462.28', '4146.00', '0.00', '829.20', '497.52', 0, 0, '0.00', '', '2015-05-27 04:19:59', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(91, 19, NULL, 'SX-2015051004', 'Galerie Lafayette', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '5254.00', '5472.72', '2015-05-26 08:07:11', '2015-05-26 08:07:11', '1050.80', '0.00', '6304.80', '6304.80', '315.24', '0.00', '5989.56', '21625.00', '12.98', '11462.28', '4146.00', '0.00', '829.20', '497.52', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(92, 19, NULL, 'SX-2015051005', 'Galerie Lafayette', NULL, 1, 'eur', 'metric', 'DLV', '0.02690', '5254.00', '5472.72', '2015-05-26 08:07:32', '2015-05-26 08:35:02', '1050.80', '0.00', '6304.80', '6304.80', '315.24', '0.00', '5989.56', '21625.00', '12.98', '11462.28', '4146.00', '0.00', '829.20', '497.52', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-05-26 08:21:44', 'dhl123', '2015-05-26 08:35:02'),
(93, 19, NULL, 'SX-2015051006', 'Galerie Lafayette', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '5254.00', '5472.72', '2015-05-26 08:07:34', '2015-05-26 08:07:34', '1050.80', '0.00', '6304.80', '6304.80', '315.24', '0.00', '5989.56', '21625.00', '12.98', '11462.28', '4146.00', '0.00', '829.20', '497.52', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(94, 19, NULL, 'SX-2015051007', 'Galerie Lafayette', NULL, 2, 'eur', 'metric', 'DLV', '0.02690', '5254.00', '8421.60', '2015-05-26 08:07:35', '2015-05-26 08:34:33', '1050.80', '0.00', '6304.80', '12609.60', '630.48', '0.00', '11979.12', '21625.00', '25.95', '20400.72', '6380.00', '0.00', '1276.00', '765.60', 1, 0, '0.00', '', '0000-00-00 00:00:00', '2015-05-26 08:33:27', '', '2015-05-26 08:34:33'),
(95, 19, NULL, 'SX-2015051008', 'P270515', NULL, 1, 'eur', 'metric', 'PAD', '0.02690', '1830.00', '1754.28', '2015-05-27 02:54:10', '2015-05-27 03:33:18', '366.00', '0.00', '2196.00', '2196.00', '109.80', '0.00', '2086.20', '2200.00', '1.32', '3840.48', '1329.00', '0.00', '265.80', '159.48', 1, 0, '0.00', '', '2015-05-27 02:59:10', '2015-05-27 03:33:18', NULL, '0000-00-00 00:00:00'),
(96, 19, NULL, 'SX-2015051009', 'P2', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '705.00', '1508.76', '2015-05-27 05:09:25', '2015-05-27 05:09:25', '141.00', '0.00', '846.00', '846.00', '42.30', '0.00', '803.70', '1000.00', '0.60', '2312.46', '1143.00', '0.00', '228.60', '137.16', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(97, 17, NULL, 'SX-2015051010', 'P3', NULL, 1, 'eur', 'metric', 'PEN', '0.02690', '705.00', '1659.64', '2015-05-27 05:17:32', '2015-05-27 05:17:32', '141.00', '0.00', '846.00', '846.00', '0.00', '0.00', '846.00', '1000.00', '0.60', '2505.64', '1143.00', '114.30', '251.46', '150.88', 0, 0, '0.00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(98, 6, NULL, 'SX-2015051011', 'PTE 01', NULL, 1, 'eur', 'metric', 'PAD', '0.02690', '6040.00', '2041.20', '2015-05-29 03:13:12', '2015-05-29 03:32:22', '1208.00', '0.00', '7248.00', '7248.00', '362.40', '0.00', '6885.60', '3850.00', '2.31', '8926.80', '1701.00', '0.00', '340.20', '0.00', 1, 0, '0.00', '', '2015-05-29 03:22:54', '2015-05-29 03:32:22', NULL, '0000-00-00 00:00:00'),
(99, 6, NULL, 'SX-2015051012', 'PTE 02', NULL, 1, 'eur', 'metric', 'CON', '0.02690', '11080.00', '4420.80', '2015-05-29 03:39:04', '2015-05-29 03:39:37', '2216.00', '4000.00', '17296.00', '17296.00', '864.80', '0.00', '16431.20', '15250.00', '9.15', '24052.00', '3684.00', '0.00', '736.80', '0.00', 0, 240, '3200.00', '5.0A:2|2.5A:0', '2015-05-29 03:39:37', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `quote_detail`
--

CREATE TABLE IF NOT EXISTS `quote_detail` (
  `quote_id` int(11) NOT NULL,
  `designType` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numOfLetterItem` int(11) DEFAULT NULL,
  `numOfGraphicItem` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scaleOption` int(11) DEFAULT NULL,
  `otherScale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`quote_id`),
  KEY `IDX_14BED1D04584665A` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quote_detail`
--

INSERT INTO `quote_detail` (`quote_id`, `designType`, `letter`, `numOfLetterItem`, `numOfGraphicItem`, `product_id`, `remark`, `scaleOption`, `otherScale`) VALUES
(3, 'cccc', 'cccc', NULL, 2, NULL, NULL, NULL, NULL),
(47, 'bh', 'ABC', NULL, 1, 1, NULL, NULL, NULL),
(48, 'bh', 'FGH', NULL, 1, 2, NULL, NULL, NULL),
(49, 'bh', 'HJK', NULL, 1, 3, NULL, NULL, NULL),
(50, 'bh', 'XYZ', NULL, 1, 4, NULL, NULL, NULL),
(58, 'bh', '2', NULL, 1, 4, NULL, NULL, NULL),
(59, 'bh', 'YO', NULL, 1, 2, NULL, NULL, NULL),
(60, 'bh', 'NIKE', NULL, 1, 1, NULL, NULL, NULL),
(61, 'lt', 'PUMA', NULL, 0, 2, NULL, NULL, NULL),
(63, 'bh', 'Reebok', NULL, 1, 4, NULL, NULL, NULL),
(64, 'lg', NULL, NULL, 1, 5, NULL, NULL, NULL),
(67, 'bh', 'CO', NULL, 0, 1, 'Example for a sign with 4 fabricated letters "COCO" for Acrylic', NULL, NULL),
(68, 'bh', 'CO', NULL, 0, 2, 'Example for a sign with 4 fabricated letters "COCO" for Inox', NULL, NULL),
(69, 'bh', 'CO', NULL, 0, 1, 'Example for a sign with 4 fabricated letters "COCO" for Acrylic for Adaptor Calculation', NULL, NULL),
(70, 'bh', 'CO', NULL, 0, 2, NULL, NULL, NULL),
(71, 'bh', '55', NULL, 0, 1, NULL, NULL, NULL),
(74, 'lt', 'CO', NULL, 0, 1, NULL, 1, NULL),
(75, 'lt', 'CO', NULL, 0, 2, NULL, 1, NULL),
(76, 'lt', 'COT', NULL, 0, 2, NULL, 1, NULL),
(77, 'bh', 'CO', NULL, 1, 1, 'sadfafd', 1, NULL),
(78, 'lt', 'DKY', NULL, 0, 2, NULL, 1, NULL),
(79, 'lt', 'Drum', NULL, 0, 2, NULL, 1, NULL),
(80, 'lt', 'DEF', NULL, 0, 2, NULL, 1, NULL),
(81, 'lt', 'CHRIS', NULL, 0, 2, NULL, 1, NULL),
(82, 'bh', 'NIKE', NULL, 1, 2, 'Brosse horizontal', 1, NULL),
(83, 'lt', 'PAUL', NULL, 0, 2, 'Face diffusante vinyl', 1, NULL),
(84, 'lg', NULL, NULL, 1, 2, NULL, 1, NULL),
(85, 'bh', 'NIKE', NULL, 1, 2, 'brossage horizontal', 1, NULL),
(86, 'lt', 'PW', NULL, 0, 2, NULL, 1, NULL),
(87, 'lt', 'Toto', NULL, 0, 2, NULL, 1, NULL),
(88, 'bh', 'abc', NULL, 1, 1, NULL, 1, NULL),
(89, 'lt', 'NIKE', NULL, 0, 2, NULL, 1, NULL),
(90, 'bh', 'NIKE', NULL, 1, 2, NULL, 0, '1/2'),
(91, 'bh', 'NIKE', NULL, 1, 2, NULL, 0, '1/2'),
(92, 'bh', 'NIKE', NULL, 1, 2, NULL, 0, '1/2'),
(93, 'bh', 'NIKE', NULL, 1, 2, NULL, 0, '1/2'),
(94, 'bh', 'NIKE', NULL, 1, 2, NULL, 0, '1/2'),
(95, 'bh', 'DD', NULL, 2, 2, NULL, 1, NULL),
(96, 'lt', 'a', NULL, 0, 2, NULL, 1, NULL),
(97, 'lt', 'A', NULL, 0, 2, NULL, 1, NULL),
(98, 'bh', 'PTE 01', NULL, 1, 2, NULL, 1, NULL),
(99, 'bh', 'PTE 02', NULL, 1, 2, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quote_document`
--

CREATE TABLE IF NOT EXISTS `quote_document` (
  `quote_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`quote_id`,`document_id`),
  KEY `IDX_325322E7DB805178` (`quote_id`),
  KEY `IDX_325322E7C33F7837` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quote_document`
--

INSERT INTO `quote_document` (`quote_id`, `document_id`) VALUES
(60, 13),
(74, 15),
(75, 14),
(76, 20),
(77, 21),
(78, 22),
(79, 23),
(80, 24),
(81, 25),
(82, 26),
(83, 27),
(84, 28),
(85, 29),
(86, 30),
(87, 31),
(88, 32),
(89, 33),
(90, 34),
(91, 35),
(92, 36),
(93, 37),
(94, 38),
(95, 39),
(96, 41),
(97, 42),
(98, 43),
(99, 44);

-- --------------------------------------------------------

--
-- Table structure for table `quote_otherdocument`
--

CREATE TABLE IF NOT EXISTS `quote_otherdocument` (
  `quote_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`quote_id`,`document_id`),
  KEY `IDX_AFDED35BDB805178` (`quote_id`),
  KEY `IDX_AFDED35BC33F7837` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quote_otherdocument`
--

INSERT INTO `quote_otherdocument` (`quote_id`, `document_id`) VALUES
(75, 16),
(75, 19),
(95, 40);

-- --------------------------------------------------------

--
-- Table structure for table `transport_rate`
--

CREATE TABLE IF NOT EXISTS `transport_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromWeight` decimal(10,2) NOT NULL,
  `toWeight` decimal(10,2) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `conditionCompare` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `formula` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=64 ;

--
-- Dumping data for table `transport_rate`
--

INSERT INTO `transport_rate` (`id`, `fromWeight`, `toWeight`, `rate`, `conditionCompare`, `formula`) VALUES
(1, '0.50', '0.50', '957.00', '=', ' '),
(2, '1.00', '1.00', '1143.00', '=', ' '),
(3, '1.50', '1.50', '1329.00', '=', ' '),
(4, '2.00', '2.00', '1515.00', '=', ' '),
(5, '2.50', '2.50', '1701.00', '=', ' '),
(6, '3.00', '3.00', '1860.00', '=', ' '),
(7, '3.50', '3.50', '2019.00', '=', ' '),
(8, '4.00', '4.00', '2178.00', '=', ' '),
(9, '4.50', '4.50', '2337.00', '=', ' '),
(10, '5.00', '5.00', '2496.00', '=', ' '),
(11, '5.50', '5.50', '2628.00', '=', ' '),
(12, '6.00', '6.00', '2760.00', '=', ' '),
(13, '6.50', '6.50', '2892.00', '=', ' '),
(14, '7.00', '7.00', '3024.00', '=', ' '),
(15, '7.50', '7.50', '3156.00', '=', ' '),
(16, '8.00', '8.00', '3288.00', '=', ' '),
(17, '8.50', '8.50', '3420.00', '=', ' '),
(18, '9.00', '9.00', '3552.00', '=', ' '),
(19, '9.50', '9.50', '3684.00', '=', ' '),
(20, '10.00', '10.00', '3816.00', '=', ' '),
(21, '10.50', '10.50', '3871.00', '=', ' '),
(22, '11.00', '11.00', '3926.00', '=', ' '),
(23, '11.50', '11.50', '3981.00', '=', ' '),
(24, '12.00', '12.00', '4036.00', '=', ' '),
(25, '12.50', '12.50', '4091.00', '=', ' '),
(26, '13.00', '13.00', '4146.00', '=', ' '),
(27, '13.50', '13.50', '4201.00', '=', ' '),
(28, '14.00', '14.00', '4256.00', '=', ' '),
(29, '14.50', '14.50', '4311.00', '=', ' '),
(30, '15.00', '15.00', '4366.00', '=', ' '),
(31, '15.50', '15.50', '4421.00', '=', ' '),
(32, '16.00', '16.00', '4476.00', '=', ' '),
(33, '16.50', '16.50', '4531.00', '=', ' '),
(34, '17.00', '17.00', '4586.00', '=', ' '),
(35, '17.50', '17.50', '4641.00', '=', ' '),
(36, '18.00', '18.00', '4696.00', '=', ' '),
(37, '18.50', '18.50', '4751.00', '=', ' '),
(38, '19.00', '19.00', '4806.00', '=', ' '),
(39, '19.50', '19.50', '4861.00', '=', ' '),
(40, '20.00', '20.00', '4916.00', '=', ' '),
(41, '20.50', '20.50', '5038.00', '=', ' '),
(42, '21.00', '21.00', '5160.00', '=', ' '),
(43, '21.50', '21.50', '5282.00', '=', ' '),
(44, '22.00', '22.00', '5404.00', '=', ' '),
(45, '22.50', '22.50', '5526.00', '=', ' '),
(46, '23.00', '23.00', '5648.00', '=', ' '),
(47, '23.50', '23.50', '5770.00', '=', ' '),
(48, '24.00', '24.00', '5892.00', '=', ' '),
(49, '24.50', '24.50', '6014.00', '=', ' '),
(50, '25.00', '25.00', '6136.00', '=', ' '),
(51, '25.50', '25.50', '6258.00', '=', ' '),
(52, '26.00', '26.00', '6380.00', '=', ' '),
(53, '26.50', '26.50', '6502.00', '=', ' '),
(54, '27.00', '27.00', '6624.00', '=', ' '),
(55, '27.50', '27.50', '6746.00', '=', ' '),
(56, '28.00', '28.00', '6868.00', '=', ' '),
(57, '28.50', '28.50', '6990.00', '=', ' '),
(58, '29.00', '29.00', '7112.00', '=', ' '),
(59, '29.50', '29.50', '7234.00', '=', ' '),
(60, '30.00', '30.00', '7356.00', '<=', ' '),
(61, '30.50', '70.00', '208.00', '>', ' '),
(62, '70.50', '300.00', '174.00', '>', ' '),
(63, '300.50', '99999.00', '172.00', '>', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `transport_rate_copy`
--

CREATE TABLE IF NOT EXISTS `transport_rate_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `condition` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `formula` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE IF NOT EXISTS `user_detail` (
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `IDX_4B5464AE979B1AD6` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`user_id`, `company_id`, `name`) VALUES
(2, 1, 'aaaa'),
(4, 3, 'mr_tukkae1@hotmail.com'),
(5, 4, 'dddd dddd'),
(6, 5, 'eeee eeee'),
(7, 6, 'gderoland@signifique.com'),
(8, 7, 'qqqq qqqq'),
(9, 8, 'brian'),
(10, 9, 'yoyo@example.com'),
(11, 10, 'nattapon@ascentec.net'),
(12, 11, 'mr_tukkae@hotmail.com'),
(13, 12, 'mr_tukkaesss'),
(14, 13, '4444@hotmail.com1'),
(15, 14, '5555@hotmail.com'),
(17, 16, 'dsfa@hotmail.com'),
(18, 17, 'sales@signinox.com'),
(22, 18, 'poweruser@hotmail.com'),
(23, 19, 'christophemartinthailand@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_user`
--

CREATE TABLE IF NOT EXISTS `user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F7129A8092FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_F7129A80A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `user_user`
--

INSERT INTO `user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(1, 'admin', 'admin', 'admin@hotmail.com', 'admin@hotmail.com', 1, 't4fypu4n8iokk0wcwk4wk8wo84s4wcs', 'tzeGhYW74aejmMWpVNREXmIUpfLo3FEdGS13Ha/kX/2D/tJLk4NbCjjsBBpcQa1AtBlWmqZsctANuhtNs/QXug==', '2015-05-29 03:32:02', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL),
(2, 'aaaa', 'aaaa', 'aaaa@hotmail.com', 'aaaa@hotmail.com', 1, 'ij461ytxapcs8gs40wsogc4k0g0w0wg', '+FYtM3SMApTlBJMIPiyzekwwBAklEKKPMxoLzt5E0xxT6QsHqN93ypImDo1E452tkEFDa2IdWcbFK2TQued9KQ==', '2015-02-19 17:18:43', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(4, 'mr_tukkae1@hotmail.com', 'mr_tukkae1@hotmail.com', 'mr_tukkae1@hotmail.com', 'mr_tukkae1@hotmail.com', 1, 'g24nzu1exeokoogw0sso8koksc0wc00', 'vSy3MaSoJ31hopVhTlyxsbdUZLZJgMN2UFan7XN8AF2GyeBKwjITV2bEgY2yL06yTMw/LsF35GX8fbI/9xs6pw==', '2014-08-06 07:49:40', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(5, 'dddd', 'dddd', 'dddd@hotmail.com', 'dddd@hotmail.com', 0, '23lyfudbqbi84ocgsoo888ogscg4ssc', 'qRHHtS5dqcq3Bv19W4icQdNgxy9UH/fNAac9Z/CVAnGco+01bsI205k536YXe+wmJbWMmEWxIq89FM3031lVpg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(6, 'eeee', 'eeee', 'eeee@hotmail.com', 'eeee@hotmail.com', 1, 'bryts85xwqgwoc80808gs4cgo4oggw8', '2Hj6u7VbJls8483wi6rwyZs1ia07mpVoKe9sQTH4ddtAo32w2E58upm9VwwxusPc5oK2aEdkZ5pCTQw2U6OaWw==', '2014-07-17 15:20:01', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(7, 'gderoland@signifique.com', 'gderoland@signifique.com', 'gderoland@signifique.com', 'gderoland@signifique.com', 1, 'lkixgsnfiuooc00kwcwkos4ggwkk4kk', 'GPtm4d+DrHdByRRKuse70lMZ8sB4P5Q0zqhR4qF5qTSAfnq7B3ddfoR0UpvK9T9zMx9kGgcjtf+6U25B43IvmA==', '2015-05-29 03:35:59', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(8, 'qqqq qqqq', 'qqqq qqqq', 'qqqq@example.com', 'qqqq@example.com', 1, 'klhkw84pgogokgss4wkcog4css0440c', '8fuuOQ0pzFbrKMZ3tZ81dqWVaPdpcolOXOVPNP6usUXhBpHQNnX94YnAGrVZSaYeHc7Y8XRldNphccc6AWaS5g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(9, 'brianmax', 'brianmax', 'brian@ascentec.net', 'brian@ascentec.net', 1, 'g3b92aud5e88s8ko4k00ckk44kok804', '2Nsb0sac6+8kyYLESwKAU3j/YEx8TSJmIMhE6IVcJepYM1x/x/pap6hht+IEkyuaMgmHacX5nOJCGpHLOQ3qTg==', '2014-07-21 08:01:24', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(10, 'yoyo@example.com', 'yoyo@example.com', 'yoyo@example.com', 'yoyo@example.com', 1, 'mw9pyu5h268w0ck80wocsokksc440sw', 'QWQRm/cPW/yJHE4i1F/mf/pMfxhjidTarAOWILoa/dC59UGczpgL7hLo/jotMgdk6GgRLq/WIxPdXB4oCsZ5OQ==', '2014-08-13 14:23:52', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(11, 'nattapon@ascentec.net', 'nattapon@ascentec.net', 'nattapon@ascentec.net', 'nattapon@ascentec.net', 1, 'mxe9fvb5w34oggg84o4wwck08s4sk0c', 'Oz1AgCQfoZ4xkHzRJRHXU1BpG08tdjA72qQTg7j08YaSxD7v7h/CsHHpaPfTfqoieIvDGgSe4cEo3Gnv3csflA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(12, 'mr_tukkae@hotmail.com', 'mr_tukkae@hotmail.com', 'mr_tukkae@hotmail.com', 'mr_tukkae@hotmail.com', 1, '5vz8pid9vh4wcgs0go0sgk4kggsc80k', 'ggh3pro8z+0rB8Bf57EDl0Sks4WavQ9GgSiPKie+whqz8LNgxA71BGCshizOHfozW/hcOWTW5z3LPrsf+1SU9w==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(13, 'mr_tukkaesss', 'mr_tukkaesss', 'mr_tukkaesss', 'mr_tukkaesss', 1, 'nmyabklg1cgcockww8kss80gsokcsok', 'p17lnX2Ic/La6iuciDInUrVHrrfisdBHXLsIV95U8J0KlZcw1aF/IvMu1OfL6VnT+L81S9vffDZrh8BOZvyZNQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(14, '4444@hotmail.com1', '4444@hotmail.com1', '4444@hotmail.com1', '4444@hotmail.com1', 1, 'etlqpos4lqo8w44k0sc0sw088c0kgsw', 'hpIBE3ASt/EOw8e/MEHFdlbe6/Mxp1Rv2Thf5tw9opN9StShFyW7jh1zE3mj16h8+X9BpMthU2mGgAE0CUNxjQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(15, '5555@hotmail.com', '5555@hotmail.com', '5555@hotmail.com', '5555@hotmail.com', 1, '5bgp7jxsv7s4kko4848c4gok0o0o0k4', 'UiVCaapM1Uxe+dgNpEoGKMRGx9FEUojDGEGRI0DK4VjFHgEcXUKZ+0oBqWZZDXyGjGEOuSFYf75Q7q/kiI03BA==', '2015-01-19 11:38:46', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(16, 'ttttttt@hotmail.com', 'ttttttt@hotmail.com', 'ttttttt@hotmail.com', 'ttttttt@hotmail.com', 1, 'kxo4a9tjmyo00gk88c8kwogk8c8koo0', 'SdiuIp/y6wZtT93WLcIEQc/exFb1H8hDFC1UQ7xAt/M/FdsutoYxylkWQjRaPQIqPhVyw51gLOZFRIXaxCtIig==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(17, 'dsfa@hotmail.com', 'dsfa@hotmail.com', 'dsfa@hotmail.com', 'dsfa@hotmail.com', 1, 'bs5aj59ct9ssgg8kws0s0kcsssos08w', 'ozxUqgQGGt/PQWFyunY51bxJ/O88c99gMdfr900nTakcJ451cNH9plOF7PIq6ot30qvtdOvJ/sRSDtwlriyNRw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(18, 'sales@signinox.com', 'sales@signinox.com', 'sales@signinox.com', 'sales@signinox.com', 1, 'b9enjqzlyooc40wcogcggcg40k48ocw', 'LBix6tP7t1ujr2H7BWt1AReEvy0Re7C2HL+w53t0L29RRaYMT5Lsfr9mOkYWmG0edYSvJS913XLKZ481seck6A==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(22, 'poweruser@hotmail.com', 'poweruser@hotmail.com', 'poweruser@hotmail.com', 'poweruser@hotmail.com', 1, 'bv489r2ylb4kw8ok0w4o4so44kk8s4o', 'g3ooDmApMlJ0O95gBFfqjICmNpkwbX9oCd1S8A0/cdpX/QmuJRX06g1LZEu141xWMCM+QsTiMxrsTGOuMe/VHQ==', '2015-02-19 16:10:22', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL),
(23, 'christophemartinthailand@gmail.com', 'christophemartinthailand@gmail.com', 'christophemartinthailand@gmail.com', 'christophemartinthailand@gmail.com', 1, 'cjfxx2tpnx4csc0gc84gsks08swws8c', 'LeBDHcBna2430Hfd40cs5hLVPi22lokKXD1GKo/F12E9AUWCpGwjELTnhdjuiTebmkmBLtPTwjKXC92Jaivabw==', '2015-05-29 02:58:47', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `variable`
--

CREATE TABLE IF NOT EXISTS `variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `helpText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CC4D878D5E237E06` (`name`),
  UNIQUE KEY `UNIQ_CC4D878D5F37A13B` (`token`),
  UNIQUE KEY `UNIQ_CC4D878D989D9B62` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `variable`
--

INSERT INTO `variable` (`id`, `name`, `value`, `token`, `slug`, `helpText`) VALUES
(1, 'Margin', '0.2', '%margin%', 'margin', ''),
(2, 'Discount', '0.05', '%discount%', 'discount', ''),
(3, 'Admin Fee EUR', '10.0', '%admin-fee-eur%', 'admin-fee-eur', ''),
(4, 'Admin Fee USD', '15.0', '%admin-fee-usd%', 'admin-fee-usd', ''),
(5, 'Transportation Remote Area Surcharge %', '0.1', '%transport-remote-area-surcharge%', 'transport-remote-area-surcharge', ''),
(6, 'Transportation Fuel Surcharge %', '0.2', '%transport-fuel-surcharge%', 'transport-fuel-surcharge', ''),
(7, 'Transportation Safety Surcharge %', '0.1', '%transport-safety-surcharge%', 'transport-safety-surcharge', ''),
(8, 'Coefficient Led-Return', '0.75', '%coefficient-led-return%', 'coefficient-led-return', ''),
(9, 'Coefficient Led-Face', '5.0', '%coefficient-led-face%', 'coefficient-led-face', ''),
(10, 'Coefficient Volume Transport', '3.0', '%coefficient-volume-transport%', 'coefficient-volume-transport', ''),
(11, 'Volume To Weight Dhl', '5000.0', '%volume-to-weight-dhl%', 'volume-to-weight-dhl', ''),
(12, 'Global Message (en)', 'Please note that our office will be closed from 13-Apr to 15-Apr as this is a Thai holiday', '%global-message-en%', 'global-message-en', ''),
(13, 'Global Message (fr)', 'S''il vous plaît noter que notre bureau sera fermé du 13 avril au 15-Apr car c''est un jour férié Thai', '%global-message-fr%', 'global-message-fr', ''),
(14, 'Quotation Validity Period', '30', '%quotation-validity-period%', 'quotation-validity-period', NULL),
(15, 'Emails (bcc)', 'nattapon@ascentec.net', '%emails-bcc%', 'emails-bcc', NULL),
(16, 'Email (from)', 'nattapon@ascentec.net', '%email-from%', 'email-from', NULL),
(17, 'Delivery Period', '15', '%order-delivery-period%', 'order-delivery-period', NULL),
(18, 'Led Quantity Calculation Face', '0.5*%s%', '%led-quantity-calculation-face%', 'led-quantity-calculation-face', NULL),
(19, 'Led Quantity Calculation Return', '0.3*%p%', '%led-quantity-calculation-return%', 'led-quantity-calculation-return', NULL),
(20, 'Led Adaptor 2.5A Capacity', '75', '%led-adaptor-2.5a-capacity%', 'led-adaptor-2.5a-capacity', NULL),
(21, 'Led Adaptor 5A Capacity', '150', '%led-adaptor-5a-capacity%', 'led-adaptor-5a-capacity', NULL),
(22, 'Led Adaptor 2.5A Rate', '800.0', '%led-adaptor-2.5a-rate%', 'led-adaptor-2.5a-rate', NULL),
(23, 'Led Adaptor 5A Rate', '1600.0', '%led-adaptor-5a-rate%', 'led-adaptor-5a-rate', NULL),
(24, 'Margin - Acrylic', '0.2', '%margin-acrylic%', 'margin-acrylic', NULL),
(28, 'Margin - Inox', '0.2', '%margin-inox%', 'margin-inox', NULL),
(30, 'Margin - Acrynox', '0.2', '%margin-acrynox%', 'margin-acrynox', NULL),
(34, 'Margin - Flatcut', '0.2', '%margin-flatcut%', 'margin-flatcut', NULL),
(35, 'Margin - Plaque', '0.2', '%margin-plaque%', 'margin-plaque', NULL),
(36, 'Emails (bcc) - Signinox Co., Ltds', 'nattapon@ascentec.net', '%emails-bcc-signinox%', 'emails-bcc-signinox', NULL),
(37, 'Email (from) - Signinox Co., Ltds', 'nattapon@ascentec.net', '%email-from-signinox%', 'email-from-signinox', NULL),
(38, 'Emails (bcc) - Signifique Co., Ltds', 'mr_tukkae@hotmail.com', '%emails-bcc-signifique%', 'emails-bcc-signifique', NULL),
(39, 'Email (from) - Signifique Co., Ltds', 'nattapon@ascentec.net', '%email-from-signifique%', 'email-from-signifique', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_value`
--
ALTER TABLE `category_value`
  ADD CONSTRAINT `FK_F544177D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_F544177DEE0CA5E8` FOREIGN KEY (`relatedRule_id`) REFERENCES `category_value` (`id`);

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `FK_4FBF094F43656FE6` FOREIGN KEY (`billingAddress_id`) REFERENCES `location` (`id`),
  ADD CONSTRAINT `FK_4FBF094FB1835C8F` FOREIGN KEY (`shippingAddress_id`) REFERENCES `location` (`id`);

--
-- Constraints for table `company_detail`
--
ALTER TABLE `company_detail`
  ADD CONSTRAINT `FK_13991BE6979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FK_1F1B251EDB805178` FOREIGN KEY (`quote_id`) REFERENCES `quote_detail` (`quote_id`);

--
-- Constraints for table `item_option`
--
ALTER TABLE `item_option`
  ADD CONSTRAINT `FK_DC7F757F12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_DC7F757F126F525E` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  ADD CONSTRAINT `FK_DC7F757F4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_DC7F757FB3C678F7` FOREIGN KEY (`categoryValue_id`) REFERENCES `category_value` (`id`),
  ADD CONSTRAINT `FK_DC7F757FDB805178` FOREIGN KEY (`quote_id`) REFERENCES `quote_detail` (`quote_id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `quote`
--
ALTER TABLE `quote`
  ADD CONSTRAINT `FK_6B71CBF4979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_6B71CBF4A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user_user` (`id`);

--
-- Constraints for table `quote_detail`
--
ALTER TABLE `quote_detail`
  ADD CONSTRAINT `FK_14BED1D04584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_14BED1D0DB805178` FOREIGN KEY (`quote_id`) REFERENCES `quote` (`id`);

--
-- Constraints for table `quote_document`
--
ALTER TABLE `quote_document`
  ADD CONSTRAINT `FK_325322E7C33F7837` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`),
  ADD CONSTRAINT `FK_325322E7DB805178` FOREIGN KEY (`quote_id`) REFERENCES `quote_detail` (`quote_id`);

--
-- Constraints for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD CONSTRAINT `FK_4B5464AE979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_4B5464AEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
