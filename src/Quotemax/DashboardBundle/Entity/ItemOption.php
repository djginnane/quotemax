<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\ItemOptionRepository")
 * @ORM\Table(name="item_option")
 */
class ItemOption
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * 
	 */
	protected $id;
	
	
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $remark;

    
    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="itemOptions")
     *
     */
    protected $product;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="QuoteDetail", inversedBy="itemOptions")
     * @ORM\JoinColumn(name="quote_id", referencedColumnName="quote_id")
     */
    protected $quoteDetail;

    
    
    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="itemOptions")
     *
     */
    protected $item;
    
    

    
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     *
     */
    protected $category;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="CategoryValue")
     *
     */
    protected $categoryValue;
    

    
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return ItemOption
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string 
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set item
     *
     * @param \Quotemax\DashboardBundle\Entity\Item $item
     * @return ItemOption
     */
    public function setItem(\Quotemax\DashboardBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Quotemax\DashboardBundle\Entity\Item 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set quoteDetail
     *
     * @param \Quotemax\DashboardBundle\Entity\QuoteDetail $quoteDetail
     * @return ItemOption
     */
    public function setQuoteDetail(\Quotemax\DashboardBundle\Entity\QuoteDetail $quoteDetail = null)
    {
        $this->quoteDetail = $quoteDetail;

        return $this;
    }

    /**
     * Get quoteDetail
     *
     * @return \Quotemax\DashboardBundle\Entity\QuoteDetail 
     */
    public function getQuoteDetail()
    {
        return $this->quoteDetail;
    }

    /**
     * Set categoryValue
     *
     * @param \Quotemax\DashboardBundle\Entity\CategoryValue $categoryValue
     * @return ItemOption
     */
    public function setCategoryValue(\Quotemax\DashboardBundle\Entity\CategoryValue $categoryValue = null)
    {
        $this->categoryValue = $categoryValue;

        return $this;
    }

    /**
     * Get categoryValue
     *
     * @return \Quotemax\DashboardBundle\Entity\CategoryValue 
     */
    public function getCategoryValue()
    {
        return $this->categoryValue;
    }

    /**
     * Set category
     *
     * @param \Quotemax\DashboardBundle\Entity\Category $category
     * @return ItemOption
     */
    public function setCategory(\Quotemax\DashboardBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Quotemax\DashboardBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set product
     *
     * @param \Quotemax\DashboardBundle\Entity\Product $product
     * @return ItemOption
     */
    public function setProduct(\Quotemax\DashboardBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Quotemax\DashboardBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
