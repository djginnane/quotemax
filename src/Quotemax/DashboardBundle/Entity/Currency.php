<?php

namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, unique=true)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100, unique=true)
     */
    private $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="fromCurrency", type="string", length=3)
     */
    private $fromCurrency;

    /**
     * @var string
     *
     * @ORM\Column(name="toCurrency", type="string", length=3)
     */
    private $toCurrency;    
    
    
    /**
     * @ORM\Column(name="rate", type="decimal", scale=5, nullable=false)
     *
     */
    private $rate;
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Currency
     */
    public function setSlug($slug)
    {
    	$this->slug = $slug;
    
    	return $this;
    }
    
    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
    	return $this->slug;
    }

    /**
     * Set fromCurrency
     *
     * @param string $fromCurrency
     * @return Currency
     */
    public function setFromCurrency($fromCurrency)
    {
        $this->fromCurrency = $fromCurrency;

        return $this;
    }

    /**
     * Get fromCurrency
     *
     * @return string 
     */
    public function getFromCurrency()
    {
        return $this->fromCurrency;
    }

    /**
     * Set toCurrency
     *
     * @param string $toCurrency
     * @return Currency
     */
    public function setToCurrency($toCurrency)
    {
        $this->toCurrency = $toCurrency;

        return $this;
    }

    /**
     * Get toCurrency
     *
     * @return string 
     */
    public function getToCurrency()
    {
        return $this->toCurrency;
    }

    /**
     * Set rate
     *
     * @param string $rate
     * @return Currency
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string 
     */
    public function getRate()
    {
        return $this->rate;
    }
}
