<?php

namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 * Category
 *
 * @ORM\Table(name="category_value")
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\CategoryValueRepository")
 */
class CategoryValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * 
     */
    private $name;
    

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */    
    private $value;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="values")
     *
     */
    protected $category;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="CategoryValue")
     *
     */
    protected $relatedRule;
    
    
	/**
     * 
     *
     * @ORM\Column(name="isAdditionalCostAfterMargin", type="boolean", nullable=true)
     */    
    private $isAdditionalCostAfterMargin = false;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="weightOrder", type="integer", nullable=true)
     */
    protected $weightOrder;
    
    
    //TODO: 
    //	-breakdown to another categoryValueDetail
    //	
    
    /**
     * @var string
     *
     * @ORM\Column(name="formattedRules", type="string", length=255, nullable=true)
     * 
     * %typeOfMeasurementUnit%['cm', 'cm2', cm3', 'perimeter'] *  rate /50
     * ToDo:
     * 	- set token for differnt calculated dimension like %area%, '%volume', %width%, '%perimeter' + validateFormulat() method to check valid tokens
     */
    protected $formattedRules;
    
    
    /**
     *	@var array 
     * 	@ORM\Column(name="rates", type="array", nullable=true)
     * 
     * $rateWithDifferentCurrency
     * keep in array ('USD' => 3.5, 'EUR' => 5.5)
     */
    protected $rates;
    
    
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
    	
    }
    
    public function __toString(){
    	return $this->name;
    }
    
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CategoryValue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return CategoryValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set weightOrder
     *
     * @param integer $weightOrder
     * @return CategoryValue
     */
    public function setWeightOrder($weightOrder)
    {
        $this->weightOrder = $weightOrder;

        return $this;
    }

    /**
     * Get weightOrder
     *
     * @return integer 
     */
    public function getWeightOrder()
    {
        return $this->weightOrder;
    }

    /**
     * Set formattedRules
     *
     * @param string $formattedRules
     * @return CategoryValue
     */
    public function setFormattedRules($formattedRules)
    {
        $this->formattedRules = $formattedRules;

        return $this;
    }

    /**
     * Get formattedRules
     *
     * @return string 
     */
    public function getFormattedRules()
    {
        return $this->formattedRules;
    }

    /**
     * Set category
     *
     * @param \Quotemax\DashboardBundle\Entity\Category $category
     * @return CategoryValue
     */
    public function setCategory(\Quotemax\DashboardBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Quotemax\DashboardBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set relatedRule
     *
     * @param \Quotemax\DashboardBundle\Entity\CategoryValue $relatedRule
     * @return CategoryValue
     */
    public function setRelatedRule(\Quotemax\DashboardBundle\Entity\CategoryValue $relatedRule = null)
    {
        $this->relatedRule = $relatedRule;

        return $this;
    }

    /**
     * Get relatedRule
     *
     * @return \Quotemax\DashboardBundle\Entity\CategoryValue 
     */
    public function getRelatedRule()
    {
        return $this->relatedRule;
    }

    /**
     * Set isAdditionalCostAfterMargin
     *
     * @param boolean $isAdditionalCostAfterMargin
     * @return CategoryValue
     */
    public function setIsAdditionalCostAfterMargin($isAdditionalCostAfterMargin)
    {
        $this->isAdditionalCostAfterMargin = $isAdditionalCostAfterMargin;

        return $this;
    }

    /**
     * Get isAdditionalCostAfterMargin
     *
     * @return boolean 
     */
    public function getIsAdditionalCostAfterMargin()
    {
        return $this->isAdditionalCostAfterMargin;
    }
}
