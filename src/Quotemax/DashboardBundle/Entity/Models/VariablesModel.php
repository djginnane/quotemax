<?php

namespace Quotemax\DashboardBundle\Entity\Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * VariablesModel
 *
 */
class VariablesModel
{
 
    private $variables;
    

    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->variables = new \Doctrine\Common\Collections\ArrayCollection();

    }
    
    
    
    
    /**
     * Add $variables
     *
     * @param \Quotemax\DashboardBundle\Entity\Variable $variables
     * @return VariablesModel
     */
    public function addVariable(\Quotemax\DashboardBundle\Entity\Variable $variables)
    {
    	
    	$this->variables[] = $variables;
    
    	return $this;
    }
    
    /**
     * Remove $variables
     *
     * @param \Quotemax\DashboardBundle\Entity\Variable $variables
     */
    public function removeVariable(\Quotemax\DashboardBundle\Entity\Variable $variables)
    {
    	$this->variables->removeElement($variables);
    }
    
    /**
     * Get $variables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVariables()
    {
    	return $this->variables;
    }
    
   
}
