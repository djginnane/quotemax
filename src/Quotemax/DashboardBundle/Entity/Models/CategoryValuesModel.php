<?php

namespace Quotemax\DashboardBundle\Entity\Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CategoryValuesModel
 *
 */
class CategoryValuesModel
{
 
    private $categoryValues;
    

    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->categoryValues = new \Doctrine\Common\Collections\ArrayCollection();

    }
    
   
    
    /**
     * Add $categoryValues
     *
     * @param \Quotemax\DashboardBundle\Entity\CategoryValue $categoryValues
     * @return CategoryValuesModel
     * 
     */
    public function addCategoryValue(\Quotemax\DashboardBundle\Entity\CategoryValue $categoryValues)
    {
    	
    	$this->categoryValues[] = $categoryValues;
    
    	return $this;
    }
    
    
    /**
     * Remove $categoryValues
     *
     * @param \Quotemax\DashboardBundle\Entity\CategoryValue $categoryValues
     */
    public function removeVariable(\Quotemax\DashboardBundle\Entity\CategoryValue $categoryValues)
    {
    	$this->categoryValues->removeElement($categoryValues);
    }
    
    /**
     * Get $categoryValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategoryValues()
    {
    	return $this->categoryValues;
    }
    
   
}
