<?php

namespace Quotemax\DashboardBundle\Entity\Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CurrenciesModel
 *
 */
class CurrenciesModel
{
 
    
    private $currencies;
    

    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->currencies = new \Doctrine\Common\Collections\ArrayCollection();

    }
    
    
    
    
    /**
     * Add $currency
     *
     * @param \Quotemax\DashboardBundle\Entity\Currency $currency
     * @return CurrenciesModel
     */
    public function addCurrency(\Quotemax\DashboardBundle\Entity\Currency $currency)
    {
    	
    	$this->currencies[] = $currency;
    
    	return $this;
    }
    
    /**
     * Remove $currency
     *
     * @param \Quotemax\DashboardBundle\Entity\Currency $currency
     */
    public function removeCurrency(\Quotemax\DashboardBundle\Entity\Currency $currency)
    {
    	$this->currencies->removeElement($currency);
    }
    
    /**
     * Get $currencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrencies()
    {
    	return $this->currencies;
    }
    
   
}
