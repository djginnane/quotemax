<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\CompanyDetailRepository")
 * @ORM\Table(name="company_detail")
 */
class CompanyDetail
{

	
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Quotemax\DashboardBundle\Entity\Company", inversedBy="detail")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * 
     */
    protected $company;
    
  
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $bankAccountInfo;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $logo;
    
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     *
     */
    protected $quoteCode;
    
    
    public function __construct()
    {
    	
    }
    
 
    public function __toString(){
    	return "CompanyDetail";
    }


   

    /**
     * Set bankAccountInfo
     *
     * @param string $bankAccountInfo
     * @return CompanyDetail
     */
    public function setBankAccountInfo($bankAccountInfo)
    {
        $this->bankAccountInfo = $bankAccountInfo;

        return $this;
    }

    /**
     * Get bankAccountInfo
     *
     * @return string 
     */
    public function getBankAccountInfo()
    {
        return $this->bankAccountInfo;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return CompanyDetail
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }
    
    
    /**
     * Set quoteCode
     *
     * @param string $quoteCode
     * @return CompanyDetail
     */
    public function setQuoteCode($quoteCode)
    {
    	$this->quoteCode = $quoteCode;
    
    	return $this;
    }
    
    /**
     * Get quoteCode
     *
     * @return string
     */
    public function getQuoteCode()
    {
    	return $this->quoteCode;
    }
    

    /**
     * Set company
     *
     * @param \Quotemax\DashboardBundle\Entity\Company $company
     * @return CompanyDetail
     */
    public function setCompany(\Quotemax\DashboardBundle\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Quotemax\DashboardBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
