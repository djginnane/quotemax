<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Quotemax\DashboardBundle\Entity\ItemOption;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\ItemRepository")
 * @ORM\Table(name="item")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class Item
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * 
	 */
	protected $id;
	
	
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $name;
    
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     *
     */
    protected $type;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="QuoteDetail", inversedBy="items")
     * @ORM\JoinColumn(name="quote_id", referencedColumnName="quote_id")
     *
     */
    protected $quoteDetail;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     *	//Type ['acrylic', 'inox', 'acrynox', 'black']
     */
    protected $productType;
    

    /**
     * @ORM\OneToMany(targetEntity="ItemOption", mappedBy="item", cascade={"persist", "remove"})
     *
     */
    protected $itemOptions;
    
    

    
	
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=false)
     *
     */
    protected $height;
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=false)
     *
     */
    protected $width;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=false)
     *
     */
    protected $depth;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=false)
     *
     */
    protected $perimeter;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     */
    protected $surface;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    protected $quantity;
    
    
    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     *
     */
    protected $currency;
    
    
    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     *
     */
    protected $measureUnit;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     * 
     * **Total Cost
     */
    protected $price = 0;
    

    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **Margin = Total Cost * 0.2;
     */
    protected $margin = 0;    
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     * 
     * **additional cost w/o margin
     * 		
     */
    protected $additionalCost = 0;
    
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **totalSellingPrice = totalCost + margin + additionalCost
     */
    protected $priceSelling = 0; 
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **totalSellingPriceAll = totalSellingPrice * quantity
     */
    protected $priceSellingTotalAll = 0;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     *
     */
    protected $numOfLed;
    
    
    
    
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->itemOptions = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->setPrice(0);
        $this->setQuantity(1);
    }
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *	
     */
    public function updateLifecycle() {
    	
    	//if 'imperial' (inch) selected, then have to convert to centimeter unit as base measurement unit first
    	if($this->getMeasureUnit() == 'imperial'){
    			$this->setHeight($this->convertInchToCentimeter($this->height));
    			$this->setWidth($this->convertInchToCentimeter($this->width));
    			$this->setDepth($this->convertInchToCentimeter($this->depth));
    			$this->setPerimeter($this->convertInchToCentimeter($this->perimeter));
    			$this->setSurface($this->convertInchToCentimeter($this->surface));
    	}
    	
    }
    
    
    //utility functions
    
    //calculate all cost & price + return totalSellingPrice
    public function calculatePrice($tokensDb = array(), &$auditLog = ""){
    	$price = 0;
    	$additionalCost = 0;
    	if($this->getQuoteDetail()){
    		$itemOptions = null;
    		if($this->getQuoteDetail()->getItemOptions()){ //get parent-level options as default
    			$itemOptions = $this->getQuoteDetail()->getItemOptions();
    		}
    		if($this->getItemOptions()){ //override parent-level options if got any options in child-level 
    			$itemOptions = $this->getQuoteDetail()->getItemOptions();
    		}
    		
    		$tokens = $this->getTokens($tokensDb);
    		foreach($itemOptions as $option){
    			
    			if($option->getCategoryValue()){
    				$cv = $option->getCategoryValue();
    				
    				if($cv->getIsAdditionalCostAfterMargin()){ //sum additionalCost (exclude margin)
    					$result = $this->proceedCalculationRule($cv, $tokens, $auditLog);
	    				$additionalCost += $result;
    				}else{ //sum totalCost
	    				$result = $this->proceedCalculationRule($cv, $tokens, $auditLog);
	    				$price += $result;
    				}
    				
    			}	
    		}	
    	}
    	
    	//calculate margin
    	$margin = $price * $tokens['%margin%'];
    	$priceSelling = $price + $margin + $additionalCost;
    	
    	$this->setPrice($price);
    	$this->setMargin($margin);
    	$this->setAdditionalCost($additionalCost);
    	$this->setPriceSelling($priceSelling);
    	$this->setPriceSellingTotalAll($priceSelling * $this->quantity);
    	
    	
    	//calculate number of Leds for some products and options selected
    	$this->calculateNumberOfLed($tokens, $auditLog);
    	
    	
    	//return $this->getPriceSelling();
    }
    
    private function proceedCalculationRule(CategoryValue $cv, $tokens = array(), &$auditLog = ""){
    	//var_dump('yoyo', $tokens);
    	
    	$formattedRules = $cv->getFormattedRules(); // ex. '2 - 1';
    	$expression = strtr($formattedRules,$tokens);
    	$result = 0;
    	//var_dump($expression);
    	eval( '$result = (' . $expression . ');' );
    	//var_dump($cv->getName(), $cv->getFormattedRules(), $result, ":::::::::::::::::::::::::");
    	$auditLog = $auditLog."<li>".$cv->getName().' > '.$cv->getFormattedRules().' > '.$expression.' > '.$result."</li>";
    	
    	if($cv->getRelatedRule()){
    		$cvChild = $cv->getRelatedRule();
    		$result += $this->proceedCalculationRule($cvChild, $tokens, $auditLog);
    	}
    	
    	return $result;
    }
    
    private function validatePriceRule(){
    	//TODO:
    	//	- validate each rules within item if they are valid to execute or not
    	//
    }
    
    
    
    public function calculateNumberOfLed($tokens = array(), &$auditLog = ""){
    	//TODO
    	// - check if product if valid case
    	// - set number of led into field
    	// - create function for get TotalNumberOfLed *  Quantity
    	$numberOfLed = 0;
    	
    	$productId = $this->getQuoteDetail()->getProduct()->getId();
    	
    	//get itemOptions & categoryValues selected by client
    	$itemOptions = $this->getQuoteDetail()->getItemOptions();
    	$cvIds = array();
    	foreach($itemOptions as $option){
    		if($option->getCategoryValue()){
    			$cv = $option->getCategoryValue();
    			$cvIds[] = $cv->getId();
    		}
    	}
    	
    	//var_dump('yoyo', $cvIds);
    	//exit();
    	
    	//check case & proceed calculation
    	switch($productId){
    		case 1: //acrylic
    			//refer these rules in Acrylic Options Box-v1.2.docx
    			$casesFace = array(5, 14, 15, 16);
    			$casesReturn = array(11, 12, 13);
    			
    			foreach($cvIds as $cvId){
    				if(in_array($cvId, $casesFace)){
    					$numberOfLed = $this->proceedCalculationAdaptorRule('face', $tokens, $auditLog);
    				}elseif(in_array($cvId, $casesReturn)){
    					$numberOfLed = $this->proceedCalculationAdaptorRule('return', $tokens, $auditLog);
    				}
    			}
    			
    			
    			break;
    			
    		case 2: //inox >> //if selected categoryValueId = 42 ['Led lighting']
    			if(in_array(42, $cvIds)){
    				$numberOfLed = $this->proceedCalculationAdaptorRule('return', $tokens, $auditLog);
    			}
    			break;
    			
    		case 3: //acrynox >> always calculate
    			$numberOfLed = $this->proceedCalculationAdaptorRule('return', $tokens, $auditLog);
    			break;
    			
    		case 4: //flat-cut
    		case 5: //plaque
    		default:
    			
    	}
    	
    	$this->setNumOfLed($numberOfLed);
    		
    }
    

    
    private function proceedCalculationAdaptorRule($option = "return", $tokens = array(), &$auditLog = ""){
    	$numberOfLed = 0;
    	
    	$formattedRules = "0";
    	switch($option){
    		case "face": //Led Quantity Calculation (Face lighting)
    			$formattedRules = $tokens['%led-quantity-calculation-face%'];
    			break;
    			
    		case "return": //Led Quantity Calculation (Return lighting)
    			$formattedRules = $tokens['%led-quantity-calculation-return%'];
    			break;
    			
    		default:
    			
    	}

    	$expression = strtr($formattedRules,$tokens);
    	$result = 0;
    	//var_dump($expression);
    	eval( '$result = (' . $expression . ');' );
    	
    	$auditLog = $auditLog.'<li>Led Quantity Calculation '.ucwords($option).' > '.$formattedRules.' > '.$expression.' > '.$result."</li>";
    	$auditLog = $auditLog.'<li>Total Leds : '.$result*$this->getQuantity().'</li>';
    	
    	$numberOfLed = $result;
    	
    	return $numberOfLed;
    }
    
    
   
    
    

    
    public function getTokens($tokensDb = array()){
    	$tokens = array(
    				'%w%' => $this->width, 
    				'%h%' => $this->height,
    				'%d%' => $this->depth,
    				'%p%' => $this->perimeter,
    				'%s%' => $this->surface,
    				'%a%' => $this->getArea(),
    				'%v%' => $this->getVolume(),
    				'%ri%' => 0.0,
    			);
    	
    	if($this->getQuoteDetail()->getProduct()->getId() == 3){ //'AcryNox'
    		$tokens['%ri%'] = $this->findReturnInox(); //'Return Inox' << for 'AcryNox' product case only
    	}
    	
    	$tokens = array_merge($tokens, $tokensDb);
    	return $tokens;
    }
    
    private function findReturnInox(){
    	$ri = 0.0;
    	 
    	if($this->getQuoteDetail()){
    		if($this->getQuoteDetail()->getItemOptions()){
    			$itemOptions = $this->getQuoteDetail()->getItemOptions();
    			foreach($itemOptions as $option){
    				/* @var $option ItemOption */
    				
    				if($option->getCategoryValue()){ //check if not null object for 'CategoryValue'
	    				if($option->getCategoryValue()->getId() == 58){ //1.0cm
	    					$ri = ($this->getDepth() - 1.0);
	    				}elseif($option->getCategoryValue()->getId() == 59){ //1.5cm
	    					$ri = ($this->getDepth() - 1.5);
	    				}
    				}
    
    			}
    		}
    	}
    	 
    	 
    	 
    	return $ri;
    }
    
    public function getArea(){
    	return $this->width * $this->height;
    }
    
    public function getVolume(){
    	return $this->width * $this->height * $this->depth;
    }
    
    
    public function getPriceSellingWithCurrencyConverted(){
    	return number_format($this->getPriceSelling() * $this->quoteDetail->getQuote()->getCurrencyRate(),2);
    }
    public function getPriceSellingTotalAllWithCurrencyConverted(){
    	return number_format($this->getPriceSellingTotalAll() * $this->quoteDetail->getQuote()->getCurrencyRate(),2);
    }
    public function getNumOfLedTotal(){
    	return $this->getNumOfLed() * $this->getQuantity();
    }
    
    
    public function convertCentimeterToInch($cm){
    	// 1 centimeter = 0.393700787 inches
    	return round($cm * 0.393700787, 2);
    }
   
    public function convertInchToCentimeter($inch){
    	// 1 inch = 2.54 centimeters
    	return round($inch * 2.54, 2);
    }
    
    public function getWidthDirect(){
    	return $this->width;
    }
    
    public function getHeightDirect(){
    	return $this->height;
    }
    
    public function getDepthDirect(){
    	return $this->depth;
    }
    
    public function getPerimeterDirect(){
    	return $this->perimeter;
    }
    
    public function getSurfaceDirect(){
    	return $this->surface;
    }
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set productType
     *
     * @param string $productType
     * @return Item
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return string 
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return Item
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string 
     */
    public function getHeight()
    {
        //return $this->height;
    	if($this->getMeasureUnit() == 'imperial'){ //inch
    		return $this->convertCentimeterToInch($this->height);
    	}else { // 'metric' cm. or mm.
    		return $this->height;
    	}
    }

    /**
     * Set width
     *
     * @param string $width
     * @return Item
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string 
     */
    public function getWidth()
    {
    	//return $this->width;
    	if($this->getMeasureUnit() == 'imperial'){ //inch
    		return $this->convertCentimeterToInch($this->width);
    	}else { // 'metric' cm. or mm.
    		return $this->width;
    	}
        
    }

    /**
     * Set depth
     *
     * @param string $depth
     * @return Item
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get depth
     *
     * @return string 
     */
    public function getDepth()
    {
        //return $this->depth;
    	if($this->getMeasureUnit() == 'imperial'){ //inch
    		return $this->convertCentimeterToInch($this->depth);
    	}else { // 'metric' cm. or mm.
    		return $this->depth;
    	}
    }

    /**
     * Set perimeter
     *
     * @param string $perimeter
     * @return Item
     */
    public function setPerimeter($perimeter)
    {
        $this->perimeter = $perimeter;

        return $this;
    }

    /**
     * Get perimeter
     *
     * @return string 
     */
    public function getPerimeter()
    {
        //return $this->perimeter;
    	if($this->getMeasureUnit() == 'imperial'){ //inch
    		return $this->convertCentimeterToInch($this->perimeter);
    	}else { // 'metric' cm. or mm.
    		return $this->perimeter;
    	}
    }
    
    
    /**
     * Set surface
     *
     * @param string $surface
     * @return Item
     */
    public function setSurface($surface)
    {
    	$this->surface = $surface;
    
    	return $this;
    }
    
    /**
     * Get surface
     *
     * @return string
     */
    public function getSurface()
    {
    	//return $this->surface;
    	
    	if($this->getMeasureUnit() == 'imperial'){ //inch
    		return $this->convertCentimeterToInch($this->surface);
    	}else { // 'metric' cm. or mm.
    		return $this->surface;
    	}
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Item
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Item
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set measureUnit
     *
     * @param string $measureUnit
     * @return Item
     */
    public function setMeasureUnit($measureUnit)
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }

    /**
     * Get measureUnit
     *
     * @return string 
     */
    public function getMeasureUnit()
    {
        return $this->measureUnit;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add itemOptions
     *
     * @param \Quotemax\DashboardBundle\Entity\ItemOption $itemOptions
     * @return Item
     */
    public function addItemOption(\Quotemax\DashboardBundle\Entity\ItemOption $itemOptions)
    {
        $this->itemOptions[] = $itemOptions;

        return $this;
    }

    /**
     * Remove itemOptions
     *
     * @param \Quotemax\DashboardBundle\Entity\ItemOption $itemOptions
     */
    public function removeItemOption(\Quotemax\DashboardBundle\Entity\ItemOption $itemOptions)
    {
        $this->itemOptions->removeElement($itemOptions);
    }

    /**
     * Get itemOptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItemOptions()
    {
        return $this->itemOptions;
    }

    /**
     * Set quoteDetail
     *
     * @param \Quotemax\DashboardBundle\Entity\QuoteDetail $quoteDetail
     * @return Item
     */
    public function setQuoteDetail(\Quotemax\DashboardBundle\Entity\QuoteDetail $quoteDetail = null)
    {
        $this->quoteDetail = $quoteDetail;

        return $this;
    }

    /**
     * Get quoteDetail
     *
     * @return \Quotemax\DashboardBundle\Entity\QuoteDetail 
     */
    public function getQuoteDetail()
    {
        return $this->quoteDetail;
    }



    /**
     * Set additionalCost
     *
     * @param string $additionalCost
     * @return Item
     */
    public function setAdditionalCost($additionalCost)
    {
        $this->additionalCost = $additionalCost;

        return $this;
    }

    /**
     * Get additionalCost
     *
     * @return string 
     */
    public function getAdditionalCost()
    {
        return $this->additionalCost;
    }

    /**
     * Set margin
     *
     * @param string $margin
     * @return Item
     */
    public function setMargin($margin)
    {
        $this->margin = $margin;

        return $this;
    }

    /**
     * Get margin
     *
     * @return string 
     */
    public function getMargin()
    {
        return $this->margin;
    }

    /**
     * Set priceSelling
     *
     * @param string $priceSelling
     * @return Item
     */
    public function setPriceSelling($priceSelling)
    {
        $this->priceSelling = $priceSelling;

        return $this;
    }

    /**
     * Get priceSelling
     *
     * @return string 
     */
    public function getPriceSelling()
    {
        return $this->priceSelling;
    }

    /**
     * Set priceSellingTotalAll
     *
     * @param string $priceSellingTotalAll
     * @return Item
     */
    public function setPriceSellingTotalAll($priceSellingTotalAll)
    {
        $this->priceSellingTotalAll = $priceSellingTotalAll;

        return $this;
    }

    /**
     * Get priceSellingTotalAll
     *
     * @return string 
     */
    public function getPriceSellingTotalAll()
    {
        return $this->priceSellingTotalAll;
    }

    /**
     * Set numOfLed
     *
     * @param integer $numOfLed
     * @return Item
     */
    public function setNumOfLed($numOfLed)
    {
        $this->numOfLed = $numOfLed;

        return $this;
    }

    /**
     * Get numOfLed
     *
     * @return integer 
     */
    public function getNumOfLed()
    {
        return $this->numOfLed;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Item
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
