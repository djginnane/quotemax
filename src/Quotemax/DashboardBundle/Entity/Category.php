<?php

namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM; // doctrine orm annotations
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\CategoryRepository")
 */
class Category
{
	/*
	 *  //How to use Doctrine Extensions: Timestampable, Sluggable, Translatable
	 *  	- http://symfony.com/doc/current/cookbook/doctrine/common_extensions.html
	 *      - https://github.com/l3pp4rd/DoctrineExtensions
	 * 
	 * 
	 * 
	 */
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255)
     * 
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100, nullable=false, unique=true)
     */
    private $slug;
    
    
    /**
     * @ORM\OneToMany(targetEntity="CategoryValue", mappedBy="category", cascade={"persist", "remove"})
     *
     */
    protected $values;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="weightOrder", type="integer", nullable=true)
     */
    protected $weightOrder;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="methodOfChoice", type="string", length=2, nullable=true)
     * 
     * //TODO:	['M' for 'mandatory', 'O' for 'optional']
     * 		- if 'mandatory', then must have just one value
     */
    protected $methodOfChoice;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="isActive", type="boolean", nullable=false)
     *
     * //TODO:	['mandatory', 'optional']
     * 		- if 'mandatory', then must have just one value
     */
    protected $isActive;
    
    

    
    
   
    
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setIsActive(true);
    }
    
    public function __toString(){
    	return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weightOrder
     *
     * @param integer $weightOrder
     * @return Category
     */
    public function setWeightOrder($weightOrder)
    {
        $this->weightOrder = $weightOrder;

        return $this;
    }

    /**
     * Get weightOrder
     *
     * @return integer 
     */
    public function getWeightOrder()
    {
        return $this->weightOrder;
    }

    /**
     * Add values
     *
     * @param \Quotemax\DashboardBundle\Entity\CategoryValue $values
     * @return Category
     */
    public function addValue(\Quotemax\DashboardBundle\Entity\CategoryValue $values)
    {
        $this->values[] = $values;

        return $this;
    }

    /**
     * Remove values
     *
     * @param \Quotemax\DashboardBundle\Entity\CategoryValue $values
     */
    public function removeValue(\Quotemax\DashboardBundle\Entity\CategoryValue $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set methodOfChoice
     *
     * @param string $methodOfChoice
     * @return Category
     */
    public function setMethodOfChoice($methodOfChoice)
    {
        $this->methodOfChoice = $methodOfChoice;

        return $this;
    }

    /**
     * Get methodOfChoice
     *
     * @return string 
     */
    public function getMethodOfChoice()
    {
        return $this->methodOfChoice;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Category
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
