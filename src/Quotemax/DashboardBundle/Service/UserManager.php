<?php

namespace Quotemax\DashboardBundle\Service;

use Symfony\Component\DependencyInjection\Dump\Container;
use Doctrine\ORM\EntityManager;

class UserManager 
{
	/**
	 * @var Container
	 */
	protected $container;
	
	/**
	 * 
	 * @var EntityManager
	 */
	protected $em;

	
	public function __construct(Container $container){
		$this->container = $container;
		$this->em = $this->container->get("doctrine.orm.entity_manager");
	}
	
	
	public function persist($entity){
		$this->prePersist($entity);
		
		$this->em->persist($entity);
		$this->em->flush($entity);
		
		$this->postPersist($entity);
	}
	
	public function prePersist($entity){
	
	}
	
	public function postPersist($entity){
		
	}
	
	public function update($entity){
		$this->preUpdate($entity);
	
		$this->em->flush($entity);
	
		$this->postUpdate($entity);
	}
	
	public function preUpdate($entity){
	
	}
	
	public function postUpdate($entity){
	
	}	
	
	
	
}