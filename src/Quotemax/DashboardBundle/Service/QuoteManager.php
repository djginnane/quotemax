<?php

namespace Quotemax\DashboardBundle\Service;

use Symfony\Component\DependencyInjection\Container;
use FOS\UserBundle\Mailer\Mailer;
use Quotemax\DashboardBundle\Repository\QuoteRepository;
use Quotemax\DashboardBundle\Repository\VariableRepository;
use Doctrine\ORM\EntityManager;
use Proxies\__CG__\Quotemax\DashboardBundle\Entity\Quote;

/*
	 * **Service Container 
	 * 		- http://symfony.com/doc/current/book/service_container.html
	 * 		- http://symfony.com/doc/current/components/dependency_injection/introduction.html (DependencyInjection Component)
	 * 
	 * 
	 * **Using a Factory to Create Services:
	 * 		- http://symfony.com/doc/current/components/dependency_injection/factories.html
	 * 
	 * 
	 */

class QuoteManager 
{
	/**
	 * @var Container
	 */
	protected $container;
	

	protected $mailer;
	

	/**
	 * 
	 * @var EntityManager
	 */
	protected $em;
	
	/**
	 * @var QuoteRepository
	 */
	protected $quoteRepo;
	
	/**
	 * 
	 * @var VariableRepository
	 */
	protected $variableRepo;
	
	public function __construct(Container $container){
		$this->container = $container;
		//$this->mailer - $container->get('mailer');
		$this->em = $this->container->get('doctrine.orm.entity_manager');
		$this->quoteRepo = $this->em->getRepository('QuotemaxDashboardBundle:Quote');
		$this->variableRepo = $this->em->getRepository('QuotemaxDashboardBundle:Variable');
	}
	
	public function findPendingQuotesAndExpire($options = array(), &$ack = ''){
		//TODO:
		//	- find all quotes with 'status' as parameter
		//	- update 'status' from 'pending' to 'expired' according to setting in tokens['%quotation-validity-period%']
		//	- create command line for auto-update daily
		//	- do command line as well to delete generated quote daily
		//
		
		$options = array();
		$options['tokens'] = $this->variableRepo->findTokens();
		$quotes = $this->quoteRepo->findPendingQuotesExceedingValidDays($options);
		
		
		if($quotes){
			//update status from 'PEN' (pending) into 'EXP' (expired)
			foreach($quotes as $quote){
				/* @var $quote Quote */
				 $quote->setStatus('EXP');
				
				$ack .= 'quote id: ' .$quote->getId().' expired. <br />';
			}
			
			$this->em->flush();
			
			return true;
			
		}else{
			
			$ack = 'Not found pending quotes to be expired.';
			return false;
		}

		
	}
	
	
	
}