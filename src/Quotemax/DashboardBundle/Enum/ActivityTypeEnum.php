<?php

namespace Quotemax\DashboardBundle\Enum;

use Biplane\EnumBundle\Enumeration\Enum;
use Symfony\Component\Security\Core\User\UserInterface;



class ActivityTypeEnum extends Enum
{
  	const MANUFACTURER = 'M';
  	const WHOLESALER = 'W';
  	const RETAILER = 'R';
  	const ARCHITECT = 'A';
  	
  	public static function getPossibleValues()
  	{
  		return array(static::MANUFACTURER, 
  					static::WHOLESALER, 
  					static::RETAILER,
  					static::ARCHITECT
  					);
  	}
  	
  	public static function getReadables()
  	{
  		return array(static::MANUFACTURER => 'Manufacturer', 
  					static::WHOLESALER => 'Wholesaler',
  					static::RETAILER => 'Retailer',
  					static::ARCHITECT => 'Architect'
  					);
  	}
}
