<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;

class CategoryValueType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('valueFieldType' => 'text'))
	{
		if(!array_key_exists('valueFieldType', $options)){
			$options['valueFieldType'] = 'text';
		}
		if(!array_key_exists('valueFieldHeight', $options)){
			$options['valueFieldHeight'] = '100%';
		}
		if(!array_key_exists('valueFieldWidth', $options)){
			$options['valueFieldWidth'] = '100%';
		}
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		//var_dump($options);
		
		$builder
		->add('name', 'text', array('label' => 'Name', 'required' => false, 'disabled' => true))
		->add('formattedRules', $this->options['valueFieldType'], array(
							'label' => 'Formatted Rules', 
							'required' => true,
							'max_length' => 255,
							'attr' => array(
										'rows'	=> $this->options['valueFieldHeight'],
										'cols'	=> $this->options['valueFieldWidth']
									),
							))
		->add('rate-virtual', 'hidden', [
							 'label' => 'Rate',
							 'required' => false,
							 'mapped' =>false
							])
		
		;
	}

	public function getName()
	{
		return 'variable';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\CategoryValue',
				'valueFieldType' => 'text',
		));
	}
}