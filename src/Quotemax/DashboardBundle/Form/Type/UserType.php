<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Quotemax\DashboardBundle\Enum\UserRolesEnum;



class UserType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('locale' => 'en'))
	{
	
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('loginVirtual', 'text', array('label' => 'Login', 'required' => false, 'mapped' => false, 'disabled'=>true))
		//->add('username', null, array('label' => 'Login', 'required' => false))
		->add('changeMyPassword', 'checkbox', array('label' => 'Change my password', 'required'=>false, 'mapped' => false))
		//->add('oldPassword', 'text', array('label' => 'Old Password', 'required' => false, 'mapped' => false))
		->add('plainPassword', 'password', array('label' => 'New Password', 'required' => false))
		->add('repeatPassword', 'password', array('label' => 'Repeat Password', 'required' => false, 'mapped' => false))
		;
		
		//TOFIX to be array
		if(array_key_exists('includeRoleField', $this->options)){
			if($this->options['includeRoleField']){
				$builder->add('role', 'choice', array('label' => 'Role', 'required' => false, 'choices' => UserRolesEnum::getReadables(), 'mapped' => false, 'empty_value' => false));
			}
		}
		
		//->add('email', null, array('label' => 'E-mail', 'required' => false));
	}

	public function getName()
	{
		return 'user';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\UserBundle\Entity\User',
		));
	}
}