<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Quotemax\DashboardBundle\Enum\UserRolesEnum;



class UserDetailType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('locale' => 'en'))
	{
	
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		//->add('name', null, array('label' => 'Name'))
		->add('user', new UserType($this->options), array('label' => 'User'))
		
		;
	}

	public function getName()
	{
		return 'userDetail';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\UserDetail',
		));
	}
}