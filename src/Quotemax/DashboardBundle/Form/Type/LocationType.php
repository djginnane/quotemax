<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;

class LocationType extends AbstractType
{
	
	private $options = array();
	
	public function __construct(array $options = array('locale' => 'en'))
	{
	
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('address', 'textarea', array('label' => 'Address', 'required' => true))
		->add('zipcode', null, array('label' => 'Post Code', 'required' => true))
		->add('city', null, array('label' => 'City', 'required' => true))
		->add('country', 'choice', array('label' => 'Country', 
						'required' => true,
						'choices' => Intl::getRegionBundle()->getCountryNames('fr'),
						'empty_value' => 'Country Name'
						))
		
		
		;
	}

	public function getName()
	{
		return 'location';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\location',
		));
	}
}