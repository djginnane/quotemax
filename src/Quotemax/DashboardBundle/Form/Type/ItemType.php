<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;

class ItemType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', null, array('label' => 'Name', 'required' => true,'disabled' => true ))
		
		->add('height', null, array('label' => 'Height', 'required' => true ))
		->add('width', null, array('label' => 'Width', 'required' => true ))
		->add('depth', null, array('label' => 'Depth', 'required' => true ))
		->add('perimeter', null, array('label' => 'Perimeter', 'required' => true ))
		->add('surface', null, array('label' => 'Surface', 'required' => false ))
		->add('type', 'hidden', array('label' => 'Type', 'required' => false ))
		

		
		;
	}

	public function getName()
	{
		return 'item';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Item',
		));
	}
}