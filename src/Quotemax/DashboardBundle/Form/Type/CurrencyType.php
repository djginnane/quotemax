<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;

class CurrencyType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('valueFieldType' => 'text'))
	{
		
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$builder
		->add('name', 'text', array('label' => 'Name', 'required' => false, 'disabled' => true))
		->add('rate', 'number', array(
							'label' => 'Rate', 
							'required' => true,
							'attr' => array(
										
									),
							))
		
		;
	}

	public function getName()
	{
		return 'currency';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Currency',
		));
	}
}