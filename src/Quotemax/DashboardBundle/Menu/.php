<?php

namespace Quotemax\DashboardBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class MainMenu extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Home', array('route' => 'qmxDashboard_quote_index'));
        $menu->addChild('List', array('route' => 'qmxDashboard_quote_list'));
        $menu->addChild('Create', array('route' => 'qmxDashboard_quote_create'));
        /*$menu->addChild('About Me', array(
            'route' => 'page_show',
            'routeParameters' => array('id' => 42)
        ));*/
        // ... add more children

        return $menu;
    }
}