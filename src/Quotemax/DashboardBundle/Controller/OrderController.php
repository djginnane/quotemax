<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Quotemax\DashboardBundle\Entity\Product;
use Quotemax\DashboardBundle\Entity\Category;

/**
 * @Route("/order", name="qmxDashboard_order")
 */
class OrderController extends Controller
{
	

	/**
	 * @Route("/", name="qmxDashboard_order_index")
	 * @Method({"GET", "POST"})
	 * @Template();
	 */
    public function indexAction(Request $request)
    {

    	
    	return $this->redirect($this->generateUrl('qmxDashboard_order_list'));
     
    }
    
    /**
     * @Route("/list", name="qmxDashboard_order_list")
     * @Template();
     */
    public function listAction(Request $request)
    {
    	
    	return array();
    }
    
    /**
     * @Route("/create", name="qmxDashboard_order_create")
     * @Template();
     */
    public function createAction(Request $request)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    	
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);

    }
    
    /**
     * @Route("/update/{id}", name="qmxDashboard_order_update",
     * 							requirements = {"id" = "\d+"})
     * @Template()
     */
    public function updateAction(Request $request, $id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
    
    /**
     * @Route("/delete/{id}", name="qmxDashboard_order_delete",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
    
    /**
     * @Route("/show/{id}", name="qmxDashboard_order_show", 
     * requirements = {"id" = "\d+"})
     * @Template()
     * 
     */
    public function showAction($id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
}