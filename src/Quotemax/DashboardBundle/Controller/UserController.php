<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Quotemax\DashboardBundle\Entity\Product;
use Quotemax\DashboardBundle\Entity\Category;
use Quotemax\DashboardBundle\Entity\Company;
use Quotemax\DashboardBundle\Form\Type\companyType;
use Quotemax\UserBundle\Entity\User;
use Quotemax\DashboardBundle\Entity\UserDetail;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * @Route("/admin/user", name="qmxDashboard_admin_user")
 */
class UserController extends Controller
{
	
	public function getUserRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxUserBundle:User');
	}
	
	public function getEntityManager(){
		return $this->getDoctrine()->getManager();
	}
	
	public function getFlash(){
		return $this->get('braincrafted_bootstrap.flash');
	}
	
	public function getUser(){
		return $this->get('security.context')->getToken()->getUser();
	}
	
	public function getCompany(){
		$company = null;
		$user = $this->getUser();
		if($user->getDetail()){
			if($user->getDetail()->getCompany()){
				$company = $user->getDetail()->getCompany();
			}
		}
		return $company;
	}

	public function forAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function forSuperAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function isValidCompanyToProceed(Company $company){
		//if($this->getUser())
		if (false !== $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			return true;
		}else{
			if($company->getId() != $this->getCompany()->getId()){
				throw new AccessDeniedException('Unable to access this item.');
				return false;
			}
			return false;
		}
	}
	
	
	public function preUpdateProfile(Company $company){
	
		//use same addess as billing
		if($company->getIsSameAsBilling()){
			$company->getShippingAddress()->setAddress($company->getBillingAddress()->getAddress());
			$company->getShippingAddress()->setZipcode($company->getBillingAddress()->getZipcode());
			$company->getShippingAddress()->setCity($company->getBillingAddress()->getCity());
			$company->getShippingAddress()->setCountry($company->getBillingAddress()->getCountry());
			
		}
		
		//upper case for 'clientCode'
		//$company->setClientCode(strtoupper($company->getClientCode()));
		
		
	
		//create new
		if(!$company->getId()){
			//set 'user' entity for login + email
			$userDetail = $company->getUsers()->first();
    		$user = $userDetail->getUser();
    		$user->setEmail($company->getEmail());
    		$user->setUsername($company->getEmail());
    		$userDetail->setName($company->getEmail());
    		
    		$company->setClientCode(uniqid()); //no longer required this field, so just keep it for future use
		}else{ //update profile
			$userDetail = $company->getUsers()->first();
			$user = $userDetail->getUser();
			$user->setEmail($company->getEmail());
			$user->setUsername($company->getEmail());
			$userDetail->setName($company->getEmail());
			
		}
	
	
	}
	

	/**
	 * @Route("/", name="qmxDashboard_admin_user_index")
	 * @Method({"GET", "POST"})
	 * @Template();
	 */
    public function indexAction(Request $request)
    {

    	 return $this->redirect($this->generateUrl('qmxDashboard_admin_user_list'));
    
     
    }
    
    /**
     * @Route("/list", name="qmxDashboard_admin_user_list")
     * @Template();
     */
    public function listAction(Request $request)
    {
    	
    	
    	$param['search-text'] = $request->query->get('search-text');
    	$param['search-type'] = $request->query->get('search-type');
   		$layoutExcluded = $request->query->get('layoutExcluded', false);
   		
   		//override role_type to be listed only for 'ADMIN' & 'SUPER_ADMIN'
   		$param['search-type'] = 'ROLE_'; // ex. of values ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']
    	 
    	$options = array('param' => $param);
    	$query = $this->getUserRepository()->searchCompanyUserInQuery($options);
    	
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    			$query,
    			$this->get('request')->query->get('page', 1), /*page number*/
    			10 /*limit per page*/
    		);
    	
    	//$layoutExcluded = false;
    	
    	return array('pagination' => $pagination, 'layoutExcluded' => $layoutExcluded);
    }
    
    /**
     * @Route("/search", name="qmxDashboard_admin_user_search",
     * requirements = {})
     * @Template("QuotemaxDashboardBundle:Company:_list.html.twig")
     *
     */
    public function searchAction(Request $request)
    {
    	$param['search-text'] = $request->query->get('search-text');
    	$param['search-type'] = $request->query->get('search-type');
    	
    	$options = array('param' => $param);
    	$query = $this->getUserRepository()->searchCompanyUserInQuery($options);
    	 
    
    	 
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    			$query,
    			$this->get('request')->query->get('page', 1), /*page number*/
    			10 /*limit per page*/
    		);
    	
    	
    	return array('pagination' => $pagination);
    }
    
    /**
     * @Route("/create", name="qmxDashboard_admin_user_create")
     * @Template();
     */
    public function createAction(Request $request)
    {
    	//$this->forAdminOnly();
    	
    	$company = new Company();
    	$user = new User();
    	$detail = new UserDetail();
    	$user->setDetail($detail);
    	$company->addUser($detail);
    	$options =array('locale' => $request->getSession()->get('_locale'), 'includeRoleField' => true);
    	$form = $this->createForm(new companyType($options), $company, array(
				    'action' => $this->generateUrl('qmxDashboard_admin_user_create'),
				    'method' => 'POST',
				));
    	
    	$form->handleRequest($request);
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$fosUserManager = $this->get('fos_user.user_manager');
    		
    		$this->preUpdateProfile($company);
    		
    		//flush 'user' entity first in order to get id
    		$userDetail = $company->getUsers()->first();
    		$user = $userDetail->getUser();
    		$role = $form['users'][0]['user']['role']->getData();

    		
    		
    		//server-side validate user before save
    		if($this->validateUser($user)){
    			
    			$user->setRoles(array($role));
    			$userDetail = $user->getDetail();
    			$user->setDetail(null);
    			$user->setEnabled(1);
    			
    			$fosUserManager->updateUser($user);
    			$em->flush($user);
    			
    			
    			$em->persist($userDetail);
    			$em->persist($company);
    			$em->flush();
    			
    			$ack = "Successfully created new company user.";
    			$this->getFlash()->success($ack);
    			 
    			return $this->redirect($this->generateUrl('qmxDashboard_admin_user_index'));
    		}else{
    			
    			
    		}
    		
    		
    	}
		
    	return array(
    			'form' => $form->createView(),
    			'company' => $company
    	);
    }
    
    private function validateUser($user){
    	$result = true;
    	
    	//validate new user
    	if(!$user->getId()){
    		$company = $user->getDetail()->getCompany();
    		$em = $this->getEntityManager();
    		
    		//check uniqueness of company.clientCode
    		$res = $em->getRepository('QuotemaxDashboardBundle:Company')->findOneBy(['clientCode' => $company->getClientCode()]);
    		
    		if($res){
    		 	$result = false;
    		 	$ack = "Failed, unable to update company user, duplicate code: '". $company->getClientCode()."'";
    		 	$this->getFlash()->error($ack);
    		}
    		
    		//check empty user.plainPassword
    		$plainPassword = $user->getPlainPassword();
    		if(empty($plainPassword)){
    			$result = false;
    			$ack = "Failed, please input password.";
    			$this->getFlash()->error($ack);
    		}
    		
    		
    		//check uniqueness for login & email
    		$res = $em->getRepository('QuotemaxUserBundle:User')->findOneBy(['email' => $user->getUsername()]);
    		

    		if($res){
    			
    			$result = false;
    			$ack = "Failed, unable to create new user, duplicate email/login: '". $user->getUsername()."'";
    			$this->getFlash()->error($ack);
    			
    			
    		}

    	}
    	
    	
    	return $result;
    }
    
    /**
     * @Route("/update/{id}", name="qmxDashboard_admin_user_update",
     * 							requirements = {"id" = "\d+"})
     * @Template("QuotemaxDashboardBundle:User:create.html.twig")
     */
    public function updateAction(Request $request, $id = null)
    {
    	if(!$id){
    		$id = $this->getUser()->getId();
    	}
     	$user = $this->getUserRepository()->find($id);
	
	    if (!$user) {
	        throw $this->createNotFoundException(
	            'No user found for id '.$id
	        );
	    }
	   
	    $company = $user->getDetail()->getCompany();
	    
	    $this->isValidCompanyToProceed($company);
	    
	    
	    $isCompanyUser = true;
	    if (false !== $this->get('security.context')->isGranted('ROLE_ADMIN')) {
	    	 $isCompanyUser = false;
	    }
	    
	    //set form
	    $options =	array(	'locale' => $request->getSession()->get('_locale'),
	    					'isCompanyUser' => $isCompanyUser,
	    					'includeRoleField' => true
	    					);
	    $form = $this->createForm(new companyType($options), $company/*, array(
	    		'action' => $this->generateUrl('qmxDashboard_admin_user_update', array('id' => $id)),
	    		'method' => 'POST',
	    )*/);
	    
	    
	    $form->handleRequest($request);
	    if ($form->isValid()) {
	    	$em = $this->getDoctrine()->getManager();
	    	$fosUserManager = $this->get('fos_user.user_manager');
	    	
	    	$this->preUpdateProfile($company);
	    
	    	//flush 'user' entity first in order to get id
	    	$userDetail = $company->getUsers()->first();
	    	$user = $userDetail->getUser();
	    	$role = $form['users'][0]['user']['role']->getData();
	    
	    
	    	$user->setRoles(array($role));
	    	//$userDetail = $user->getDetail();
	    	//$user->setDetail(null);
	    	//$user->setEnabled(1);
	    	$fosUserManager->updateUser($user);
	    	$em->flush($user);
	    
	    

	    	$em->flush($company);
	    
	    	$ack = "Successfully updated new company user.";
	    	$this->getFlash()->success($ack);
	    	 
	    	if($isCompanyUser){
	    		return $this->redirect($this->generateUrl('qmxDashboard_user_update'));
	    	}else{
	    		return $this->redirect($this->generateUrl('qmxDashboard_admin_user_index'));
	    	}
	    }
	    
	    return array(
	    		'form' => $form->createView(),
	    		'company' => $company
	    );
	    
	    
    }
    
    /**
     * @Route("/delete/{id}", name="qmxDashboard_admin_user_delete",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
    	
	    $user = $this->getUserRepository()->find($id);
	    /* @var $user User */
	
	    if (!$user) {
	        throw $this->createNotFoundException(
	            'No user found for id '.$id
	        );
	    }
	    
	    if(in_array('ROLE_SUPER_ADMIN', $user->getRoles())){
	    	throw $this->createNotFoundException(
	    			'Unable to delete Super Admin Account, id: '.$id
	    	);
	    }
	
	    //$userDetail = $user->getDetail();
	    //$company = $userDetail->getCompany();
	    //$this->getEntityManager()->remove($company);
	    
	    
	    //temp: not delete user + company from DB
	    //$this->getEntityManager()->remove($user);
	    
	    //de-activate this user when click 'delete'
	    $user->setEnabled(0); 
	    $this->getEntityManager()->flush();
	    
	    $ack = "Successfully deleted a company user.";
	    $this->getFlash()->alert($ack);
	    
	
	    return $this->redirect($this->generateUrl('qmxDashboard_admin_user_list'));
    }
    
    /**
     * @Route("/show/{id}", name="qmxDashboard_admin_user_show", 
     * requirements = {"id" = "\d+"})
     * @Template()
     * 
     */
    public function showAction($id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
    
   
}