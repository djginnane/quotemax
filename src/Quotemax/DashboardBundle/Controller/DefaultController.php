<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/", name="qmxDashboard_default")
 */
class DefaultController extends Controller
{
	
	/*
	 * **Translation
	 * 		- http://stackoverflow.com/questions/16139894/how-to-generate-translation-file-po-xliff-yml-from-a-symfony2-silex-p
	 * 		- http://stackoverflow.com/questions/17884199/translation-catalog-generation-with-symfony2-for-main-app
	 * 
	 * Example of 'Extract' command:
	 * 		- >php app/console translation:update --dump-messages --force fr QuotemaxDashboardBundle --output-format="xlf"
	 * 
	 * 
	 * Moving Symfony to Production 
	 * 		- http://stackoverflow.com/questions/9258715/moving-app-to-production-mode-in-symfony-2
	 * 
	 * Installing wkhtmltopdf on Ubuntu
	 * 		- http://www.zeroincombenze.eu/wiki/OpenERP/7.0/install/CentOS#Webkit_Installation
	 * 		- https://www.odoo.com/forum/help-1/question/how-to-install-wkhtmltopdf-0-12-0-version-in-server-56107
	 * 		- http://www.tecmint.com/install-wkhtmltopdf-html-page-to-pdf-converter-in-rhel-centos-fedora/
	 * 		- http://fedir.github.io/web/blog/2013/09/25/install-wkhtmltopdf-on-ubuntu/
	 * 		- http://sourceforge.net/projects/wkhtmltopdf/files/0.12.2/
	 * 		- [way to rpm on RHCloud] https://forums.openshift.com/is-it-possible-to-use-rpm-to-install-soft-and-resolve-dependencies
	 *			> $rpm2cpio '$pkg'.rpm | cpio -idmv
	 *
	 *
	 */
	

	public function getFlash(){
		return $this->get('braincrafted_bootstrap.flash');
	}
	
	public function getUser(){
		return $this->get('security.context')->getToken()->getUser();
	}
	
	public function getCompany(){
		$company = null;
		$user = $this->getUser();
		if($user->getDetail()){
			if($user->getDetail()->getCompany()){
				$company = $user->getDetail()->getCompany();
			}
		}
		return $company;
	}
	
	public function getVariableRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Variable');
	}
	
	/**
	 * @Route("/", name="qmxDashboard_default_index")
	 * 
	 */
    public function indexAction(Request $request)
    {
        
    	$user = $this->get('security.context')->getToken()->getUser();
    	if (!$user) {
    		throw $this->createNotFoundException(
    				'No user found for id '.$id
    		);
    	}
    	
    	$company = null;
		if( $user->getDetail()){
    		$company = $user->getDetail()->getCompany();
		}
		
		/*
		$locale = $request->getSession()->get('_locale');
		
		//translator
		$translator = $this->get('translator');
		
		$flash = $this->get('braincrafted_bootstrap.flash');
		
		//global message (information)
		$tokens = $this->getVariableRepository()->findTokens();
		$messageHeading = "<strong>".$translator->trans("Information")."!</strong> ";
		switch($locale){
			case 'en':
				if(array_key_exists('%global-message-en%', $tokens)){
					if(!empty($tokens['%global-message-en%']))
						$flash->info($messageHeading.$tokens['%global-message-en%']);
				}
				break;
			case 'fr':
				if(array_key_exists('%global-message-fr%', $tokens)){
					if(!empty($tokens['%global-message-fr%']))
						$flash->info($messageHeading.$tokens['%global-message-fr%']);
				}
				break;
			default:
		}
		
    	//set company-level required-attention message for company user
    	
    	if($company){
    		$messageHeading = "<strong>".$translator->trans("Attention Required")."!</strong> ";
    		switch($locale){
    			case 'en':
			    	if($company->getHomeMessageEn()){
			    		$flash->alert($messageHeading.$company->getHomeMessageEn());
			    	}
			    	break;
    			case 'fr':
    				if($company->getHomeMessageFr()){
    					$flash->alert($messageHeading.$company->getHomeMessageFr());
    				}
    				break;
    			default:
    		}
    	}
    	*/
    	
    	
    	/*
    	$flash->info('Hello, I\'m  Global-Level info Message');
    	$flash->alert('Hey, I\'m  Company-Level required-attention Message');
    	$flash->error('Yo, I\'m  Auto-System Error Message');
		$flash->success('Yo, I\'m  Auto-System Success Message');
    	*/
    	
    	//use translator service
    	//$translated = $this->get('translator')->trans('Hello '.$name);
    	//$locale = $request->getLocale();
    	//$request->setLocale('fr');
    	//$request->getSession()->set('_locale', 'fr');
  		
    	$name = "";
    	if($user->getDetail()){
    		$name = $user->getDetail()->getName();
    	}else{
    		$name ="";
    	}
    	
    	//$this->get('session')->getFlashBag()->add('info', 'message  yoyo');
        return $this->render('QuotemaxDashboardBundle:Default:index.html.twig', array('name' => $name));
    }
    
    
    /**
     * @Route("/locale/{locale}", name="qmxDashboard_default_locale", requirements={"locale" = "en|fr"})
     *
     */
    public function localeAction(Request $request, $locale)
    {
    	//var_dump('yoyoyo', $locale);
    
    	$localeCurrent = $request->getLocale();
    	if($locale != $localeCurrent){
	    	$request->setLocale($locale);
	    	$request->getSession()->set('_locale', $locale);
    	}
    	$referer = $this->getRequest()->headers->get('referer');
    	return new RedirectResponse($referer);
    }
}

