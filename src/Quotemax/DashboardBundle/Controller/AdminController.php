<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Quotemax\DashboardBundle\Entity\Product;
use Quotemax\DashboardBundle\Entity\Category;
use Quotemax\DashboardBundle\Entity\TransportRate;
use Quotemax\DashboardBundle\Form\Type\VariablesType;
use Quotemax\DashboardBundle\Form\Type\VariableType;
use Quotemax\DashboardBundle\Entity\Models\VariablesModel;
use Quotemax\DashboardBundle\Entity\Models\CurrenciesModel;
use Quotemax\DashboardBundle\Form\Type\CurrenciesType;
use Quotemax\DashboardBundle\Form\Type\CategoryValuesType;
use Quotemax\DashboardBundle\Entity\Models\CategoryValuesModel;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

/**
 * @Route("/admin", name="qmxDashboard_admin")
 */
class AdminController extends Controller
{
	
	

	public function getFlash(){
		return $this->get('braincrafted_bootstrap.flash');
	}
	
	public function getUser(){
		return $this->get('security.context')->getToken()->getUser();
	}
	
	public function getCompany(){
		$company = null;
		$user = $this->getUser();
		if($user->getDetail()){
			if($user->getDetail()->getCompany()){
				$company = $user->getDetail()->getCompany();
			}
		}
		return $company;
	}
	
	public function forAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function forSuperAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function getCompanyRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Company');
	}
	
	public function getInternalCompanies(){
		$companies = [];
		
		$comRepo = $this->getCompanyRepository();
		
		$companies = $comRepo->getInternalCompanies();
		
		return $companies;
		
	}
	
	public function isValidCompanyToProceed(Quote $quote){
		//if($this->getUser())
		if (false !== $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			return true;
		}else{
			if($quote->getCompany()->getId() != $this->getCompany()->getId()){
				throw new AccessDeniedException('Unable to access this item.');
				return false;
			}
			return false;
		}
	}
	
	public function getEntityManager(){
		return $this->getDoctrine()->getManager();
	}
	
	public function getTransportRateRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:TransportRate');
	}
	
	public function getVariableRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Variable');
	}
	
	public function getCurrencyRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Currency');
	}
	
	public function getCategoryValueRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:CategoryValue');
	}

	/**
	 * @Route("/", name="qmxDashboard_admin_index")
	 * @Method({"GET", "POST"})
	 * @Template();
	 */
    public function indexAction(Request $request)
    {
    	//translator
    	$translator = $this->get('translator');

    	$name = $translator->trans("Welcome to Admin Management");
        return array('name' => $name);
    
     
    }
    
    
    /**
     * @Route("/message/update", name="qmxDashboard_admin_message_update")
     * @Template();
     */
    public function updateMessageAction(Request $request)
    {
    	//get variables
    	$variableMessageEn = $this->getVariableRepository()->find(12); // global message - en
    	$variableMessageFr = $this->getVariableRepository()->find(13); // global message - fr
    	
    	$variablesModel = new VariablesModel();
    	$variablesModel->addVariable($variableMessageEn);
    	$variablesModel->addVariable($variableMessageFr);
    	
    	$options = array('valueFieldType' => 'textarea', 'valueFieldHeight' => '5', 'valueFieldWidth' => '70');
    	
    	$form = $this->createForm(new VariablesType($options), $variablesModel);
    	 
    	$form->handleRequest($request);
    	
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    		
    		$em = $this->getDoctrine()->getManager();

    		$em->flush();
    		 
    		
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
		
    	return array(
    			'form' => $form->createView()
    	);
    
    }
    
    /**
     * @Route("/transport/surcharge/update", name="qmxDashboard_admin_transport_surcharge_update")
     * @Template();
     */
    public function updateTransportSurchargeAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//get variables
    	$variableMessage1 = $this->getVariableRepository()->find(5); // Transport Remote Area Surcharge
    	$variableMessage2 = $this->getVariableRepository()->find(6); // Transport Fuel Surcharge
    	$variableMessage3 = $this->getVariableRepository()->find(7); // Transport Safety Surcharge
    	 
    	$variablesModel = new VariablesModel();
    	$variablesModel->addVariable($variableMessage1);
    	$variablesModel->addVariable($variableMessage2);
    	$variablesModel->addVariable($variableMessage3);
    	 
    	$options = array();
    	 
    	$form = $this->createForm(new VariablesType($options), $variablesModel);
    
    	$form->handleRequest($request);
    	 
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	return array(
    			'form' => $form->createView()
    	);
    
    }
    
    
    /**
     * @Route("/product/coefficient/update", name="qmxDashboard_admin_product_coefficient_update")
     * @Template();
     */
    public function updateProductCoefficientAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//get variables
    	$variableMessage1 = $this->getVariableRepository()->find(8); // Coefficient Led-Return
    	$variableMessage2 = $this->getVariableRepository()->find(9); // Coefficient Led-Face
    
    	$variablesModel = new VariablesModel();
    	$variablesModel->addVariable($variableMessage1);
    	$variablesModel->addVariable($variableMessage2);
    
    	$options = array();
    
    	$form = $this->createForm(new VariablesType($options), $variablesModel);
    
    	$form->handleRequest($request);
    
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	return array(
    			'form' => $form->createView()
    	);
    
    }
    
    
    /**
     * @Route("/product/rate/update", name="qmxDashboard_admin_product_rate_update")
     * @Template();
     */
    public function updateProductRateAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//get category values
    	$categories = array(12, 16, 21, 25); //raw materials for 4 products, no raw material for 'Acrylic' product
    	$categoryValues = $this->getCategoryValueRepository()->findAllInCategories($categories);
    	
    	$categoryValuesModel = new CategoryValuesModel();
    	foreach($categoryValues as $categoryValue){
    		
    		$categoryValuesModel->addCategoryValue($categoryValue);
    	}
    	
    	

    	
    	$options = array();
    	
    	$form = $this->createForm(new CategoryValuesType($options), $categoryValuesModel);
    	
    	$form->handleRequest($request);
    	
    	
    
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	return array(
    			'form' => $form->createView()
    	);
    
    }
    
    
    /**
     * @Route("/general/update", name="qmxDashboard_admin_general_update")
     * @Template();
     */
    public function updateGeneralAction(Request $request)
    {
    	//get variables
    	$variableMessage1 = $this->getVariableRepository()->find(14); // Quotation Validity Period
    	//$variableMessage2 = $this->getVariableRepository()->find(16); // Emails (from)
    	//$variableMessage3 = $this->getVariableRepository()->find(15); // Emails (bcc)
    	$variableMessage2 = $this->getVariableRepository()->find(37); // Emails (from) - Sigininox Co., Ltds
    	$variableMessage3 = $this->getVariableRepository()->find(36); // Emails (bcc) - Sigininox Co., Ltds
    	$variableMessage4 = $this->getVariableRepository()->find(39); // Emails (from) - Signifique Co., Ltds
    	$variableMessage5 = $this->getVariableRepository()->find(38); // Emails (bcc) - Signifique Co., Ltds
    	
    
    	$variablesModel = new VariablesModel();
    	$variablesModel->addVariable($variableMessage1);
    	$variablesModel->addVariable($variableMessage2);
    	$variablesModel->addVariable($variableMessage3);
    	$variablesModel->addVariable($variableMessage4);
    	$variablesModel->addVariable($variableMessage5);
    	
    	//get internal companies
    	$companies = $this->getInternalCompanies();
    
    	
    	$options = array();
    
    	$form = $this->createForm(new VariablesType($options), $variablesModel);
    
    	$form->handleRequest($request);
    
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	
    	return array(
    			'form' => $form->createView(),
    			'companies' => $companies
    	);
    
    }
    
    /**
     * @Route("/currency/update", name="qmxDashboard_admin_currency_update")
     * @Template();
     */
    public function updateCurrencyAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//get currencie
    	$currencies = $this->getCurrencyRepository()->findAll();
    	
    	$currenciesModel = new CurrenciesModel();
    	if($currencies){
    		foreach($currencies as $currency){
    			$currenciesModel->addCurrency($currency);
    		}
    	}
    	    	 
    	$options = array();
    	 
    	$form = $this->createForm(new CurrenciesType(), $currenciesModel);
    	
    
    	$form->handleRequest($request);
    	 
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	return array(
    			'form' => $form->createView()
    	);
    
    }
    
    
    /**
     * @Route("/margin/rate/update", name="qmxDashboard_admin_margin_rate_update")
     * @Template();
     */
    public function updateMarginRateAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//get variables
    	$variableMessage1 = $this->getVariableRepository()->find(24); // Margin - Acrylic
    	$variableMessage2 = $this->getVariableRepository()->find(28); // Margin - Inox
    	$variableMessage3 = $this->getVariableRepository()->find(30); // Margin - Acrynox
    	$variableMessage4 = $this->getVariableRepository()->find(34); // Margin - Flatcut
    	$variableMessage5 = $this->getVariableRepository()->find(35); // Margin - Plague
    	 
    	$variablesModel = new VariablesModel();
    	$variablesModel->addVariable($variableMessage1);
    	$variablesModel->addVariable($variableMessage2);
    	$variablesModel->addVariable($variableMessage3);
    	$variablesModel->addVariable($variableMessage4);
    	$variablesModel->addVariable($variableMessage5);
    	 
    	$options = array();
    	 
    	$form = $this->createForm(new VariablesType($options), $variablesModel);
    
    	$form->handleRequest($request);
    	
    	//get internal companies
    	$companies = $this->getInternalCompanies();
    	 
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	return array(
    			'form' => $form->createView(),
    			'companies' => $companies,
    	);
    
    
    }
    
    
    /**
     * @Route("/labour/rate/update", name="qmxDashboard_admin_labour_rate_update")
     * @Template();
     */
    public function updateLabourRateAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//get category values
    	$categories = array(29, 30, 31, 32, 33); //labour cost for 5 products;
    	$categoryValues = $this->getCategoryValueRepository()->findAllInCategories($categories);
    	 
    	$categoryValuesModel = new CategoryValuesModel();
    	foreach($categoryValues as $categoryValue){
    
    		$categoryValuesModel->addCategoryValue($categoryValue);
    	}
    	  
    	
    	 
    	$options = array();
    	 
    	$form = $this->createForm(new CategoryValuesType($options), $categoryValuesModel);
    	 
    	$form->handleRequest($request);
    	 
    	//get internal companies
    	$companies = $this->getInternalCompanies();
    	
    	 
    
    	if ($form->isValid()) {
    		//translator
    		$translator = $this->get('translator');
    
    		$em = $this->getDoctrine()->getManager();
    
    		$em->flush();
    		 
    
    		$ack = $translator->trans("Successfully updated items.");
    		$this->getFlash()->success($ack);
    	}
    
    	return array(
    			'form' => $form->createView(),
    			'companies' => $companies,
    	);
    
    }
    
    
    
    /**
     * @Route("/transport/import", name="qmxDashboard_admin_transport_import")
     * @Template();
     */
    public function importTransportRateAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//TODO:
    	//	- http://stackoverflow.com/questions/12460740/importing-csv-in-symfony-2
    	
    	//translator
    	$translator = $this->get('translator');
    	
    	$form = $this->createFormBuilder()
        ->add('submitFile', 'file', array('label' => 'Import Transportation Rates (.csv only)'))
        //Button
        ->add('saveChanges', 'submit', array('label' => 'Import rates'))
        ->add('cancel', 'button', array('label' => 'Cancel'))
        ->getForm();

	    // Check if we are posting stuff
	    if ($request->getMethod('post') == 'POST') {
	        // Bind request to the form
	        $form->handleRequest($request);
	
	        // If form is valid
	        if ($form->isValid()) {
	             // Get file
	             $file = $form->get('submitFile');
	
	             // Your csv file here when you hit submit button
	             $csvFile = $file->getData();
	             $this->validateCsvTransportRate($csvFile);
	             if (($handle = fopen($csvFile->getRealPath(), "r")) !== FALSE) {
	             	$em = $this->getEntityManager();
	             	
	             	//delete all rates from transport table
	             	$rates = $this->getTransportRateRepository()->findAll();
	             	foreach($rates as $rate){
	             		$em->remove($rate);
	             	}
	             	
	             	
	             	//insert new rates into transport table
	             	$i = 0;
	             	while(($row = fgetcsv($handle)) !== FALSE) { //ex. 5 cols >> 'from, to, rate, compare, formula'
	             		
	             		//remove comma from numeric string & cast to float value
	             		$row[0] = floatval(str_replace( ',', '', $row[0]));
	             		$row[1] = floatval(str_replace( ',', '', $row[1]));
	             		$row[2] = floatval(str_replace( ',', '', $row[2]));
	             		
	             		if($i == 0){ //skip heading row
	             			
	             			
	             		}else{
	             			//var_dump($row);	
	             			$rate = new TransportRate();
	             			$rate->setFromWeight($row[0]);
	             			$rate->setToWeight($row[1]);
	             			$rate->setRate($row[2]);
	             			$rate->setConditionCompare($row[3]);
	             			$rate->setFormula(" ");
	             			$em->persist($rate);
	             		}
	             		
	             		$i++;
	             	}
	             	
	             	
	             	//commit
	             	$em->flush();
	             	$ack = $translator->trans("Successfully imported new transport rates.");
	             	$this->getFlash()->success($ack);
	             }
	             
	        }
	
	     }
	
	    return array('form' => $form->createView());
    }
    
    private function validateCsvTransportRate($csvFile){
    	
    	if (($handle = fopen($csvFile->getRealPath(), "r")) !== FALSE) {
    		$i = 0;
    		while(($row = fgetcsv($handle)) !== FALSE) { //ex. 5 cols >> 'from, to, rate, compare, formula'
    			//debug
    			//var_dump($row); // process the row.
    	
    			//remove comma from numeric string
    			$row[0] = str_replace( ',', '', $row[0]);
    			$row[1] = str_replace( ',', '', $row[1]);
    			$row[2] = str_replace( ',', '', $row[2]);
    	
    			if($i == 0){
    				if($row[0] != 'from'
    						|| $row[1] != 'to'
    						|| $row[2] != 'rate'
    						|| $row[3] != 'compare'
    						|| $row[4] != 'formula'
    				){
    					throw $this->createAccessDeniedException('Invalid import CSV file !!');
    				}
    				 
    			}else{
    				if(!is_numeric($row[0]) || !is_numeric($row[1]) || !is_numeric($row[2])){
    					throw $this->createAccessDeniedException('Invalid import CSV file, allow only number for to, from, rate  !!');
    				}
    				if($row[3] != '=' && $row[3] != '<=' && $row[3] != '>'){  //allow only 3 compare signs ['=', '<=', '>']
    					throw $this->createAccessDeniedException('Invalid import CSV file, allow only 3 compare signs ["=", "<=", ">"]  !!');
    				}
    			}
    	
    			$i++;
    		}
    	}else{
    		throw $this->createAccessDeniedException('Invalid import CSV file !!');
    	}
    }
    
    
    
    /**
     * @Route("/list", name="qmxDashboard_admin_list")
     * @Template();
     */
    public function listAction(Request $request)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
    
    /**
     * @Route("/create", name="qmxDashboard_admin_create")
     * @Template();
     */
    public function createAction(Request $request)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    	
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);

    }
    
    /**
     * @Route("/update/{id}", name="qmxDashboard_admin_update",
     * 							requirements = {"id" = "\d+"})
     * @Template()
     */
    public function updateAction(Request $request, $id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
    
    /**
     * @Route("/delete/{id}", name="qmxDashboard_admin_delete",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
    
    /**
     * @Route("/show/{id}", name="qmxDashboard_admin_show", 
     * requirements = {"id" = "\d+"})
     * @Template()
     * 
     */
    public function showAction($id)
    {
    	//return new Response('Welcome to Dashboard Homepage');
    
    	$name = "Welcome to Dashboard Quote List";
    	return array('name' => $name);
    }
}