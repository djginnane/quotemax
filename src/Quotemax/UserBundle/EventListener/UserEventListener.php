<?php

namespace Quotemax\UserBundle\EventListener;

use FOS\UserBundle\Model\UserManager;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;
use Quotemax\UserBundle\Entity\User;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\DependencyInjection\Container;



class UserEventListener implements EventSubscriberInterface{
	
	protected $um;
	protected $em;
	protected $container;
	
	
	public function __construct(UserManager $um, EntityManager $em, Container $container){
		$this->um = $um;
		$this->em = $em;
		$this->container = $container;
	}
	
	
	/* (non-PHPdoc)
	 * @see \Symfony\Component\EventDispatcher\EventSubscriberInterface::getSubscribedEvents()
	 */
	public static function getSubscribedEvents() {
		// TODO: Auto-generated method stub
		
		return array(
			FOSUserEvents::REGISTRATION_COMPLETED => "onRegistrationSuccess",
			//FOSUserEvents::SECURITY_IMPLICIT_LOGIN => "onLoginSuccess", //<< probably,  not right event to intercept for the action
			SecurityEvents::INTERACTIVE_LOGIN => "onLoginSuccess",
		);

	}
	
	public function onRegistrationSuccess(FilterUserResponseEvent $event){
		
		$user = $event->getUser();
		$user->addRole(User::ROLE_DEFAULT); //by default same empty string in 'roles' column
		//$user->setRoles( array(User::ROLE_DEFAULT) ) ;
		$this->um->updateUser($user);
		$this->em->flush();
		
	}
	
	public function onLoginSuccess(InteractiveLoginEvent  $event){
		
		$response = $event->getRequest();
		$userToken = $event->getAuthenticationToken();
		
		//flash messages
		$user = $userToken->getUser();
		$company = null;
		if( $user->getDetail()){
			$company = $user->getDetail()->getCompany();
		}
		
		$locale = $this->container->get('session')->get('_locale', 'en');
		
		//translator
		$translator = $this->container->get('translator');
		
		$varRepo = $this->container->get('doctrine')->getRepository('QuotemaxDashboardBundle:Variable');
		
	
		$flash = $this->container->get('braincrafted_bootstrap.flash');
		
		//global message (information)
		$tokens = $varRepo->findTokens();
		$messageHeading = "<strong>".$translator->trans("Information")."!</strong> ";
		switch($locale){
			case 'en':
				if(array_key_exists('%global-message-en%', $tokens)){
					if(!empty($tokens['%global-message-en%']))
						$flash->info($messageHeading.$tokens['%global-message-en%']);
				}
				break;
			case 'fr':
				if(array_key_exists('%global-message-fr%', $tokens)){
					if(!empty($tokens['%global-message-fr%']))
						$flash->info($messageHeading.$tokens['%global-message-fr%']);
				}
				break;
			default:
		}
		
		//set company-level required-attention message for company user 
		if($company){
			$messageHeading = "<strong>".$translator->trans("Attention Required")."!</strong> ";
			switch($locale){
				case 'en':
					if($company->getHomeMessageEn()){
						$flash->alert($messageHeading.$company->getHomeMessageEn());
					}
					break;
				case 'fr':
					if($company->getHomeMessageFr()){
						$flash->alert($messageHeading.$company->getHomeMessageFr());
					}
					break;
				default:
			}
		}
		
		
		
	}
	
	
}

